#ifndef _TpxDigitizerModule__H
#define _TpxDigitizerModule__H

#include "core/config/Configuration.hpp"
#include "core/messenger/Messenger.hpp"
#include "core/module/Module.hpp"
#include "core/geometry/Detector.hpp"
#include "TpxParameter.h"
#include "TpxPixelHit.h"
#include "objects/PixelCharge.hpp"
#include "TpxDigitizer.h"
#include "Export/CmsLog.h"

namespace allpix {
/**
    * @ingroup Modules
    * @brief Module to simulate digitization of collected charges
    * @note This module supports parallelization
     *
     * This module provides a relatively simple simulation of a charge-sensitive amplifier
     * with Krummenacher feedack circuit while adding electronics
     * noise and simulating the threshold as well as accounting for threshold dispersion and ADC noise.
     */



class TpxDigitizerModule : public Module {
public:
    /**
    * @brief Constructor for this detector-specific module
    * @param config Configuration object for this module as retrieved from the steering file
    * @param messenger Pointer to the messenger object to allow binding to messages on the bus
    * @param detector Pointer to the detector for this module instance
    */
    TpxDigitizerModule(Configuration& config, Messenger* messenger, std::shared_ptr<Detector> detector);
    /**
    * @brief Constructor for this detector-specific module
    */
    ~TpxDigitizerModule() = default;
    /**
    * @brief Initialize optional ROOT histograms
    */
    void initialize() final;

    /**
    * @brief digitize the event
    */

    void run(Event* event);

    /**
    * @brief Finalize and write optional histograms
    */
    void finalize() final;

private:
    // Input message with the charges on the pixels
    std::shared_ptr<PixelChargeMessage> __pixel_message__;
    std::shared_ptr<TpxParameter> __tpx_parameter__;
    Messenger* __messenger__{nullptr};
    std::shared_ptr<DetectorModel> __model__;
    Configuration* __config__ {nullptr};
    Configuration* _tpx_config__ {nullptr};
    std::vector<TpxPixelHit> __hits__;
    std::unique_ptr<TpxDigitizer> __digitizer__;
    void __add_units__();
    std::vector<DiscriminatorEvent> __disc_events__;
};
} // namespace allpix



#endif // _TpxDigitizerModule__H
