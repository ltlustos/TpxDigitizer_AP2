# Configuration Example  

[TpxDigitizer]  

 #general  
log_level = STATUS  
 #run_enc = 0  _# optional [0,1]/[False,True], default 0. Run a 10us noise simulation before running event simulations_  

dt = 195.3ps  _# timestep of preamp simulation_  
t0 = 0ns  _# optional, default 200ns. Run preamp for t0 before the event simulation, assures the buildup of the bandwidth filtered noise_  
h_cutoff = 1e-3  _# optional, default 1e-3. Cutoff determining how long the preamplifier runs before entering idle state, higher cutoff gives shorter simulation times_  
t_acq = 10us  _# optional, default 0. Maximum shutter time, event simulation stops when h_cutoff condition is fulfilled or time exceeds t_acq for tacq > 0. 0 deactivates_  
clock_period = 25ns  _# optional, default 25ns.ToA clock period_  
fclock_period = 1.5625ns  _# optional, default 1.5625ns. fToA clock period_  
ufclock_period = 195.3ps  _# optional, default 195.3ps. ufToT clock period_  
randomize_event_clock_phase = 0 # optional [0,1],[False,True], default 0. random time offset for each event within clock_period_  

 ####Preamplifier  
  
tau_integrator = 4ns  _# optional, default 4ns. Preamplifier integration time_  
gain = 1.0  _# optional, default 1. Pixel gain_  
gain_asymmetry = 2.5perc  _# optional, default 2.5perc. Gain mismatch for positive and negative inputs - only of interest for high LET events / heavy ions_  
gain_dispersion = 0.0perc  _# optional, default 0. Gain dispersion across pixel matrix_  
noise_ENC = 80.0e  _# optional, default 80e. Preamplifier output noise, typically 60-80e_  
noise_bw = 1e4Hz 1.5e7Hz  _# optional, 1e4Hz 1.5e7Hz. Preamplifier output noise bandwidth_  

 ####Feedback  
  
 ###either  
 ###nonlinear ToT response, eg in https://doi.org/10.1088/1748-0221/13/11/P11014  
 ###definition of discharge current by piecewise linear function of preamplifier charge load  
 #fb_ikrum_threshold =    0           200e3       240e3   245e3   555e3       1e6  
 #fb_ikrum =              1.75e/ns    1.75e/ns    40e/ns  0.5e/ns 0.5e/ns     5e3e/ns  
 ##or  
 ##constant discharge current independent of preamplifier charge load  
fb_ikrum = 1.5e/ns  _# mandatory_  
 #fb_ikrum_threshold =    0   _# optional, default 0. Has to be 0 for constant discharge current_  

fb_ikrum_sigma = 0.05e/ns  _# optional, default 0. rms dispersion across pixel matrix_  
fb_ikrum_enc =.0e/ns  _# optional, default 0e/ns. time series noise rms on alpha_ikrum ~ 0 - 0.1 e/ns_  
fb_noise_bw = 1e1Hz 1.0e7Hz  _# optional, default 1e1Hz 1.0e7Hz. Noise bandwidth if alpha_ikrum_noise_enc > 0_  

fb_taper_width = 1600  _# optional, default 1600. Krummenacher feedback tapering width_  

fb_tau = 70ns 720ns 1.0us  _# optional, default 70ns 720ns 1.0us. Time constants of low-pass filtered feedback branches_  
fb_weight =  1.0 0.5 1.0  _# optional, default 1.0 0.5 1.0, Relative weights of low-pass filtered feedback branches_  
fb_ileak_fc = 1.1e4Hz  _# optional, default 1.1e4Hz. Lowpass filter cutoff for leakage current compensation, typically 1.1e4Hz, negative value inactivates_  
fb_ileak_weight= 1.0  _# optional, default 1.0. Weight of the leakage current compensation branch_  
  
 ##default or volcano  - volcano is still experimental  
 #feedback_type = default  _#  optional, volcano enables preamplifier inversion_  
 #volcano_limit_amplitude = 600e3e  _#  optional, default 600e3e. Only used if feedback_type == volcano_  
 #volcano_undershoot = -3e3e  _#  optional, default -3e3e. Only used if feedback_type == volcano_  
 #volcano_rebound_injection = 0e  _#  optional, default 0e. Only used if feedback_type == volcano_  
 #volcano_inversion_delay = 150ns  _#  optional, default 150ns. Only used if feedback_type == volcano. the time the preamp stays saturated before   inverting -> +- width of the first packet_  

 ##Discriminator  
disc_type=asym  _# optional, [ASYM, IDEAL], default ASYM - asymmetric shape. ideal - ideal square infinite bandwidth discriminator_  
disc_tau_rise = 2ns  _#  optional, default 2ns. Discriminator rise time, only used if disc_type==asym_  
disc_tau_fall = 15ns  _#  optional, default 15ns. Discriminator fall time, only used if disc_type==asym_  
disc_glitch_tau = 411ns  _#  optional, default 411ns. Glitch filter / noise count rejection. Tpx3 packet generation time after event completion 411ns_  

threshold = 650e  _#  mandatory, usually >= 650e. Discriminator threshold for ToA/ToT measurement_  
threshold_sigma = 0e  _#  optional, default = 0. Threshold dispersion across pixel matrix_  
threshold_noise_enc = 7.0e  _#  optional, default 7e. Given by lsb_dac * sqrt(2/12) - threshold is given by difference between FBK and threshold DAC, ~7e_  
threshold_noise_bw = 1e1Hz 1.1e5Hz  _#  optional, default 1e1Hz 1.1e7Hz. Threshold noise bandwidth_  

 ####Output  
  
 ##output path for png, csv files, relative to the directory of this file_  
output_path = ./_out/TEMP/  _#  optional, default directory of this file_  
timestamp_output_path = 0   _#  optional [0,1],[False,True], default 0. Append a timestamp per run to output path_   
 ##generate event histograms  
output_histogram = 1  _#  optional [0,1],[False,True], default 1. Add histogram data to root file_  
output_single_pixel_histogram = 0  _#  optional [0,1],[False,True], default 1. Generate also single pixel hit histograms_  
output_csv = 0  _#  optional [0,1],[False,True], default 0. - Export histogram data also as csv_  
output_png = 1  _#  optional [0,1],[False,True], default 1 - Export histogram also as png_  
output_event_plot = 10  _#  optional, default 0. Plot n first preamp events as png, -1 plot all, 0 no output_  
output_event_csv = 0  _#  optional, default 0. Export n first preamp events as csv, -1 export all, 0 no export_  
output_listmode = 1  _#  optional [0,1],[False,True], default 0. Export hit listmode_  