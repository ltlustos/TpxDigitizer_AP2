#ifndef TAPER_ERF_H
#define TAPER_ERF_H

#include "_TaperFunction_.h"
#include "Util/u_math.h"

class _Erf_: public _TaperFunction_
{
public:
    _Erf_(DTYPE sigma, DTYPE threshold = 0.0):
        _TaperFunction_(sigma,  threshold)
    {}

    virtual ~_Erf_() = default;

    DTYPE kernel(DTYPE val) override
    {
        return erf_norm(val, _threshold_, _scale_);
    }

protected:
    _Erf_(const _Erf_ &other) = default;
    _Erf_& operator =(const _Erf_& other) = default;
    };


class Erf_Sigma: public _Erf_
{
public:
    Erf_Sigma(DTYPE sigma, DTYPE _threshold = 0.0):
        _Erf_(sigma, _threshold)
    {}
    virtual ~Erf_Sigma() = default;
    std::string name() const
    {
        return "Erf_Sigma";
    }
protected:
    Erf_Sigma(const Erf_Sigma &other) = default;
    Erf_Sigma& operator =(const Erf_Sigma& other) = default;
};


class Erf_Width: public _Erf_
{
public:
    Erf_Width(DTYPE width, DTYPE _threshold = 0.0):
        _Erf_(0.388125 * width, _threshold)
    {}
    virtual ~Erf_Width() = default;
protected:
    Erf_Width(const Erf_Width &other) = default;
    Erf_Width& operator =(const Erf_Width& other) = default;
};
#endif // TAPER_ERF_H
