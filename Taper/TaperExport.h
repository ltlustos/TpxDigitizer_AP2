#ifndef TAPEREXPORT_H
#define TAPEREXPORT_H

#include "Common/dtype.h"
#include <string>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TDirectory.h>
#include <TMath.h>
#include <unistd.h>
#include "Util/u_root.h"
#include "Util/u_string.h"
#include "core/utils/log.h"
#include "Common/Exceptions.h"
#include "_TaperFunction_.h"
#include "LinearLogistic.h"
class TaperExport
{
public:

    TaperExport(TDirectory *rootDir, std::string output_path, std::unique_ptr<_TaperFunction_> &taper)
    {
        _rootDirectory = rootDir;
        _output_path = output_path + "taper/";
        __taper__ = &taper;
        std::filesystem::create_directories(_output_path);
        cleanup_old_export();
    }

    ~TaperExport() = default;

    void cleanup_old_export() const
    {

        auto files_regex = [](std::string path, std::string regex)
        {
            std::vector<std::string> files;
            std::regex fmatch(regex, std::regex_constants::ECMAScript | std::regex_constants::icase);
            for(auto& p: std::filesystem::directory_iterator(path))
            {
                if (std::regex_match(p.path().filename().c_str(), fmatch))
                    files.emplace_back(p.path().filename().string());
            }
            return files;
        };


        for (auto f : files_regex(_output_path, "^taper.*\\.png$"))
        {
            std::remove((_output_path + f).c_str());
        }
    }

    void create_png(DTYPE min, DTYPE max, DTYPE step=10)
    {
        delete_root_object_named<TMultiGraph>("taper");
        TMultiGraph *mg = new TMultiGraph("taper", "taper");
        size_t size = int((max-min) / step);
        std::vector<DTYPE> input_vec(size);
        std::vector<DTYPE> taper_vec(size);
        for (std::size_t i=0; i<size; ++i)
        {
            input_vec[i] = min + i * step;
            taper_vec[i] = (*__taper__)->calc(input_vec[i]);
        }
        auto* taper_graph = new TGraph(int(size), &input_vec[0], &taper_vec[0]);
        taper_graph->SetLineColor(kBlue);
        mg->Add(taper_graph);
        mg->Add(taper_graph, "ALP");
        mg->GetXaxis()->SetTitle("input [e]");
        mg->GetYaxis()->SetTitle("output [1]");
        mg->SetTitle("Taper");

        _rootDirectory->WriteTObject(mg, "Taper");
        png_export<TMultiGraph>(mg,
                                string_sprintf("%staper.png", _output_path.c_str()).c_str(),
                                "A");
        mg->Clear();
    }

    void create_ov()
    {

        delete_root_object_named<TMultiGraph>("ov");
        TMultiGraph *mg = new TMultiGraph("ov", "ov");

        DTYPE scale=1000;
        DTYPE step=0.01 * scale;

        std::vector<DTYPE> i,o;
        add(mg, 3*scale, 0.1*scale, step, i, o);

        std::vector<DTYPE> i1,o1;
        add(mg, 3*scale, 0.2*scale, step, i1, o1);

        std::vector<DTYPE> i2,o2;
        add(mg, 3*scale, 0.3*scale, step, i2, o2);

        std::vector<DTYPE> i3,o3;
        add(mg, 3*scale, 0.5*scale, step, i3, o3);

        std::vector<DTYPE> i4,o4;
        add(mg, 3*scale, 1*scale, step, i4, o4);

        mg->GetXaxis()->SetTitle("input [e]");
        mg->GetYaxis()->SetTitle("output [1]");
        mg->SetTitle("Taper");
        _rootDirectory->WriteTObject(mg, "Taper");
        png_export<TMultiGraph>(
                    mg,
                    string_sprintf("%staper_ov.png", _output_path.c_str()).c_str(),
                    "A");
        mg->Clear();
        delete mg;
    }

    void add(TMultiGraph *mg, DTYPE w, DTYPE es, DTYPE step, std::vector<DTYPE> &input_vec, std::vector<DTYPE> &taper_vec)
    {

        DTYPE min = 0;
        DTYPE max = 3*w + 3* es;
        size_t size = size_t((max-min) / step);
        input_vec.resize(size);
        taper_vec.resize(size);
        LinearLogistic_Width taper(w, es);
        for (std::size_t i=0; i<size; ++i)
        {
            input_vec[i] = min + i * step;
            taper_vec[i] = taper.calc(input_vec[i]);
        }
        auto* taper_graph = new TGraph(int(input_vec.size()), &input_vec[0], &taper_vec[0]);
        taper_graph->SetLineColor(kBlue);
        mg->Add(taper_graph);
        mg->Add(taper_graph, "ALP");
    }


private:
    TaperExport() = default;
    std::unique_ptr<_TaperFunction_> *__taper__;
    std::string _output_path;
    TDirectory* _rootDirectory;

};


#endif // TAPEREXPORT_H
