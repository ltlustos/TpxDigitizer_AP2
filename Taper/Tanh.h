#ifndef TAPER_TANH_H
#define TAPER_TANH_H

#include "_TaperFunction_.h"

class _Tanh_: public _TaperFunction_
{
public:
    _Tanh_(DTYPE scale, DTYPE threshold = 0.0):
        _TaperFunction_(scale,  threshold)
    {}

    virtual ~_Tanh_() = default;

    DTYPE kernel(DTYPE val) override
    {
        val -= _threshold_;
        return std::tanh(val / _scale_ / SQRT2);
    }

protected:
    _Tanh_(const _Tanh_ &other) = default;
    _Tanh_& operator =(const _Tanh_& other) = default;
    };


class Tanh_Sigma: public _Tanh_
{
public:
    Tanh_Sigma(DTYPE sigma, DTYPE _threshold = 0.0):
        _Tanh_(0.7796968 * sigma, _threshold)
    {}
    virtual ~Tanh_Sigma() = default;
protected:
    Tanh_Sigma(const Tanh_Sigma &other) = default;
    Tanh_Sigma& operator =(const Tanh_Sigma& other) = default;
};


class Tanh_Width: public _Tanh_
{
public:
    Tanh_Width(DTYPE width, DTYPE _threshold = 0.0):
        _Tanh_(0.642578125 * width, _threshold)
    {}
    virtual ~Tanh_Width() = default;
protected:
    Tanh_Width(const Tanh_Width &other) = default;
    Tanh_Width& operator =(const Tanh_Width& other) = default;
};
#endif // TAPER_TANH_H
