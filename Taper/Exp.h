#ifndef TAPER_EXP_H
#define TAPER_EXP_H

#include "_TaperFunction_.h"
#include "Util/u_math.h"
class _Exp_: public _TaperFunction_
{
public:
    _Exp_(DTYPE shape, DTYPE threshold = 0.0):
        _TaperFunction_(shape,  threshold)
    {}

    virtual ~_Exp_() = default;

    DTYPE kernel(DTYPE val) override
    {
        val -= _threshold_;
        return sign(val)* (1.0 - std::exp(-std::abs(val)/_scale_));
    }

protected:
    _Exp_(const _Exp_ &other) = default;
    _Exp_& operator =(const _Exp_& other) = default;
    };


class Exp_Sigma: public _Exp_
{
public:
    Exp_Sigma(DTYPE sigma, DTYPE _threshold = 0.0):
        _Exp_(0.67984772 * sigma, _threshold)
    {}
    virtual ~Exp_Sigma() = default;
protected:
    Exp_Sigma(const Exp_Sigma &other) = default;
    Exp_Sigma& operator =(const Exp_Sigma& other) = default;
};


class Exp_Width: public _Exp_
{
public:
    Exp_Width(DTYPE width, DTYPE _threshold = 0.0):
        _Exp_(0.1793 * width, _threshold)
    {}
    virtual ~Exp_Width() = default;
protected:
    Exp_Width(const Exp_Width &other) = default;
    Exp_Width& operator =(const Exp_Width& other) = default;
};
#endif // TAPER_EXP_H
