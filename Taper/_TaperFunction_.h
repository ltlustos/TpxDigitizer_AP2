#ifndef _TAPER__H
#define _TAPER__H

#include "Util/u_math.h"
#include "Common/dtype.h"

class _TaperFunction_
{
public:
    _TaperFunction_(DTYPE scale, DTYPE threshold=0.0)
    {
        _threshold_ = threshold;
        _scale_ = scale;
    }

    virtual ~_TaperFunction_() = default;

    virtual DTYPE calc(DTYPE val)
    {
        return kernel(val);
    }

protected:
    DTYPE _threshold_;
    DTYPE _scale_;
    _TaperFunction_() = default;
    _TaperFunction_(const _TaperFunction_ &other) = default;
    _TaperFunction_& operator =(const _TaperFunction_& other) = default;
    virtual DTYPE kernel(DTYPE)
    {
        throw SubclassResponsabilityException("Taper:kernel");
        return 0;
    }
};


#endif // _TAPER__H
