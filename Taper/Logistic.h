#ifndef TAPER_LOGISTIC_H
#define TAPER_LOGISTIC_H

#include "_TaperFunction_.h"

class _Logistic_: public _TaperFunction_
{
public:
    _Logistic_(DTYPE shape, DTYPE threshold = 0.0):
        _TaperFunction_(shape,  threshold)
    {}

    virtual ~_Logistic_() = default;

    DTYPE kernel(DTYPE val) override
    {
        return  2 / (1 + std::exp(- (val - _threshold_) / _scale_)) - 1;
    }

protected:
    _Logistic_(const _Logistic_ &other) = default;
    _Logistic_& operator =(const _Logistic_& other) = default;
    };


class Logistic_Sigma: public _Logistic_
{
public:
    Logistic_Sigma(DTYPE sigma, DTYPE _threshold = 0.0):
        _Logistic_(0.5513288 * sigma, _threshold)
    {}
    virtual ~Logistic_Sigma() = default;
protected:
    Logistic_Sigma(const Logistic_Sigma &other) = default;
    Logistic_Sigma& operator =(const Logistic_Sigma& other) = default;
};


class Logistic_Width: public _Logistic_
{
public:
    Logistic_Width(DTYPE width, DTYPE _threshold = 0.0):
        _Logistic_(0.1889 * width, _threshold)
    {}
    virtual ~Logistic_Width() = default;
protected:
    Logistic_Width(const Logistic_Width &other) = default;
    Logistic_Width& operator =(const Logistic_Width& other) = default;
};
#endif // TAPER_LOGISTIC_H
