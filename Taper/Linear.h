#ifndef TAPER_LINEAR_H
#define TAPER_LINEAR_H
#include "_TaperFunction_.h"

class _Linear_: public _TaperFunction_
{
public:
    _Linear_(DTYPE width, DTYPE threshold = 0.0):
        _TaperFunction_(width,  threshold)
    {
        _k_ = 1.0 / width;
        _min_ = this->_threshold_ - width;
        _max_ = this->_threshold_ + width;
    }

    virtual ~_Linear_() = default;

    DTYPE kernel(DTYPE val) override
    {
        val -= _threshold_;
        if (val < _min_)
            return -1;
        else if (val > _max_)
            return 1;
        else
            return val * _k_;
    }

protected:
    _Linear_(const _Linear_ &other) = default;
    _Linear_& operator =(const _Linear_& other) = default;
    DTYPE _k_;
    DTYPE _min_;
    DTYPE _max_;
    };

class Linear_Sigma: public _Linear_
{
public:
    Linear_Sigma(DTYPE sigma, DTYPE _threshold = 0.0):
        _Linear_(0.57735054 * 3 * sigma, _threshold)
    {}
    virtual ~Linear_Sigma() = default;
protected:
    Linear_Sigma(const Linear_Sigma &other) = default;
    Linear_Sigma& operator =(const Linear_Sigma& other) = default;
};

class Linear_Width: public _Linear_
{
public:
    Linear_Width(DTYPE width, DTYPE _threshold = 0.0):
        _Linear_(width, _threshold)
    {}
    virtual ~Linear_Width() = default;
protected:
    Linear_Width(const Linear_Width &other) = default;
    Linear_Width& operator =(const Linear_Width& other) = default;
};
#endif // TAPER_LINEAR_H
