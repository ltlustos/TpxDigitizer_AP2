#ifndef TAPER_LINEARLOGISTIC_H
#define TAPER_LINEARLOGISTIC_H
#include "_TaperFunction_.h"


class _LinearLogistic_: public _TaperFunction_
{
public:
    _LinearLogistic_(DTYPE width, DTYPE sigma_edge, DTYPE threshold = 0.0):
        _TaperFunction_(width, threshold)
    {
        _width_ = width;
        _sigma_edge_ = sigma_edge;
    }

    virtual ~_LinearLogistic_() = default;

    DTYPE kernel(DTYPE val) override
    {
        val -= _threshold_;
        return 1 + _sigma_edge_ / _width_ * (-std::log(std::exp((-val + _width_) /_sigma_edge_) + 1) + std::log(std::exp((-val - _width_) /_sigma_edge_) + 1));
    }

protected:
    _LinearLogistic_(const _LinearLogistic_ &other) = default;
    _LinearLogistic_& operator =(const _LinearLogistic_& other) = default;

protected:
    DTYPE _sigma_edge_;
    DTYPE _width_;
};


class LinearLogistic_Sigma: public _LinearLogistic_
        //_width_ scaled so that std(gradient(f)) == gradient_std
{
private:
    static DTYPE __scale__(DTYPE gradient_std, DTYPE sigma_edge)
    {
        DTYPE frac = sigma_edge / gradient_std;
        DTYPE min = 0.02;
        DTYPE max = 0.55;
        if (frac < min || frac > max)
        {
            std::string msg = string_sprintf("LinearLogistic_Sigma:: edge_width / width out = (%g / %g) = %g out of range [%g, %g]", sigma_edge * 3.0, gradient_std * 3.0, frac, min, max);
             throw ParameterOutOfRangeException(msg.c_str());
        }
        std::vector<DTYPE> p{-1.46586594e+03,  2.21387215e+03, -1.28756070e+03,  3.56097897e+02, -5.06140119e+01,  2.76861303e+00,  1.68292869e+00};
        DTYPE r = polyval(p, frac);
        return r;
    }
public:
    LinearLogistic_Sigma(DTYPE gradient_std, DTYPE sigma_edge, DTYPE _threshold = 0.0):
        _LinearLogistic_(3*gradient_std*__scale__(gradient_std, sigma_edge), sigma_edge, _threshold)
    {}
    virtual ~LinearLogistic_Sigma() = default;

protected:
    LinearLogistic_Sigma(const LinearLogistic_Sigma &other) = default;
    LinearLogistic_Sigma& operator =(const LinearLogistic_Sigma& other) = default;
};


class LinearLogistic_Width: public _LinearLogistic_

{
private:
    static DTYPE __scale__(DTYPE gradient_std, DTYPE sigma_edge)
    {
        //_width_ scaled so that percentile(50) at 1.5*gradient_std
        DTYPE frac = sigma_edge / gradient_std;
        DTYPE min = 0.005;
        DTYPE max = 1.4;
        if (frac < min || frac > max)
        {
            std::string msg = string_sprintf("LinearLogistic_Width:: sigma_edge / gradient_std out of range: %g <> [%g, %g]", frac, min, max);
            throw ParameterOutOfRangeException(msg.c_str());
        }
        std::vector<DTYPE> p { -6.37693335e+02,  7.65858904e+02, -3.52515049e+02,  7.46853849e+01,
                               -8.05286823e+00,  3.99631414e-01,  3.25619440e-01};
        DTYPE r = 3 * polyval(p, frac);
        return r;
    }

public:
    LinearLogistic_Width(DTYPE width, DTYPE sigma_edge, DTYPE _threshold = 0.0):
        _LinearLogistic_(width * __scale__(width, sigma_edge), sigma_edge, _threshold)
    {}
    virtual ~LinearLogistic_Width() = default;
protected:
    LinearLogistic_Width(const LinearLogistic_Width &other) = default;
    LinearLogistic_Width& operator =(const LinearLogistic_Width& other) = default;
};


#endif // TAPER_LINEARLOGISTIC_H
