/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */
#ifndef H_IO_NODE_H
#define H_IO_NODE_H

#include <stdlib.h>
#include <stdio.h>
#include "../Common/dtype.h"

template <typename T>
class _H_IO_Node_
{
public:
    _H_IO_Node_()
    {
        _in = 0.0;
        _out= 0.0;
    }

    _H_IO_Node_(const _H_IO_Node_ &other) = default;
    _H_IO_Node_& operator =(const _H_IO_Node_& other) = default;
    virtual ~_H_IO_Node_() = default;

    virtual void reset()
    {
        _in  = 0.0;
        _out = 0.0;
    }

    virtual void reset_input()
    {
        _in  = 0.0;
    }

    virtual void input_add(DTYPE val)
    {
        _in += val;
    }

    T in() const
    {
        return _in;
    }

    virtual T out() const
    {
        return _out ;
    }

protected:
    T _in, _out;
};

typedef _H_IO_Node_<DTYPE>  H_IO_Node_DTYPE;
#endif // H_IO_NODE_H
