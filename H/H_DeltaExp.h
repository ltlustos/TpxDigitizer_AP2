#ifndef H_DELTAEXP_H
#define H_DELTAEXP_H
/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#include "H/H_Atomic.h"
class H_DeltaExp: public H_Atomic
{
public:
    H_DeltaExp(DTYPE tau1, DTYPE tau2, DTYPE delta_t, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle)
    {
        _tau1_ = tau1;
        _tau2_ = tau2;
        _dt_ = delta_t;
        _beta1_ = std::exp(-_dt_ /_tau1_);
        _beta2_ = std::exp(-_dt_ /_tau2_);
        _initialize_();
        __idle__ = true;
    }

    virtual ~H_DeltaExp() = default;
    H_DeltaExp(const H_DeltaExp &other) = default;
    H_DeltaExp& operator =(const H_DeltaExp& other) = default;

    virtual void normalize_integral_h() override
    {
        _initialize_();
        normalize(std::abs((_beta1_-_beta2_) *_dt_ / (_tau2_-_tau1_)));
    }

    void normalize_max_h() override
    {
        _initialize_();
        DTYPE norm = std::exp(_tau1_*std::log(_tau2_/_tau1_)/(_tau1_ - _tau2_)) - std::exp(_tau2_*std::log(_tau2_/_tau1_)/(_tau1_ - _tau2_));
        normalize(std::abs((_beta1_-_beta2_)  / norm));
    }

    DTYPE tau1() const
    {
        return _tau1_;
    }

    DTYPE tau2() const
    {
        return _tau1_;
    }
private:
    void _initialize_()
    {
        DTYPE  temp_num_coeff  [1] = {-_beta1_+_beta2_};
        DTYPE  temp_denom_coeff [3] = {1.0, -_beta1_-_beta2_, _beta1_*_beta2_};
        __initialize_h__(1,
                         3,
                         temp_num_coeff,
                         temp_denom_coeff);
    }
    DTYPE _tau1_;
    DTYPE _tau2_;
    DTYPE _beta1_;
    DTYPE _beta2_;
    DTYPE _dt_;
};



#endif // H_DELTAEXP_H
