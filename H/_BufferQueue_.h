/* Mpx3Sim ver. 0.1, 2018.08.27 */
/* Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef _BUFFERQUEUE__H
#define _BUFFERQUEUE__H

#include "Common/dtype.h"
#include <stdlib.h>
#include <cmath>
#include <limits>
#include <string.h>
#include "Common/Exceptions.h"
#include <vector>
#include <cstring>
#include <algorithm>
#include "stdio.h"
#include <omp.h>

template < typename T>
class _BufferQueue_
{

protected:
    T* _buffer_{nullptr};
    std::size_t _size_;
    std::size_t _byte_size_;
    std::size_t _size_of_T_;
    std::size_t _idx_tail_;

public:
    _BufferQueue_()
    {
        _buffer_    = nullptr;
        _size_      = 0;
        _byte_size_ = 0;
        _size_of_T_ =  sizeof(T);
    }

    _BufferQueue_(T* source, const std::size_t n)
    {
        initialize(source, n);
    }

    ~_BufferQueue_()
    {
        _clear_();
    }

    void _clear_()
    {
        delete [] _buffer_;
        _buffer_ = nullptr;
        _size_ = 0;
        _byte_size_ = 0;
    }

    _BufferQueue_(const _BufferQueue_ &other)
    {
        *this = other;
    }

    _BufferQueue_& operator =(const _BufferQueue_& other)
    {
        if (this == &other)
            return *this;
        if (other._size_ > 0)
            initialize(other._buffer_, other._size_);
        return *this;
    }

    void print() const
    {
        for(std::size_t i=0; i<_size_; ++i)
            printf("%10.5e ",  _buffer_[i]);
        printf("\n");
    }


    std::size_t size() const
    {
        return _size_;
    }

    virtual T& operator[] (std::size_t index)  const
    {
        return _buffer_[index];
    }

    virtual T& head()
    {
        return _buffer_[0];
    }

    virtual T& tail()
    {
        return _buffer_[_idx_tail_];
    }

    void initialize(T* source, std::size_t n)
    {
        if (source == _buffer_)
            return;
        _clear_();
        if (n==0)
            throw OutOfRangeException("size zero no good for buffer\n");
        resize(n);
        memcpy(_buffer_, source, _byte_size_);
    }

    void resize (std::size_t n)
    {
        if (n==0)
            throw OutOfRangeException("size zero no good for buffer\n");
        if (n == _size_)
            return;
        delete [] _buffer_;
        _buffer_ = new T[n];
        _size_ = n;
        _byte_size_ = _size_ * _size_of_T_;
        _idx_tail_ = _size_ -1;
    }

    void set(T val)
    {
        memset(_buffer_, val, _byte_size_);
    }

    void set_zero()
    {
        memset(_buffer_, T(0), _byte_size_);
    }

    virtual void push(T val)
    {
        // shift right
        memmove(_buffer_ + 1, _buffer_, _size_of_T_ *(_idx_tail_));
        _buffer_[0] = val;
    }

    void multiply(T val)
    {
#pragma omp simd
        for(std::size_t i=0; i<_size_; ++i)
            _buffer_[i] *= val;
    }

    void add(T val)
    {
#pragma omp simd
        for(std::size_t i=0; i<_size_; ++i)
            _buffer_[i] += val;
    }

    T sum() const
    {
        T s = 0.0;
#pragma omp simd reduction(+ : s)
        for(std::size_t i=0; i<_size_; ++i)
            s += _buffer_[i];
        return s;
    }

    T sum2() const
    {
        T s = 0.0;
#pragma omp simd reduction(+ : s)
        for(std::size_t i=0; i<_size_; ++i)
            s += _buffer_[i] * _buffer_[i];
        return s;
    }

    T norm() const
    {
        return std::sqrt(sum2());
    }

    virtual T max(std::size_t *idx_max = nullptr)
    {
        T amax = 0;
        std::size_t idx = 0;
        for(std::size_t i=0;i<_size_; ++i){
            if (amax < _buffer_[i]){
                amax = _buffer_[i];
                idx = i;
            }
        }
        if (idx_max != nullptr)
            *idx_max = idx;
        return amax;
    }

    T inner( const _BufferQueue_ &other){
        T s = 0.0;
#pragma omp simd reduction(+ : s)
        for(std::size_t i=0; i<_size_; ++i)
            s += _buffer_[i] * other[i];
        return s;
    }
};

#endif // _BUFFERQUEUE__H
