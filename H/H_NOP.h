#pragma once
#include "H_Atomic.h"
#include "Common/dtype.h"
class H_NOP: public H_Atomic
{
public:
    H_NOP()
        :H_Atomic(0, false){
        _out = 0.0;
    }

    virtual ~H_NOP() = default;
    H_NOP(const H_NOP &other) = default;
    H_NOP& operator =(const H_NOP& other) = default;


    void normalize_max_h() override {}
    void normalize_integral_h() override {}

    virtual DTYPE process() override
    {
       return _out;
    }

    virtual DTYPE process_input(DTYPE in) override
    {
        return _out;
    }

};

