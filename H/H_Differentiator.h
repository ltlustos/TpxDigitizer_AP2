/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */
#ifndef H_DIFF_H
#define H_DIFF_H

#include "H/H_Atomic.h"
class H_Differentiator: public H_Atomic
{
public:
    H_Differentiator(DTYPE tauD, DTYPE delta_t, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle)
    {
        _tau_ = tauD;
        _dt_ = delta_t;
        _beta_ = std::exp(-delta_t /_tau_);
        DTYPE  temp_num_coeff   [1] = {1.0};
        DTYPE  temp_denom_coeff [2] = {1.0, -_beta_};
        __initialize_h__(1,
                         2,
                         temp_num_coeff,
                         temp_denom_coeff);
        __idle__ = true;
    }
    H_Differentiator(const H_Differentiator &other) = default;
    H_Differentiator& operator =(const H_Differentiator& other) = default;
    virtual ~H_Differentiator() = default;

    virtual void normalize_integral_h() override
    {
        normalize(1-_beta_);
    }

    DTYPE beta() const
    {
        return _beta_;
    }

    DTYPE tau() const
    {
        return _tau_;
    }
    //private:
    DTYPE _tau_;
    DTYPE _dt_;
    DTYPE _beta_;
};



class H_Saturated_Differentiator: public H_Differentiator
{
public:
    H_Saturated_Differentiator(
        DTYPE tauD,
        DTYPE delta_t,
        DTYPE amplitude,
        DTYPE idle_cutoff,
        bool reset_on_idle)
        :H_Differentiator(tauD, delta_t, idle_cutoff, reset_on_idle)
    {
        _amplitude_ = amplitude;
    }

    H_Saturated_Differentiator(const H_Saturated_Differentiator &other)
        :H_Differentiator(other)
    {
        *this = other;
    }

    virtual ~H_Saturated_Differentiator() = default;
    H_Saturated_Differentiator& operator =(const H_Saturated_Differentiator& other) = default;

    void input_add(DTYPE in)
    {
        H_IO_Node_DTYPE::input_add(in);
    }

    DTYPE process_input(DTYPE in)
    {
        input_add(in);
        return process();
    }

    virtual DTYPE process()
    {
        DTYPE delta = __out_buffer__[0] * __denom_coeff__[1];
        DTYPE out = _in * __num_coeff__[0] - delta;
        if (out > _amplitude_)
            _in = std::max(0.0, std::min( ( _amplitude_ + delta) / __num_coeff__[0], _in));
        else if (out < -_amplitude_)
            _in = std::min(0.0, std::max( (-_amplitude_ + delta) / __num_coeff__[0], _in));
        return H_Atomic::process();
    }

private:
    DTYPE _amplitude_;
};

#endif // H_DIFF_H
