#ifndef H_RAMP_H
#define H_RAMP_H
#include "H/H_Atomic.h"
#include "Common/dtype.h"
#include <array>
#include <vector>
class H_Ramp: public H_Atomic
{
public:
    H_Ramp(DTYPE tau, DTYPE delta_t, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle){

        _tau_ = tau;
        _dt_ = delta_t;
        size_t N = size_t(_tau_/_dt_);

        std::vector<DTYPE>  temp_num_coeff= {0.0};
        std::array<DTYPE, 2>  temp_denom_coeff = {1.0, -1.0};
        temp_num_coeff.resize(N);
        std::fill(temp_num_coeff.begin(), temp_num_coeff.end(), 1.0 / N);
        __initialize_h__(N,
                         2,
                         &temp_num_coeff[0],
                &temp_denom_coeff[0]);
        __idle__ = true;
        normalize_int_h();
    }

    virtual ~H_Ramp()
    {}

    H_Ramp(const H_Ramp &other) = default;
    H_Ramp& operator =(const H_Ramp& other) = default;


    void normalize_max_h() override
    {
        __num_coeff__.set(1.0);
    }

    virtual void normalize_int_h()
    {
        __num_coeff__.set(1.0 / DTYPE(__nnum__));
    }

    DTYPE tau() const
    {
        return _tau_;
    }

    DTYPE process_input(DTYPE in) override
    {
        input_add(in);
        return process();
    }

    virtual DTYPE process() override
    {

        DTYPE isum = __in_buffer__.sum();
        if (_in !=0 && (isum >= 1 || _out >= 1))
        {
            _in = 0;
        }
        if (_out > 1)
            _in = (1 - _out);
        else if (_out <0)
            _in = - 0.1*_out ;
        return H_Atomic::process();
    }


private:
    DTYPE _tau_;
    DTYPE _dt_;
};


#endif // H_RAMP_H
