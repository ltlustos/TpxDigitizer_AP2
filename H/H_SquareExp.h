#ifndef H_SQUAREEXP_H
#define H_SQUAREEXP_H
/* MpxSim 2022, Lukas Tlustos, CERN, IEAP/CTU */

#include "H/H_Atomic.h"
class H_SquareExp: public H_Atomic
{
public:
    H_SquareExp(DTYPE tau, DTYPE delta_t, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle)
    {
        _tau_ = tau;
        _dt_ = delta_t;
        _beta_ = std::exp(-_dt_ /_tau_);
        _initialize_();
        __idle__ = true;
    }

    virtual ~H_SquareExp() = default;
    H_SquareExp(const H_SquareExp &other) = default;
    H_SquareExp& operator =(const H_SquareExp& other) = default;

    virtual void normalize_integral_h() override
    {
        _initialize_();
        normalize(__num_coeff__[0]*_dt_ / (1.0 * cube(_tau_)));
    }

    void normalize_max_h() override
    {
        _initialize_();
    }

    DTYPE tau() const
    {
        return _tau_;
    }

private:
    void _initialize_()
    {
        DTYPE beta_square = _beta_*_beta_;
        DTYPE dt_square = _dt_ * _dt_;
        DTYPE beta_cube = _beta_*_beta_*_beta_;
        DTYPE  temp_num_coeff   [2] = {_beta_ *dt_square, dt_square * beta_square};
        DTYPE  temp_denom_coeff [4] = {1.0, -3*_beta_, 3*beta_square, -beta_cube};
        __initialize_h__(2,
                         4,
                         temp_num_coeff,
                         temp_denom_coeff);
    }
    DTYPE _tau_;
    DTYPE _beta_;
    DTYPE _dt_;
};

#endif // H_SQUAREEXP_H
