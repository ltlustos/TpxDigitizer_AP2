#pragma once

#include "H/H_Atomic.h"
class H_ConjPole: public H_Atomic
{
public:
    H_ConjPole(DTYPE tauD, DTYPE tauC, DTYPE delta_t, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle)
    {
        _taud_ = tauD;
        _tauc_ = tauC;
        _dt_ = delta_t;
        _alpha_ = std::cos(delta_t /_tauc_);
        _beta_ = std::exp(-delta_t /_taud_);

        __initialize_h__( {1.0, - _alpha_ * _beta_},  {1.0, -2*_alpha_ * _beta_, _alpha_ * _alpha_});
        __idle__ = true;
    }
    H_ConjPole(const H_ConjPole &other) = default;
    H_ConjPole& operator =(const H_ConjPole& other) = default;
    virtual ~H_ConjPole() = default;

    virtual void normalize_integral_h() override
    {
        normalize(1-_beta_);  // normalizing like simple real pole
    }

    DTYPE beta() const
    {
        return _beta_;
    }

    DTYPE alpha() const
    {
        return _alpha_;
    }

    DTYPE tau_d() const
    {
        return _taud_;
    }

    DTYPE tau_c() const
    {
        return _tauc_;
    }

    //private:
    DTYPE _taud_{0}, _tauc_{0};
    DTYPE _dt_;
    DTYPE _alpha_;
    DTYPE _beta_;
};
