#ifndef _H_Composite__H
#define _H_Composite__H

#include "H/H_Atomic.h"
#include "H/_H_.h"
class _H_Composite_: public _H_,public H_IO_Node_DTYPE
{
public:
    _H_Composite_():_H_()
    {
    }

    _H_Composite_(const _H_Composite_ &other)
        :_H_(other), H_IO_Node_DTYPE(other)
    {
        *this = other;
    }

    _H_Composite_& operator =(const _H_Composite_& other)
    {
        if (this == &other)
            return *this;
        _H_::operator=(other);
        H_IO_Node_DTYPE::operator=(other);
        _stages_ = other._stages_;
        return *this;
    }

    virtual ~_H_Composite_()
    {
        while (!_stages_.empty())
        {
            delete _stages_.back();
            _stages_.pop_back();
        }
    }
    void add(_H_ *h)
    {
        _stages_.emplace_back(h);
    }

    virtual void reset() override
    {
        for (auto s : _stages_)
            s->reset();
    }


    virtual DTYPE process() override
    {
        throw SubclassResponsabilityException("_H_Composite_:process");
    }

    virtual DTYPE process_input(DTYPE in) override
    {
        _in = in;
        return process();
    }

    virtual void set_idle(bool state) override
    {
        for (auto s : _stages_)
            s->set_idle(state);
    }

    virtual bool is_idle() const override
    {
        bool val = true;
        for (auto s : _stages_)
            val &= s->is_idle();
        return val;
    }

    virtual void invert() override
    {
        for (auto s : _stages_)
            s->invert();
    }

protected:
    std::vector<_H_*> _stages_;

};

// #######################################################################
class H_Composite_Serial: public _H_Composite_
{
public:
    H_Composite_Serial():_H_Composite_()
    {
    }

    H_Composite_Serial(const H_Composite_Serial &other)
        :_H_Composite_(other)
    {
        *this = other;
    }

    H_Composite_Serial& operator =(const H_Composite_Serial& other)
    {
        if (this == &other)
            return *this;
        _H_Composite_::operator=(other);
        return *this;
    }



    virtual DTYPE process() override
    {
        DTYPE val = _in;
        for (std::size_t i =0; i<_stages_.size(); ++i)
            val = _stages_[i]->process_input(val);
        _out = val;
        return _out;
    }
};

// #########################################################################
// #######################################################################
class H_Composite_Parallel: public _H_Composite_
{
public:
    H_Composite_Parallel():_H_Composite_()
    {
    }

    H_Composite_Parallel(const H_Composite_Parallel &other)
        :_H_Composite_(other)
    {
        *this = other;
    }

    virtual ~H_Composite_Parallel()
    {

    }
    H_Composite_Parallel& operator =(const H_Composite_Parallel& other)
    {
        if (this == &other)
            return *this;
        _H_Composite_::operator=(other);
        _weights_ = other._weights_;
        return *this;
    }


    void add(_H_ *h, DTYPE weight)
    {
        _H_Composite_::add(h);
        _weights_.emplace_back(weight);
    }

    virtual DTYPE process_input(DTYPE in) override
    {
	_in = in;
	return process();
    }

    virtual DTYPE process() override
    {
	_out = 0.0;
	DTYPE temp = 0.0;
        for (std::size_t i =0; i<_stages_.size(); ++i)
	{
	    temp =  _weights_[i] * _stages_[i]->process_input(_in);
	    _out += temp;
	}
	return _out;
    }


protected:
    std::vector<DTYPE> _weights_;
};
#endif // _H_Composite__H
