/* Mpx3Sim ver. 0.1, 2018.08.27 */
/* Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef _H_BASE_
#define _H_BASE_

#include "Common/dtype.h"
#include <stdlib.h>
#include <cmath>
#include <limits>
#include <string.h>
#include "Common/Exceptions.h"
#include "Util/u_math.h"
#include "_BufferQueue_.h"

#include "_H_.h"

template < typename T>
class _T_HBase_: public _H_
{

protected:
    std::size_t __nnum__;
    std::size_t __ndenom__;

    _BufferQueue_<T> __num_coeff__;
    _BufferQueue_<T> __denom_coeff__;
    _BufferQueue_<T> __in_buffer__;
    _BufferQueue_<T> __out_buffer__;

    bool _is_normalized_;
    std::size_t __n_sync_idle__;
    T __idle_cutoff2__;
    bool __idle__;

private:
    std::size_t _sync_count_;
    T __load2sum__;
    bool __reset_on_idle__{true};

public:

    _T_HBase_(T idle_cutoff, bool reset_on_idle)
    {
        _is_normalized_ = false;
        __idle_cutoff2__ = square(idle_cutoff);
        __idle__ = false;
        _sync_count_ = 0;
        __reset_on_idle__ = reset_on_idle;
    }

    _T_HBase_(const _T_HBase_ &other) = default;
    _T_HBase_& operator =(const _T_HBase_& other) = default;

protected:
    ////....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    _T_HBase_(std::size_t nnum, std::size_t ndenom, T * num_coeff, T *denom_coeff, DTYPE idle_cutoff, bool reset_on_idle)
        :_T_HBase_(idle_cutoff, reset_on_idle)
    {
        __initialize_h__(nnum, ndenom, num_coeff, denom_coeff);
    }

    _T_HBase_(std::vector<T> num_coeff, std::vector<T> denom_coeff, DTYPE idle_cutoff, bool reset_on_idle)
        :_T_HBase_(idle_cutoff, reset_on_idle)
    {
        __initialize_h__(num_coeff, denom_coeff);
    }


public:
    ////....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    ~_T_HBase_(){
        free();
    }

    ////....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    void free(){

    }
    ////....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    std::size_t n_num_(void) const {
        return __nnum__;
    }
    ////....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    std::size_t n_denom(void) const{
        return __ndenom__;
    }
    ////....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


    void print() const
    {
        printf("numerator\n");
        __num_coeff__.print();
        printf("denominator\n");
        __denom_coeff__.print();
    }

    void print_buffers() const
    {
        printf("in_buffer\n");
        __in_buffer__.print();
        printf("out_buffer\n");
        __out_buffer__.print();
    }

    void invert() override
    {
        __in_buffer__.multiply(-1.0);
        __out_buffer__.multiply(-1.0);
    }

    virtual bool is_idle() const override
    {
        return __idle__;
    }

    virtual bool is_active() const {
        return !__idle__;
       // return __num_coeff__[0] !=0;  // input factor == 0 -> inactive
    }

    virtual bool set_active(bool val){
        return __idle__ = ! val;
    }

    T _out_load_2_(){
        return __out_buffer__.sum2();
    }

    void set_idle(bool flag) override
    {
        __idle__ = flag;
    }

    void scale(T s){
        __in_buffer__.multiply(s);
        __out_buffer__.multiply(s);
    }

    void reset() override
    {
        _reset_();
    }
    DTYPE load2sum() const
    {
        return __load2sum__;
    }

    DTYPE load2sum_out() const
    {
        return __out_buffer__.sum2();
    }

    DTYPE load2sum_in() const
    {
        return __in_buffer__.sum2();
    }

    DTYPE loadsum_in() const
    {
        return __in_buffer__.sum();
    }

protected:
    ////....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    ///
    ///
    ////....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    void __initialize_h__(std::vector<T> _num_coeff, std::vector<T> _denom_coeff)
    {
        __initialize_h__(_num_coeff.size(), _denom_coeff.size(), _num_coeff.data(), _denom_coeff.data());
    }


protected:
    ////....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    void __initialize_h__(std::size_t _nnum,
                          std::size_t _ndenom,
                          T * _num_coeff,
                          T *_denom_coeff)
    {
        free();
        __nnum__ = _nnum;
        __ndenom__ = _ndenom;
        __num_coeff__.initialize(_num_coeff, __nnum__);
        __denom_coeff__.initialize(_denom_coeff, __ndenom__);
        if (__nnum__)
        {
            __in_buffer__.resize(__nnum__);
            __in_buffer__.set_zero();
        }

        if (__ndenom__)
        {
            __out_buffer__.resize(__ndenom__);
            __out_buffer__.set_zero();
        }
        __n_sync_idle__ = 2 * std::max(__ndenom__, __nnum__);

        __num_coeff__.multiply(1.0/__denom_coeff__[0]);
        __denom_coeff__.multiply(1.0/__denom_coeff__[0]);

        _reset_();
    }
    void _reset_()
    {
        __in_buffer__.set_zero();
        __out_buffer__.set_zero();
        __idle__ = true;
        _sync_count_ = 0;
        __load2sum__ = 0;
    }


    ////....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    DTYPE _process_(T aninput)
    {
        bool input_activates = square(aninput) > __idle_cutoff2__;
        if ( !__idle__ && !(_sync_count_ % __n_sync_idle__))
        {
            _sync_count_ = 0;
            __load2sum__ = __out_buffer__.sum2();
            __load2sum__ += __in_buffer__.sum2();
            __load2sum__ += aninput * aninput;
#ifdef __DEBUG__
            if (ISNAN(__load2sum__))
                throw ThisShouldNotHappenException("_T_HBase_: __load2sum__ == nan");
#endif
            __idle__ = __load2sum__ < __idle_cutoff2__ && !input_activates;
            if (__idle__ && __reset_on_idle__)
                reset();
        }
        __idle__ = __idle__ && !input_activates;
        ++_sync_count_ ;

        if (__idle__)
            return DTYPE(0);
        else
        {
            // shift right
            __in_buffer__.push(aninput);
            __out_buffer__.push(0.0);
            //calc next value
            T val = __num_coeff__.inner(__in_buffer__);

#ifdef __DEBUG__
            if (ISNAN(val))
                throw ThisShouldNotHappenException("_T_HBase_: val == nan");
#endif

            val -= __denom_coeff__.inner(__out_buffer__);
#ifdef __DEBUG__
            if (ISNAN(val))
                throw ThisShouldNotHappenException("_T_HBase_: val == nan");
#endif
            __out_buffer__[0] = val; // append left
            return val;
        }
    }
};

typedef _T_HBase_<DTYPE> _HBase_DTYPE_;
#endif // _H_BASE_
