#ifndef H_GAUSSIAN_H
#define H_GAUSSIAN_H
#include <math.h>
#include "H/H_Atomic.h"
class H_Gaussian: public H_Atomic
{
public:
    H_Gaussian(DTYPE tau, int order, DTYPE delta_t, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle)
    {
        _tau_ = tau;
        DTYPE a = 1 / _tau_ ;
        DTYPE aT = delta_t / _tau_;
        DTYPE alpha = std::exp( - delta_t/ _tau_ );


        if (order == 1)
        {
            DTYPE  temp_num_coeff[2] = {1, -alpha * (1 + aT)};
            DTYPE temp_denom_coeff[3] = {1, -2 * alpha, alpha * alpha};
            __initialize_h__(2, 3, temp_num_coeff, temp_denom_coeff);
        }
        else if (order == 2)
        {
            DTYPE  temp_num_coeff[3] = {0,
                                        alpha * delta_t * (2 - aT),
                                        -alpha * alpha * delta_t * (2 + aT)};

            DTYPE temp_denom_coeff[4] = { 2,
                                          - 6 * alpha,
                                          6 * alpha * alpha,
                                          -2 * std::pow(alpha,3)};
            __initialize_h__(3, 4, temp_num_coeff, temp_denom_coeff);

        }

        else if (order == 3)
        {
            DTYPE  temp_num_coeff[4] = {0,
                                        alpha * delta_t * delta_t * (3 - aT),
                                        -4 * a * std::pow(delta_t, 3) * std::pow(alpha,2),
                                        std::pow(delta_t, 3) * std::pow(alpha,3)};

            DTYPE temp_denom_coeff[5] = { 6,
                                          - 24*alpha,
                                          36 * std::pow(alpha,2),
                                          - 24 * std::pow(alpha,3),
                                          6 * std::pow(alpha,4)
                                        };
            __initialize_h__(2, 4, temp_num_coeff, temp_denom_coeff);
        }
    }
    H_Gaussian(const H_Gaussian &other) = default;
    H_Gaussian& operator =(const H_Gaussian& other) = default;
    ~H_Gaussian() = default;

private:
    DTYPE _tau_;
};
#endif // H_GAUSSIAN_H
