/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */
#ifndef H_INTEGRATOR_H
#define H_INTEGRATOR_H
#include "H/H_Atomic.h"
#include "Common/dtype.h"

class H_Integrator: public H_Atomic
{
public:
    H_Integrator(DTYPE tauD, DTYPE delta_t, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle){

        _tau_ = tauD;
        _dt_ = delta_t;
        _beta_ = std::exp(-_dt_ /_tau_);
        initialize_coeff();
        __idle__ = true;
        normalize_integral_h();
    }

    virtual ~H_Integrator()
    {}

    void initialize_coeff()
    {

        DTYPE  temp_num_coeff   [1] = {1-_beta_,};
        DTYPE  temp_denom_coeff [3] = {1.0, -1-_beta_, _beta_};
        __initialize_h__(1,
                         3,
                         temp_num_coeff,
                         temp_denom_coeff);
    }

    H_Integrator(const H_Integrator &other)
        :H_Atomic(other)
    {
        *this = other;
    }

    H_Integrator& operator =(const H_Integrator& other)
    {
        if (this == &other)
            return *this;
        H_Atomic::operator=(other);
        _tau_ = other._tau_;
        _dt_ = other._dt_;
        return *this;
    }

    virtual void normalize_integral_h() override
    {
        normalize(1-_beta_);
    }

    DTYPE tau() const
    {
        return _tau_;
    }

    DTYPE beta() const
    {
        return _beta_;
    }
private:
    DTYPE _tau_;
    DTYPE _beta_;
    DTYPE _dt_;
};



class H_Saturated_Integrator: public H_Integrator
{
public:
    H_Saturated_Integrator(DTYPE tauD, DTYPE delta_t, DTYPE min, DTYPE max, DTYPE idle_cutoff, bool reset_on_idle):
        H_Integrator(tauD, delta_t, idle_cutoff, reset_on_idle)
    {
        _max_ = max;
        _min_ = min;
    }


    H_Saturated_Integrator(const H_Saturated_Integrator &other)
        :H_Integrator(other)
    {
        *this = other;
    }

    H_Saturated_Integrator& operator =(const H_Saturated_Integrator& other) = default;
    virtual ~H_Saturated_Integrator() = default;

    void input_add(DTYPE in)
    {
        H_IO_Node_DTYPE::input_add(in);
    }

    DTYPE process_input(DTYPE in)
    {
        input_add(in);
        return process();
    }

    virtual DTYPE process()
    {
        DTYPE delta = __out_buffer__[0] * __denom_coeff__[1] + __out_buffer__[1] * __denom_coeff__[2];
        DTYPE out = _in * __num_coeff__[0] - delta;
        if (out > _max_)
            _in = std::min( ( _max_ + delta) / __num_coeff__[0], _in);
        else if (out < _min_)
            _in = std::max( (-_min_ + delta) / __num_coeff__[0], _in);
        return H_Atomic::process();
    }

private:
    DTYPE _min_, _max_;
};

#endif // H_INTEGRATOR_H
