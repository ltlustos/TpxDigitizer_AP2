/* Mpx3Sim ver. 0.1, 2018.08.27 */
/* Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef H_ATOMIC_H
#define H_ATOMIC_H

#include <stdlib.h>
#include <stdio.h>
#include "_T_HBase_.h"
#include "_H_IO_Node_.h"



class H_Atomic: public _HBase_DTYPE_, public H_IO_Node_DTYPE
{

protected:
    H_Atomic(DTYPE idle_cutoff, bool reset_on_idle)
        :_HBase_DTYPE_(idle_cutoff, reset_on_idle)
    {}

public:

    H_Atomic(std::vector<DTYPE> num_coeff, std::vector<DTYPE> denom_coeff, DTYPE idle_cutoff, bool reset_on_idle):
        _HBase_DTYPE_(num_coeff, denom_coeff, idle_cutoff, reset_on_idle)
    {}

    H_Atomic(std::size_t nnum, std::size_t ndenom, DTYPE* num_coeff, DTYPE* denom_coeff, DTYPE idle_cutoff, bool reset_on_idle):
        _HBase_DTYPE_(nnum, ndenom, num_coeff, denom_coeff, idle_cutoff, reset_on_idle)
    {}

    virtual ~H_Atomic(){}
    H_Atomic(const H_Atomic &other) = default;
    H_Atomic& operator =(const H_Atomic& other) = default;


    void reset() override
    {
        _HBase_DTYPE_::_reset_();
        H_IO_Node_DTYPE::reset();
    }

    void input_add(DTYPE val) override
    {
        H_IO_Node_DTYPE::input_add(val);
        __idle__ &= (val==0);
    }

    virtual DTYPE process() override
    {
        _out = _HBase_DTYPE_::_process_(_in);
        H_IO_Node_DTYPE::reset_input();
        return _out;
    }

    virtual DTYPE process_input(DTYPE in) override
    {
        // if (in !=0)
        //     set_active(true);
        H_IO_Node_DTYPE::input_add(in);
        return process();
    }

    void normalize(DTYPE val)
    {
        __num_coeff__[0] = val;
    }

    virtual void normalize_max_h()
    {
        normalize(1.0);
    }

    virtual void normalize_integral_h(){
        throw SubclassResponsabilityException("H_Atomic:normalize_integral_h");
    }

    DTYPE in_sum()
    {
        return __in_buffer__.sum();
    }

    DTYPE out_sum()
    {
        return __out_buffer__.sum();
    }
};

#endif // H_ATOMIC_H
