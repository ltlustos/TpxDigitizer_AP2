/* Mpx3Sim ver. 0.1, 2018.08.27 */
/* Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef H_H
#define H_H
#include "Common/Exceptions.h"
#include "Common/dtype.h"
class _H_
{
public:
    _H_()
    {}

    virtual ~_H_()
    {}

    _H_(const _H_ &other)
    {
        *this = other;
    }

    _H_& operator =(const _H_& other)
    {
        if (this == &other)
            return *this;
        return *this;
    }

    virtual void reset()
    {
        throw SubclassResponsabilityException("H:reset");
    }


    virtual DTYPE process()
    {
        throw SubclassResponsabilityException("H:process");
        return 0;
    }

    virtual DTYPE process_input(DTYPE)
    {
        throw SubclassResponsabilityException("H:process_input");
        return 0;
    }

    virtual void set_idle(bool)
    {
        throw SubclassResponsabilityException("H:set_idle");
    }

    virtual bool is_idle()  const
    {
        throw SubclassResponsabilityException("H:is_idle");
        return true;
    }

    virtual void invert()
    {
        throw SubclassResponsabilityException("H:invert");

    }

};

#endif // H_H
