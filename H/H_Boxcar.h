#ifndef H_BOXCAR_H
#define H_BOXCAR_H
#include "H/H_Atomic.h"
#include "Common/dtype.h"
#include <array>
#include <vector>
class H_Boxcar: public H_Atomic
{
public:
    H_Boxcar(DTYPE tau, DTYPE delta_t, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle){

        _tau_ = tau;
        _dt_ = delta_t;
        size_t N = size_t(_tau_/_dt_);

        std::vector<DTYPE>  temp_num_coeff= {0.0};
        std::array<DTYPE, 2>  temp_denom_coeff = {1.0, -1.0};
        temp_num_coeff.resize(N);
        temp_num_coeff[0] = 1.0 / (N-1);
        temp_num_coeff[N-1] = -1.0 / (N-1);
        __initialize_h__(
                    N,
                    2,
                    &temp_num_coeff[0],
                &temp_denom_coeff[0]);
        __idle__ = true;
        normalize_integral_h();
    }

    virtual ~H_Boxcar()
    {}

    H_Boxcar(const H_Boxcar &other) = default;
    H_Boxcar& operator =(const H_Boxcar& other) = default;


    void normalize_max_h() override {
        __num_coeff__[0] = 1.0;
        __num_coeff__[__nnum__-1] = -1.0;
    }

    void normalize_integral_h() override
    {
        __num_coeff__[0] = 1.0 / DTYPE(__nnum__-1);
        __num_coeff__[__nnum__-1] = -1.0 / DTYPE(__nnum__-1);
    }

    DTYPE tau() const
    {
        return _tau_;
    }
private:
    DTYPE _tau_;
    DTYPE _dt_;
};

#endif // H_BOXCAR_H
