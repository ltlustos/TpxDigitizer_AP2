/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */
/* based on https://exstrom.com/journal/sigproc/dsigproc.html */
#pragma once
#include "IIR1/Iir.h"
#include "H_Atomic.h"



class _H_BW_LP1_: public H_Atomic
{
public:
    _H_BW_LP1_(std::size_t n, DTYPE f_s, DTYPE f_cutoff, std::size_t idx, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle)
    {

        DTYPE alpha = std::tan(M_PI * f_cutoff / f_s);

        DTYPE  temp_num_coeff [2] = {alpha / (1 + alpha), alpha / (1 + alpha)};
        DTYPE  temp_denom_coeff [3] = {1, (alpha-1.0) / (alpha +1.0)};

        __initialize_h__(
                    2,
                    2,
                    temp_num_coeff,
                    temp_denom_coeff);
        __idle__ = false;
    }
    _H_BW_LP1_(const _H_BW_LP1_ &other) = default;
    _H_BW_LP1_& operator =(const _H_BW_LP1_& other) = default;
    virtual ~_H_BW_LP1_() = default;
};



class _H_BW_LP_: public H_Atomic
{
public:
    _H_BW_LP_(std::size_t n, DTYPE f_s, DTYPE f_cutoff, std::size_t idx, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle)
    {

        DTYPE alpha = std::tan(M_PI * f_cutoff / f_s);
        DTYPE alpha2 = square(alpha);

        DTYPE r = std::sin(M_PI * (2.0 * idx + 1.0) / (4.0 * n));
        DTYPE s = alpha2 + 2.0 * alpha * r + 1.0;
        DTYPE a = alpha2 / s;

        DTYPE  temp_num_coeff [3] = {a, 2*a, a};
        DTYPE  temp_denom_coeff [3] = {1, 2.0 * (alpha2 - 1) / s, (alpha2 - 2.0 * alpha * r + 1.0) / s};
        __initialize_h__(3,
                         3,
                         temp_num_coeff,
                         temp_denom_coeff);
        __idle__ = false;
    }
    _H_BW_LP_(const _H_BW_LP_ &other) = default;
    _H_BW_LP_& operator =(const _H_BW_LP_& other) = default;
    virtual ~_H_BW_LP_() = default;
};



// Low Pass Filter
class H_BW_LP
{
public:
    H_BW_LP(std::size_t n,  DTYPE f_s, DTYPE f_cutoff, DTYPE idle_cutoff, bool reset_on_idle)
    {
        for (std::size_t i = 0; i < n; ++i)
        {
            __f__.emplace_back(new _H_BW_LP_(n, f_s, f_cutoff, i, idle_cutoff));
        }
     }

    ~H_BW_LP()
    {
        for(auto f : __f__)
            delete f;
        __f__.clear();
    }
    void reset()
    {
        for(auto f : __f__)
            f->reset();
    }

    bool is_idle()
    {
        bool r = true;
        for(auto f : __f__)
            r = r && f->is_idle();
        return r;
    }

    DTYPE process_input(DTYPE val)
    {
        DTYPE r = val;
        for(auto f : __f__)
            r = f->process_input(r);
        return r;
    }

private:
    std::vector<_H_BW_LP_*> __f__;
};
