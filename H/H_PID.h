/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */
#ifndef H_PID_H
#define H_PID_H

#include "Common/dtype.h"
#include "H/H_Atomic.h"
#include "H/H_Composite.h"
#include "H/H_Differentiator.h"
#include "H/H_Integrator.h"

class H_PID: public H_Atomic
{
public:
    H_PID(DTYPE kp, DTYPE kd, DTYPE ki, DTYPE delta_t, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle)
    {
        _dt_ = delta_t;
        DTYPE  temp_denom_coeff [3] = {kd, kp - 2 * kd, kp + kd +ki};
        DTYPE  temp_num_coeff   [3] = {1.0, -1.0, 0.0};

        __initialize_h__(3,
                         3,
                         temp_num_coeff,
                         temp_denom_coeff);
        __idle__ = false;
    }
    virtual ~H_PID()
    {}

    H_PID(const H_PID &other)
        :H_Atomic(other)
    {
        *this = other;
    }

    H_PID& operator =(const H_PID& other)
    {
        if (this == &other)
            return *this;
        H_Atomic::operator=(other);
        _dt_ = other._dt_;
        return *this;
    }
private:
    DTYPE _dt_;
};


class H_Unity:public H, H_IO_Node_DTYPE
{
public:
    H_Unity()
    {}

    virtual ~H_Unity()
    {}


    H_Unity(const H_Unity &other)
        :H(other), H_IO_Node_DTYPE(other)
    {
        *this = other;
    }

    H_Unity& operator =(const H_Unity& other)
    {
        if (this == &other)
            return *this;
        H::operator=(other);
        H_IO_Node_DTYPE::operator=(other);
        return *this;
    }

    virtual void set_idle(bool val) override
    {
        __idle__ = val;
    }

    virtual bool is_idle() const override
    {
        return __idle__;
    }

    virtual void invert() override
    {
        throw SubclassResponsabilityException("H_PID.invert");

    }
    void reset() override
    {
    }


    virtual DTYPE process() override
    {
        _out = _in;
        return _out;
    }

    virtual DTYPE process_input(DTYPE in) override
    {
        _in = in;
        return process();
    }
private:
    bool __idle__;

};

class H_PIDC: public H_Composite_Parallel
{
public:
    H_PIDC(DTYPE kp, DTYPE kd, DTYPE ki, DTYPE taud, DTYPE taui, DTYPE delta_t, DTYPE idle_cutoff)
        :H_Composite_Parallel()
    {
        add(new H_Unity(), kp);
        add(new H_Differentiator(taud, delta_t, idle_cutoff), kd);
        add(new H_Integrator(taui, delta_t, idle_cutoff), ki);
    }


};


#endif // H_PID_H
