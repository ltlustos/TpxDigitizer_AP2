[frontend]
tpx_def = tpx_base.conf
log_level = STATUS
run_enc = 0  # 0,1: run a 10us noise simulation before running event simulations

pileup_event=0
pileup_period=1.0us
pileup_poisson=false
pileup_alternate_polarity=1

#run_enc = 1
dt = 195.3ps  # timestep of preamp simulation
t0 = 10ns  # run preamp for t0 before the event simulation, assures the buildup of the bw filtered noise
h_cutoff = 1e-3  # cutoff determining how long the preamplifier runs before entering idle state, higher cutoff gives shorter simulation times
t_acq = 15us  # maximum shutter time, event simulation stops when h_cutoff condition is fulfilled or time exceeds t_acq for tacq > 0.
clock_period = 25ns  # ToA clock period
fclock_period = 1.5625ns  # fToA clock period
ufclock_period = 195.3ps  # ufToT clock period
randomize_event_clock_phase = 1 # random time offset for each event within clock_period

#### Peamplifier
tau_integrator = 5.ns  # preamplifier integration time
#gain = 1.0
gain_asymmetry = 2.5perc  # gain mismatch for positive and negative inputs - only of interest for high LET events / heavy ions
gain_dispersion = 0.0perc  # gain dispersion across pixel matrix

noise_ENC = 0.0e  # preampfilier output noise, typically 60e rms
noise_bw = 1e4Hz 1.0e7Hz  # preampfilier output noise bandwidth

#### Feedback

## nonlinear ToT response, eg in https://doi.org/10.1088/1748-0221/13/11/P11014
#fb_ikrum_threshold =    0           200e3       240e3   245e3   555e3       1e6
#fb_ikrum =              1.75e/ns    1.75e/ns    40e/ns  0.5e/ns 0.5e/ns     5e3e/ns

#ss_limit = 1e
#ss_limit_width = 1630e

fb_ikrum = 1.e/ns

fb_ikrum_sigma = 0.0e/ns  # rms dispersion across pixel matrix
fb_ikrum_enc = 0perc
fb_noise_bw = 1e0Hz 1.0e5Hz  # noise bandwidth if alpha_ikrum_noise_enc > 0

fb_taper_width = 1000e  # krummenacher feedback tapering width

fb_tau = 40.0ns   350ns  1.842us
fb_weight =  0.8 1.9 2.15  # relative weights

#fb_tau = 80ns
#fb_weight =  1.0

#### default or volcano  - volcano is still experimental
feedback_type = default
volcano_limit_amplitude = 600e3e  # only used if feedback_type == volcano
volcano_undershoot = -3e3e  # only used if feedback_type == volcano
#volcano_rebound_injection = 0e  # only used if feedback_type == volcano
volcano_inversion_delay = 150ns  # only used if feedback_type == volcano. the time the preamp stays saturated before inverting -> +- width of the first packet

#### LCC
lcc_tau = 10us
lcc_weight = .04

#### Discriminator
disc_type=asym
disc_tau_rise = 1ns  # discriminator rise time
disc_tau_fall = 5ns  # discriminator fall time
disc_glitch_tau = 411ns  # glitch filter / noise count rejection

threshold = 400e  # discriminator threshold
threshold_sigma = 0e  # threshold disperion across pixel matrix
threshold_noise_enc = 0.0e  # lsb_dac * sqrt(2/12) - threshold is given by difference between feedback and threshold DAC, ~7e
threshold_noise_bw = 1e1Hz 1.1e7Hz  # threshold noise bandwidth

#### - output path for png, csv files, relative to the directory of this file
output_path = ./_out/TEMP/
timestamp_output_path = 0   # 0,1: append a timestamp per run to putput path

#### - generate event histograms
output_histogram = 0 #  add histograms to root file
output_single_pixel_histogram = 0
output_csv = 0  # - export histogram data also as csv
output_png = 1  # - export histogram also as png
output_event_plot = 1  # - plot n first preamp events as png, -1 plot all, 0 no output
output_event_csv = 0  # - export n first preamp events as csv, -1 export all, 0 no export
output_listmode = true  # - export hit listmode

output_single_pixel_histogram = false
output_single_pixel_listmode = false
