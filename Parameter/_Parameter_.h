#pragma once
#include "_Parameter_.h"
#include "core/config/Configuration.hpp"
#include "Util/u_string.h"
namespace PARAMETER {



class _Parameter_
{

public:

    _Parameter_(std::string key, bool mandatory, std::string descriptor)
    {
        _key_ = key;
        _mandatory_ = mandatory;
        _descr_ = descriptor;
    }

    ~_Parameter_() = default;
    _Parameter_& operator = (const _Parameter_& o) = default;

    virtual bool from_parameter_file(const allpix::Configuration &config, bool check_used=true)
    {
        return _from_parameter_file_(config, check_used) &&_check_value_();
    }

    virtual std::string key() const
    {
        return _key_;
    }

    virtual std::string set_key(std::string key)
    {
        return _key_ = key;
    }

    virtual void set_descriptor(std::string descriptor)
    {
        _descr_ = descriptor;
    }

    virtual bool is_parsed() const
    {
        return _is_parsed_;
    }
protected:
    std::string _key_{"undefined"};
    bool _is_parsed_{false};
    bool _mandatory_{true};
    std::string _descr_{""};

    _Parameter_()=default;
    virtual bool _from_parameter_file_(const allpix::Configuration &config, bool check_used) = 0;
    virtual bool _check_value_() const = 0;
};
}
