/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#pragma once
#include "Common/Exceptions.h"
#include "Util/u_string.h"
#include "Common/dtype.h"
#include "core/utils/log.h"
#include "core/config/Configuration.hpp"
#include "core/utils/unit.h"

class _Parameter_
{
public:
    _Parameter_()
    {}

    virtual void adjust_values()
    {}

    virtual void adjust_timestep_values(DTYPE __attribute__((unused)) dt_s)
    {}

    virtual void check_values() const
    {
        throw SubclassResponsabilityException("_Parameter_:check_values");
    }

    virtual void check_values(__attribute__((unused)) DTYPE dt_sec) const
    {}

    virtual void parse_config(__attribute__((unused)) const allpix::Configuration&)
    {
        throw SubclassResponsabilityException("_Parameter_:parse_config");
    }

protected:

    template <typename T>
    void _check_le_(T val, T ref, const std::string name, const std::string unit) const
    {
        if (val > ref)
            throwException(string_sprintf("%s out of range: %g > %g [%s]", name.c_str(), val, ref, unit.c_str()));
    }

    template <typename T>
    void _check_lt_(const T val, const T ref, std::string name, std::string unit) const
    {
        if (val >= ref)
            throwException(string_sprintf("%s out of range: %g >= %g [%s]", name.c_str(), val, ref, unit.c_str()));
    }

    template <typename T>
    void _check_ge_(const T val, const T ref, std::string name, std::string unit) const
    {
        if (val < ref)
            throwException(string_sprintf("%s out of range: %g < %g [%s]", name.c_str(), val, ref, unit.c_str()));
    }

    template <typename T>
    void _check_gt_(const T val, const T ref, std::string name, std::string unit) const
    {
        if (val <= ref)
            throwException(string_sprintf("%s out of range: %g <= %g [%s]", name.c_str(), val, ref, unit.c_str()));
    }


    template <typename T>
    void _check_in_range_(const T val, const T min, const T max, std::string name, std::string unit) const
    {
        if (val < min || val > max)
            throwException(string_sprintf("%s out of range: %g <> [%g, %g] [%s]", name.c_str(), val, min, max, unit.c_str()));
    }

    template <typename T>
    void _check_within_range_(const T val, const T min, const T max, std::string name, std::string unit) const
    {
        if (val <= min || val >= max)
            throwException(string_sprintf("%s out of range: %g <> (%g, %g) [%s]", name.c_str(), val, min, max, unit.c_str()));
    }

protected:
    void throwException(std::string msg) const
    {
        LOG(ERROR) << msg;
        throw ParameterOutOfRangeException(msg.c_str());
    }
};
