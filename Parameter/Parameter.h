#pragma once
#include "core/utils/log.h"
#include "_Parameter_.h"
#include "Util/u_string.h"
#include "Common/Exceptions.h"
#include <magic_enum/magic_enum.hpp>
#include <stdio.h>
#include <map>

namespace PARAMETER {
class PathParameter: public _Parameter_
{
public:
    std::filesystem::path value{"."};
    PathParameter(std::string key, bool mandatory, std::filesystem::path default_value=".", std::string descriptor=""):
        _Parameter_(key, mandatory, descriptor)
    {
        value = _default_value_ = default_value;
    }
    ~PathParameter() = default;
    PathParameter& operator = (const PathParameter& o) = default;

protected:
    PathParameter() = default;
    virtual bool _from_parameter_file_(const allpix::Configuration &config, bool check_used) override
    {
        if (check_used && _is_parsed_)
            throw ThisShouldNotHappenException(0, "%s already used", _key_.c_str());
        if (!config.has(_key_))
        {
            if (_mandatory_)
            {
                LOG(ERROR) <<  _key_ << " not found" << std::endl;
                throw NotFoundException(0, "%s", _key_);
            } else {
                value = config.getFilePath().parent_path();
                return false;
            }
        }

        value = config.getText(_key_);
        return _is_parsed_ = true;
    }

    virtual bool _check_value_() const override
    {
        return true;
    }
protected:
    std::filesystem::path _default_value_{""};
};


class range_check
{
public:
    template <typename T>
    static void check_le(T val, T ref, const std::string name, const std::string unit)
    {
        if (val > ref)
            _out_of_range_exception(string_sprintf("%s out of range: %g > %g [%s]", name.c_str(), val, ref, unit.c_str()));
    }

    template <typename T>
    static void check_lt(const T val, const T ref, std::string name, std::string unit)
    {
        if (val >= ref)
            _out_of_range_exception(string_sprintf("%s out of range: %g >= %g [%s]", name.c_str(), val, ref, unit.c_str()));
    }

    template <typename T>
    static void check_ge(const T val, const T ref, std::string name, std::string unit)
    {
        if (val < ref)
            _out_of_range_exception(string_sprintf("%s out of range: %g < %g [%s]", name.c_str(), val, ref, unit.c_str()));
    }

    template <typename T>
    static void check_gt(const T val, const T ref, std::string name, std::string unit)
    {
        if (val <= ref)
            _out_of_range_exception(string_sprintf("%s out of range: %g <= %g [%s]", name.c_str(), val, ref, unit.c_str()));
    }

    template <typename T>
    static void check_in_range(const T val, const T min, const T max, std::string name, std::string unit)
    {
        if (val < min || val > max)
        {
            _out_of_range_exception(string_sprintf("%s out of range: %g <> [%g, %g] [%s]", name.c_str(), val, min, max, unit.c_str()));
        }
    }

    template <typename T>
    static void check_within_range(const T val, const T min, const T max, std::string name, std::string unit)
    {
        if (val <= min || val >= max)
            _out_of_range_exception(string_sprintf("%s out of range: %g <> (%g, %g) [%s]", name.c_str(), val, min, max, unit.c_str()));
    }
private:
    static void _out_of_range_exception(std::string msg)
    {
        throw OutOfRangeException(msg.c_str());
    }
};

class StringParameter: public _Parameter_
{
public:

    std::string value{""};

    StringParameter(std::string key, bool mandatory, std::string default_value, std::string descriptor=""):
        _Parameter_(key, mandatory, descriptor)
    {
        value = _default_value_ = default_value;
    }
    ~StringParameter() = default;
    StringParameter& operator = (const StringParameter& o) = default;

    bool equal(std::string s)
    {
        return value == s;
    }
    bool starts_with(std::string s)
    {
        return value.compare(0, s.size(), s) == 0;
    }

protected:
    StringParameter() = default;

    virtual bool _from_parameter_file_(const allpix::Configuration &config, bool check_used=true) override
    {
        if (check_used && _is_parsed_)
            throw ThisShouldNotHappenException(0, "%s already used", _key_.c_str());
        if (!config.has(_key_))
        {
            if (_mandatory_)
            {
                LOG(ERROR) <<  _key_ << " not found" << std::endl;
                throw NotFoundException(0, "%s", _key_);
            }
            else
                return false;
        }
        value = config.get<std::string>(_key_);
        _is_parsed_ = true;
        return _is_parsed_ ;
    }

    virtual bool _check_value_() const override
    {
        return true;
    }
protected:
    std::string _default_value_{""};
};


class BooleanParameter: public _Parameter_
{
public:
    bool value{false};

    BooleanParameter(std::string key, bool mandatory, bool default_value, std::string descriptor=""):
        _Parameter_(key, mandatory, descriptor)
    {
        value = _default_value_ = default_value;
    }
    ~BooleanParameter() = default;
    BooleanParameter& operator = (const BooleanParameter& o) = default;

protected:

    BooleanParameter() = default;

    virtual bool _from_parameter_file_(const allpix::Configuration &config,bool check_used=true) override
    {
        if (check_used && _is_parsed_)
            throw ThisShouldNotHappenException(0, "%s already used", _key_.c_str());

        if (!config.has(_key_))
        {
            if (_mandatory_)
            {
                LOG(ERROR) <<  _key_ << " not found" << std::endl;
                throw NotFoundException(0, "%s", _key_);
            }
            else
                return false;
        }
        value = config.get<bool>(_key_);
        _is_parsed_ = true;
        return _is_parsed_ ;
    }

    virtual bool _check_value_() const override
    {
        return true;
    }
protected:
    bool _default_value_{false};
};


template <typename T>
class TParameter: public _Parameter_
{
public:
    T value;
    TParameter(std::string key, bool mandatory, T default_value, std::string unit="", std::string descriptor=""):
        _Parameter_(key, mandatory, descriptor)
    {
        if (typeid(T) == typeid(std::string))
        {
            throw ThisShouldNotHappenException("TParameter::from_parameter_file: no strings here");
        }
        if (typeid(T) == typeid(bool))
        {
            throw ThisShouldNotHappenException("TParameter::from_parameter_file: no bools here");
        }
        value = _default_value_ = default_value;
        _unit_= unit;
    }
    virtual ~TParameter() = default;
    TParameter& operator = (const TParameter& o) = default;

    virtual bool _check_value_() const override
    {
        return true;
    }

    virtual std::string unit() const
    {
        return _unit_;
    }

    void check_le(T ref) const
    {
        range_check::check_le(value, ref, _key_, _unit_);
    }

    void check_lt(T ref) const
    {
        range_check::check_lt(value, ref, _key_, _unit_);
    }

    void check_ge(T ref) const
    {
        range_check::check_ge(value, ref, _key_, _unit_);
    }

    void check_gt(const T ref) const
    {
        range_check::check_gt(value, ref, _key_, _unit_);
    }

    void check_in_range(const T min, const T max) const
    {
        range_check::check_in_range(value, min, max, _key_, _unit_);
    }

    void check_within_range( T min, const T max) const
    {
        range_check::check_within_range(value, min, max, _key_, _unit_);
    }

    T default_value() const
    {
        return _default_value_;
    }
protected:
    T _default_value_{0};
    TParameter() = default;

    std::string _unit_{""};

    virtual bool _from_parameter_file_(const allpix::Configuration &config, bool check_used=true) override
    {
        if (check_used && _is_parsed_)
            throw ThisShouldNotHappenException(0, "%s already used", _key_.c_str());

        if (!config.has(_key_))
        {
            if (_mandatory_)
            {
                LOG(ERROR) <<  _key_ << " not found" << std::endl;
                throw NotFoundException(0, "%s", _key_);
            }
            else
                return false;
        }
        value = config.get<T>(_key_);
        if (_unit_.size() >0)
            value /= allpix::Units::get(_unit_);

        _is_parsed_ = true;
        return _is_parsed_ ;
    }
};

template <typename T>
class TArrayParameter: public _Parameter_
{
public:
    std::vector<T> value;
    TArrayParameter(std::string key, bool mandatory, std::vector<T> default_value, std::string unit="", std::string descriptor="", std::size_t size=0)
        :_Parameter_(key, mandatory, descriptor)
    {
        value = _default_value_ = default_value;
        _unit_=unit;
        _size_ = size;
    }

    virtual ~TArrayParameter() = default;
    TArrayParameter& operator = (const TArrayParameter& o) = default;

    virtual std::string unit() const
    {
        return _unit_;
    }

    bool from_parameter_file(const allpix::Configuration &config, bool check_used=true)
    {
        bool r = __from_parameter_file__(config, check_used);
        if ((_size_ > 0) && (value.size() != _size_))
            throw ParseException("Array of size %zu expected, %zu found\n", _size_, value.size());
        return r && _check_value_();
    }


    std::size_t size() const
    {
        return value.size();
    }

    void check_le_all(T ref) const
    {
        for (std::size_t idx = 0; idx < value.size(); ++idx)
            if (value[idx] > ref)
                _out_of_range_exception(string_sprintf("%s[%zu] out of range: %g > %g [%s]", _key_.c_str(), idx, value[idx], ref, _unit_));
    }

    void check_lt_all(T ref) const
    {
        for (std::size_t idx = 0; idx < value.size(); ++idx)
            if (value[idx] >= ref)
                _out_of_range_exception(string_sprintf("%s[%zu] out of range: %g >= %g [%s]", _key_.c_str(), idx, value[idx], ref, _unit_));
    }

    void check_ge_all(T ref) const
    {
        for (std::size_t idx = 0; idx < value.size(); ++idx)
            if (value[idx] < ref)
                _out_of_range_exception(string_sprintf("%s[%zu] out of range: %g < %g [%s]", _key_.c_str(), idx, value[idx], ref, _unit_));
    }

    void check_gt_all(T ref) const
    {
        for (std::size_t idx = 0; idx < value.size(); ++idx)
            if (value[idx] <= ref)
                _out_of_range_exception(string_sprintf("%s[%zu] out of range: %g <= %g [%s]", _key_.c_str(), idx, value[idx], ref, _unit_));
    }

    void check_in_range_all(T min, T max) const
    {
        for (std::size_t idx = 0; idx < value.size(); ++idx)
            if (value[idx] < min || value[idx] > max)
                _out_of_range_exception(string_sprintf("%s[%zu] out of range: %g <> [%g, %g] [%s]", _key_.c_str(), idx, value[idx], min, max, _unit_));
    }

    void check_within_range_all( T min, const T max) const
    {
        for (std::size_t idx = 0; idx < value.size(); ++idx)
            if (value[idx] <= min || value[idx] >= max)
                _out_of_range_exception(string_sprintf("%s[%zu] out of range: %g <> (%g, %g) [%s]", _key_.c_str(), idx, value[idx], min, max, _unit_));
    }


    void check_le(std::size_t idx, T ref) const
    {
        if (value[idx] > ref)
            _out_of_range_exception(string_sprintf("%s[%zu] out of range: %g > %g [%s]", _key_.c_str(), idx, value[idx], ref, _unit_));
    }

    void check_lt(std::size_t idx, T ref) const
    {
        if (value[idx] >= ref)
            _out_of_range_exception(string_sprintf("%s[%zu] out of range: %g >= %g [%s]", _key_.c_str(), idx, value[idx], ref, _unit_));
    }

    void check_ge(std::size_t idx, T ref) const
    {
        if (value[idx] < ref)
            _out_of_range_exception(string_sprintf("%s[%zu] out of range: %g < %g [%s]", _key_.c_str(), idx, value[idx], ref, _unit_));
    }

    void check_gt(std::size_t idx, T ref) const
    {
        if (value[idx] <= ref)
            _out_of_range_exception(string_sprintf("%s[%zu] out of range: %g <= %g [%s]", _key_.c_str(), idx, value[idx], ref, _unit_));
    }

    void check_in_range(std::size_t idx, T min, T max) const
    {
        if (value[idx] < min || value[idx] > max)
            _out_of_range_exception(string_sprintf("%s[%zu] out of range: %g <> [%g, %g] [%s]", _key_.c_str(), idx, value[idx], min, max, _unit_));
    }

    void check_within_range(std::size_t idx,  T min, const T max) const
    {
        if (value[idx] <= min || value[idx] >= max)
            _out_of_range_exception(string_sprintf("%s[%zu] out of range: %g <> (%g, %g) [%s]", _key_.c_str(), idx, value[idx], min, max, _unit_));
    }

private:
    void _out_of_range_exception(std::string msg) const
    {
        throw OutOfRangeException(msg.c_str());
    }

protected:
    TArrayParameter() = default;

    std::vector<T> _default_value_;
    std::string _unit_{""};
    std::size_t _size_{0};
    virtual bool _check_value_() const override
    {
        return true;
    }

private:
    virtual bool _from_parameter_file_(__attribute__((unused)) const allpix::Configuration &config, __attribute__((unused)) bool check_used) override
    {
        throw ThisShouldNotHappenException("TArrayParameter::from_parameter_file needs expected vector size");
    }

    bool __from_parameter_file__(const allpix::Configuration &config, bool check_used=true)
    {

        if (check_used && _is_parsed_)
            throw ThisShouldNotHappenException(0, "%s already used", _key_.c_str());


        if (!config.has(_key_))
        {
            if (_mandatory_)
            {
                LOG(ERROR) <<  _key_ << " not found" << std::endl;
                throw NotFoundException(0, "%s", _key_);
            }
            else
                return false;
        }
        value = config.getArray<T>(_key_);
        if (_unit_.size() >0)
            for (T& v: value)
                v /= allpix::Units::get(_unit_);

        _is_parsed_ = true;
        return _is_parsed_ ;
    }
};


template <typename T>
class TEnumParameter: public _Parameter_
{
protected:
    std::decay_t<T> _default_value_;
public:
    std::decay_t<T> value;

    TEnumParameter(std::string key, bool mandatory, std::decay_t<T>  default_value, std::string descriptor=""):
        _Parameter_(key, mandatory, descriptor)
    {
        value = _default_value_ = default_value;
    }

    virtual ~TEnumParameter() = default;
    TEnumParameter& operator = (const TEnumParameter& o) = default;
protected:
    TEnumParameter() = default;

    virtual bool _from_parameter_file_(const allpix::Configuration &config, bool check_used) override
    {
        if (check_used && _is_parsed_)
            throw ThisShouldNotHappenException(0, "%s already used", _key_.c_str());
        if (!config.has(_key_))
        {
            if (_mandatory_)
            {
                LOG(ERROR) <<  _key_ << " not found" << std::endl;
                throw NotFoundException(0, "%s", _key_);
            }
            else
                return false;
        }
        value= config.get<T>(_key_);
        _is_parsed_ = true;
        return _check_value_();
    }
    virtual bool _check_value_() const override
    {
        return true;
    }
};

template <typename T>
bool operator == (const T& a, const TParameter<T>& b);
template <typename T>
bool operator == (const TParameter<T>& b, const T& a);
bool operator == (const std::string& a, const PARAMETER::StringParameter& b);

template <typename T>
bool operator == (const T& a, const TEnumParameter<T>& b);
template <typename T>
bool operator == ( const TEnumParameter<T>& b, const T& a);

bool operator == (const PARAMETER::StringParameter& b, const std::string& a);
bool operator == (const std::filesystem::path& a, const PathParameter& b);
bool operator == (const PARAMETER::PathParameter& b, const std::filesystem::path& a);
}


