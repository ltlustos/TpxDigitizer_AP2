#include "Parameter.h"
#include <map>
#include <string>
namespace PARAMETER
{

template <typename T>
bool operator == (const T& a, const TParameter<T>& b)
{
    return a == b.value;
}

template <typename T>
bool operator == (const T& a, const TEnumParameter<int>& b)
{
    return a == b.value;
}

bool operator == (const std::string& a, const StringParameter& b)
{
    return a == b.value;
}

bool operator == (const std::filesystem::path& a, const PathParameter& b)
{
    return a == b.value;
}

template <typename T>
bool operator == (const TParameter<T>& b, const T& a)
{
    return a == b.value;
}

template <typename T>
bool operator == (const TEnumParameter<T>& b, const T& a)
{
    return a == b.value;
}

bool operator == (const StringParameter& b, const std::string & a)
{
    return a == b.value;
}

bool operator == (const PathParameter& b, const std::filesystem::path& a)
{
    return a == b.value;
}


}

