#pragma once

#include "TpxPixelHit.h"
#include "objects/PixelCharge.hpp"
#include "objects/Pixel.hpp"
#include "Tpx.h"
#include "Export/ListModeLog.h"
#include "Export/HistogramExport.h"
#include "Export/EventExport.h"
#include "Util/u_math.h"
#include <TDirectory.h>
#include "core/utils/prng.h"
#include "TpxParameter.h"

class TpxDigitizer
{
public:
    TpxDigitizer(const std::shared_ptr<TpxParameter>, TDirectory *rootDirectory);

    ~TpxDigitizer() = default;
    void finalize();


    void process_event(allpix::RandomNumberGenerator& random_generator,
                       const PixelCharge &pixel_charge,
                       unsigned int event_num,
                       bool is_sp_event);

    std::unique_ptr<Tpx> tpx;
    std::vector<TpxPixelHit> hits;

private:

    std::shared_ptr<TpxParameter> __tpx_parameter__;
    Data2<double> __threshold_matrix__; // [e]
    Data2<double> __gain_matrix__;  // [1]
    Data2<double> __alpha_ikrum_offset_matrix; // [e/s]

    unsigned int __current_event_idx__;
    DTYPE __current_event_clock_phase{0};

    // exports
    HistogramExport __hist_export__;
    ListModeLog __lm_log__;
    ListModeLog __lm_log_sp__;  // logs only real single pixel events

    HistogramExport __hist_export_single_pixel_hit__;
    std::unique_ptr<EventExport > __event_export__;
    TDirectory *__rootDirectory__;

    void __run_enc_std__(allpix::RandomNumberGenerator& random_generator);
    const DTYPE __idle_noise__{0.001};
};

