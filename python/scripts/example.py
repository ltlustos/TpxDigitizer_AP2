#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 20 10:58:46 2023

@author: ltlustos
"""

import os
thisdir = os.path.dirname(__file__)
python_dir = os.path.realpath(thisdir + "/..")
import sys
if python_dir not in sys.path:
    sys.path.append(python_dir)

from pyap2 import Ap2Conf as ap2conf


# ap2dir = "/home/ltlustos/local/allpix-squared-v2.3.0/_build/src/exec/"
ap2dir = "/home/ltlustos/local/packages/allpix-squared-v2.3.0/_debug/src/exec/"

number_of_events = int(5e2)
t_acq = 12  #us
overwrite = 1
for ENC in [0, 80]:
    for keV in [20]:
        thresholds = [750, 900, 1000, 1500, 2000][0:1]
        for threshold in thresholds:
            outpath = f"{thisdir}/_out/example/ENC{ENC}e/{keV}keV/threshold{threshold}/"
            os.makedirs(outpath, exist_ok=True)
            if os.path.exists(outpath + "lm_log.txt") and not overwrite:
                continue
            cfg = ap2conf(f"{thisdir}/tpx3.run.template")  ## use this as conf templte and change what you nned
            cfg["AllPix"]["number_of_events"] = f"{number_of_events}"
            cfg["DepositionGeant4"]["source_energy"] = f"{keV}keV"
            cfg["TpxDigitizer"]["t_acq"] = f"{t_acq}us"
            cfg["TpxDigitizer"]["threshold"] = f"{threshold}e"
            cfg["TpxDigitizer"]["output_path"] = f"{outpath}"
            cfg.run_ap2(ap2dir, thisdir)
