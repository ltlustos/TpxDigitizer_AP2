#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 09:18:16 2023

@author: ltlustos
"""
from collections import OrderedDict as odict
import subprocess
import os
import datetime
import shutil
import warnings
import glob

class Ap2Line:
    def __init__(self, line):
        try:
            line, comment = line.strip().split("#", 1)
            comment = "# " + comment
        except(ValueError):
            comment = ""
        key, valstr = line.strip().split("=", 1)
        self.key = key.strip()
        self.valstr = valstr.strip()
        self.comment = comment.strip()

    def __repr__(self):
        return f"{self.key} = {self.valstr}\n"

    def write(self, cfg):
        cfg.write(str(self))

    def __set__(self, valstr):
        self.val = valstr

class Ap2Section:
    def __init__(self, name):
        self.name = name
        self.parms = odict()
        self.enabled = True

    def __repr__(self):
        s = f"[{self.name}]\n"
        for key, line in self.parms.items():
            s += str(line)
        s += "\n"
        return s

    def write(self, cfg):
        if self.enabled:
            cfg.write(str(self))


    def parse(self, line):
        l = Ap2Line(line)
        self.parms[l.key] = l

    def __getitem__(self, key):
        if not key in self.parms:
            raise Exception(f"{key} not a valid key")
        return self.parms[key].valstr

    def __setitem__(self, key, valstr):
        if not key in self.parms:
            raise Exception(f"{key} not a valid key")
        self.parms[key].valstr = valstr

class Ap2Conf:
    def __init__(self, path):
        self.path = os.path.realpath(path)
        self.sections = odict()
        self.read()

    def __repr__(self):
        s = ""
        for key, section in self.sections.items():
            s += str(section)
        return s


    def read(self):
        # print(self.path)
        with open(self.path, "r") as cfg:
            lines = cfg.readlines()
            section = None
            for line in lines:
                line = line.strip()
                if line.startswith("#") or len(line) == 0:
                    continue
                elif line.startswith("[") and line.endswith("]"):
                    section = Ap2Section(line[1:-1])
                    self.sections[section.name] = section
                else:
                    section.parse(line)


    def write(self, path=None):
        if path is None:
            path = self.path
        with open(path, "w") as cfg:
            for _, s in self.sections.items():
                s.write(cfg)

    def __getitem__(self, key):
        if not key in self.sections:
            raise Exception(f"{key} section not found")
        return self.sections[key]

    def run_ap2(self, ap2dir, confdir=".", conf_name="run.conf", verbose=False):
        os.makedirs(confdir, exist_ok=True)
        conf_file = f"{confdir}/{conf_name}"
        # if os.path.exists(conf_file):
        #     timestamp = '%04i%02i%02i_%02i%02i%02i' % datetime.datetime.now().timetuple()[0:6]
        #     bkp_conf = f"{confdir}/runf_{timestamp}.bkp"
        #     shutil.copy(conf_file, bkp_conf)
        #     warnings.warn(f"{conf_file} exists already -> {bkp_conf}")
        self.write(conf_file)
        # subprocess.run([f"{ap2dir}allpix","-c", conf_file], cwd=confdir)
        if verbose:
            subprocess.call([f"{ap2dir}allpix","-c", conf_file],
                             cwd=confdir,
                             timeout=None)
        else:
            subprocess.call([f"{ap2dir}allpix","-c", conf_file],
                             stderr=subprocess.DEVNULL,
                             stdout=subprocess.DEVNULL,
                             cwd=confdir,
                             timeout=None)
