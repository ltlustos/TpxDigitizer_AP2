#pragma once

#include "H/H_Atomic.h"

class BW_LP1: public H_Atomic
{
public:
    BW_LP1(DTYPE f_sample, DTYPE f_cutoff, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle)
    {
        if (f_cutoff<0)
        {
            __initialize_h__({0}, {1});
            __idle__ = true;
            return;
        }
        DTYPE a =  std::tan(M_PI * f_cutoff / f_sample);
        std::vector<DTYPE> num{a, a};
        std::vector<DTYPE> denom{a + 1.0 , a -1.0};
        __initialize_h__(num, denom);
        __idle__ = false;

    }
    BW_LP1(const BW_LP1 &other) = default;
    BW_LP1& operator =(const BW_LP1& other) = default;
    virtual ~BW_LP1() = default;

};


class BW_LP2: public H_Atomic
{
public:
    BW_LP2(DTYPE f_sample, DTYPE f_cutoff, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle)
    {
        if (f_cutoff<0)
        {
            __initialize_h__({0}, {1});
            __idle__ = false;
            return;
        }
        DTYPE a =  std::tan(M_PI * f_cutoff / f_sample);
        DTYPE a2 = square(a);
        DTYPE r = std::sin(M_PI/4);
        std::vector<DTYPE> num{a2, 2.0 * a2, a2};
        std::vector<DTYPE> denom{a2 + 2 * a * r + 1.0, -2.0 * (1-a2), a2 - 2 * a * r + 1.0};
        __initialize_h__(num, denom);
        __idle__ = false;
    }
    BW_LP2(const BW_LP2 &other) = default;
    BW_LP2& operator =(const BW_LP2& other) = default;
    virtual ~BW_LP2() = default;
};



