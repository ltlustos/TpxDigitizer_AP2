#pragma once

#include "Common/dtype.h"
#include <vector>
std::vector<DTYPE> binomial_mult( std::size_t n, const std::vector<DTYPE> p )
{
    std::vector<DTYPE> r(2 * n);
    for(std::size_t i = 0; i < n; ++i )
    {
        for(std::size_t j = i; j > 0; --j )
        {
            r[2*j] += p[2*i] * r[2*(j-1)] - p[2*i+1] * r[2*(j-1)+1];
            r[2*j+1] += p[2*i] * r[2*(j-1)+1] + p[2*i+1] * r[2*(j-1)];
        }
        r[0] += p[2*i];
        r[1] += p[2*i+1];
    }
    return r;
}


std::vector<DTYPE> trinomial_mult( int n, const std::vector<DTYPE> b, const std::vector<DTYPE> c )
{
    std::vector<DTYPE> r(4*n);

    r[2] = c[0];
    r[3] = c[1];
    r[0] = b[0];
    r[1] = b[1];

    for(std::size_t  i = 1; i < n; ++i )
    {
        r[2*(2*i+1)]   += c[2*i]*r[2*(2*i-1)]   - c[2*i+1]*r[2*(2*i-1)+1];
        r[2*(2*i+1)+1] += c[2*i]*r[2*(2*i-1)+1] + c[2*i+1]*r[2*(2*i-1)];

        for(std::size_t  j = 2*i; j > 1; --j )
        {
            r[2*j]   += b[2*i] * r[2*(j-1)]   - b[2*i+1] * r[2*(j-1)+1] +
                    c[2*i] * r[2*(j-2)]   - c[2*i+1] * r[2*(j-2)+1];
            r[2*j+1] += b[2*i] * r[2*(j-1)+1] + b[2*i+1] * r[2*(j-1)] +
                    c[2*i] * r[2*(j-2)+1] + c[2*i+1] * r[2*(j-2)];
        }

        r[2] += b[2*i] * r[0] - b[2*i+1] * r[1] + c[2*i];
        r[3] += b[2*i] * r[1] + b[2*i+1] * r[0] + c[2*i+1];
        r[0] += b[2*i];
        r[1] += b[2*i+1];
    }

    return r;
}
