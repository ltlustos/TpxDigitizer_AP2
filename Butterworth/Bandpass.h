#pragma once
#include "H/H_Atomic.h"

class BW_BP1: public H_Atomic
{
public:
    BW_BP1(DTYPE f_sample, DTYPE f_upper, DTYPE f_lower, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle)
    {
        if (f_upper <= f_lower)
            throw ParameterException("f_upper %g <= f_lower %g", f_upper, f_lower);
        DTYPE f_center = 0.5 * (f_upper + f_lower);
        DTYPE f_width = f_upper - f_lower;

        DTYPE a =  std::cos(2 * M_PI * f_center / f_sample) / std::cos(M_PI * f_width / f_sample);
        DTYPE b =  0.45 *( std::tan(M_PI * f_width / f_sample) + std::tan(M_PI * f_center / f_sample));

        std::vector<DTYPE> num{b, 0.0, -b};
        std::vector<DTYPE> denom{b + 1.0 , -2.0 * a, 1.0 - b};

        __initialize_h__(num, denom);
        __idle__ = false;
    }
    BW_BP1(const BW_BP1 &other) = default;
    BW_BP1& operator =(const BW_BP1& other) = default;
    virtual ~BW_BP1() = default;
};


class BW_BP2: public H_Atomic
{
public:
    BW_BP2(DTYPE f_sample, DTYPE f_upper, DTYPE f_lower, DTYPE idle_cutoff, bool reset_on_idle)
        :H_Atomic(idle_cutoff, reset_on_idle)
    {
        if (f_upper <= f_lower)
            throw ParameterException("f_upper %g <= f_lower %g", f_upper, f_lower);
        DTYPE f_center = 0.5 * (f_upper + f_lower);
        DTYPE f_width = f_upper -f_lower;

        DTYPE a =  std::cos(M_PI * 2 * f_center / f_sample) / std::cos(M_PI * f_width / f_sample);
        DTYPE b =  std::tan(M_PI * f_width / f_sample);
        DTYPE a2 = square(a);
        DTYPE b2 = square(b);
        DTYPE r = std::sin(M_PI/4);
        std::vector<DTYPE> num{b2, 0, -2.0 * b2, 0, b2};
        std::vector<DTYPE> denom{b2 + 2.0 * b * r + 1,
                    -4.0 * a * (1.0 + b*r),
                    -2.0 * (b2 - 2.0 * a2 - 1.0),
                    -4.0 * a * (1.0 - b * r),
                    b2 - 2.0 * b * r + 1.0};
        __initialize_h__(num, denom);
        __idle__ = false;
    }
    BW_BP2(const BW_BP2 &other) = default;
    BW_BP2& operator =(const BW_BP2& other) = default;
    virtual ~BW_BP2() = default;
};



