/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef TPXPARAMETER_H
#define TPXPARAMETER_H

#include "core/config/ConfigManager.hpp"
#include "core/config/Configuration.hpp"
#include "core/geometry/DetectorModel.hpp"
#include "physics/MaterialProperties.hpp"
#include "Common/dtype.h"
#include "Common/const.h"
#include "modules/TpxDigitizer/Parameter/_ParameterSet_.h"
#include "Pixel/Preamp/Parameter/FeedbackParameter.h"
#include "Pixel/Disc/DiscParameter.h"

#include "Pixel/Preamp/Parameter/PreampParameter.h"
#include "OutputParameter.h"
#include "modules/TpxDigitizer/Parameter/Parameter.h"
#include <limits.h>

class TpxParameter: public _Parameter_
{
public:

    PARAMETER::TParameter<DTYPE> temperature{"temperature", false, T_ref, "K"};
    PARAMETER::TParameter<DTYPE> t_acq_sec{"t_acq", false, 0, "s"," acquisition time, if 0: simulation stops when preamp is idle"};
    PARAMETER::TParameter<DTYPE> t0_sec{"t0", false, 200e-9, "s", "pre event run time, warmup cisrcuit, charge noise channels"};
    PARAMETER::TParameter<DTYPE> dt_sec{"dt", true, 0.1953e-9, "s", "simulation timestep"};
    PARAMETER::TParameter<DTYPE> dt_ns{"dt", true, 0.1953e-9, "ns", "simulation timestep"};
    PARAMETER::BooleanParameter randomize_event_clock_phase{"randomize_event_clock_phase", false, true, "randomize event time with respect to coarse clock period"};
    PARAMETER::TParameter<std::size_t> pileup_event{"pileup_event", false, 0};
    PARAMETER::TParameter<DTYPE> pileup_period{"pileup_period", false, 100e-9, "s"};
    PARAMETER::BooleanParameter pileup_poisson{"pileup_poisson", false, false};
    PARAMETER::BooleanParameter pileup_alternate_polarity{"pileup_alternate_polarity", false, false};
    PARAMETER::BooleanParameter run_enc{"run_enc", false, false, "debug option, run a 3us noise run before first event"};
    PARAMETER::TParameter<DTYPE> h_cutoff{"h_cutoff", false, 1.0e-3};

    PreampParameter preamp_parm;
    DiscParameter disc_parm;

    OutputParameter output_parm;
    PARAMETER::TParameter<DTYPE> max_hist_charge_ke{"max_hist_charge_ke", false, 0};

    TpxParameter(const std::shared_ptr<allpix::DetectorModel>& model, allpix::ConfigManager& conf_mgr_)
    {
        _n_cols_ = model->getNPixels().Coordinates().X();
        _n_rows_ = model->getNPixels().Coordinates().Y();
        DTYPE charge_creation_energy = allpix::ionization_energies[model->getSensorMaterial()] *1e6; // eV/eh
        _rand_seed_ = conf_mgr_.getGlobalConfiguration().get<uint64_t>("random_seed");
        _n_events_ = conf_mgr_.getGlobalConfiguration().get<uint64_t>("number_of_events");
        for(const auto& cfg : conf_mgr_.getModuleConfigurations())
        {
            if(cfg.getName() == "DepositionGeant4")
                _beam_energy_keV_ = cfg.get<DTYPE>("source_energy") * 1e3;  // [keV]
            if(str_starts_with(cfg.getName(), "DepositionPointCharge"))
            {
                _beam_energy_keV_ = cfg.get<DTYPE>("number_of_charges");
                _beam_energy_keV_ *= 1e-3 * charge_creation_energy;  // [keV]
            }
        }
        max_hist_charge_ke.value = _beam_energy_keV_ / charge_creation_energy ;
    }

    virtual ~TpxParameter() = default;
    TpxParameter& operator = (const TpxParameter& o) = default;


    void check_adjust()
    {
        check_values();
        adjust_values();
    }

    void check_values() const override
    {
        preamp_parm.check_values();
        if(disc_parm.clock_period_sec.value < dt_sec.value)
            LOG(WARNING) << "Preamp timestep must be smaller than  clock.";
        if(disc_parm.fclock_period_sec.value < dt_sec.value)
            LOG(WARNING) << "Preamp timestep must be smaller than  f_clock.";

        if(disc_parm.ufclock_period_sec.value < dt_sec.value)
            LOG(WARNING) << "Preamp timestep must be smaller than uf_clock.";

        temperature.check_in_range( 250.0, 400.0);

        dt_sec.check_in_range( 1e-12, 5e-9);
        t_acq_sec.check_ge( 0.0);
        t0_sec.check_ge( 0.0);
        h_cutoff.check_in_range( 1.0e-7, 1.0e-3);
        pileup_event.check_in_range(0, 1000);
        pileup_period.check_in_range(1.0e-9, 100.0e-6);

        if (disc_parm.fclock_period_sec.value < dt_sec.value)
            throwException("Preamp timestep must be smaller than ToA clock.");

        if (disc_parm.clock_period_sec.value < dt_sec.value) {
            throwException("Preamp timestep must be smaller than ToT clock.");
        }
        preamp_parm.check_values(dt_sec.value);
        disc_parm.check_values(dt_sec.value);

        output_parm.check_values();
    }


    void adjust_values() override
    {
        preamp_parm.adjust_values();
        disc_parm.adjust_values();

        output_parm.adjust_values();
        preamp_parm.adjust_timestep_values(dt_sec.value);
        disc_parm.adjust_timestep_values(dt_sec.value);

        output_parm.adjust_timestep_values(dt_sec.value);
    }

    void parse_config(const allpix::Configuration &config) override
    {
        dt_ns.from_parameter_file(config);
        dt_sec.from_parameter_file(config);
        t_acq_sec.from_parameter_file(config);
        t0_sec.from_parameter_file(config);
        h_cutoff.from_parameter_file(config);
        randomize_event_clock_phase.from_parameter_file(config);
        temperature.from_parameter_file(config);
        run_enc.from_parameter_file(config);
        pileup_event.from_parameter_file(config);
        pileup_period.from_parameter_file(config);
        pileup_poisson.from_parameter_file(config);
        pileup_alternate_polarity.from_parameter_file(config);
        preamp_parm.parse_config(config);
        disc_parm.parse_config(config);

        output_parm.parse_config(config);
    }

    std::size_t n_rows() const
    {
        return _n_rows_;
    }

    std::size_t n_cols() const
    {
        return _n_cols_;
    }


    std::size_t n_events() const
    {
        return _n_events_;
    }


    DTYPE beam_energy_keV() const
    {
        return _beam_energy_keV_;
    }

    std::uint64_t rand_seed() const
    {
        return _rand_seed_;
    }


private:
    std::size_t _n_cols_;
    std::size_t _n_rows_;
    DTYPE _beam_energy_keV_{0};
    std::uint64_t _rand_seed_{0};
    std::size_t _n_events_{0};
};


#endif // TPXPARAMETER_H
