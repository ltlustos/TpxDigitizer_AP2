/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef _ListModeLog__H
#define _ListModeLog__H
#include "stdio.h"
#include <mutex>
#include "Pixel/Disc/DiscriminatorEvent.h"
#include "objects/PixelCharge.hpp"
#include "Util/u_string.h"
#include "limits.h"
#include "stdarg.h"
#include "stddef.h"
#include "Common/Exceptions.h"
#include <stdio.h>

class _ListModeLog_
{
public:
    _ListModeLog_()
    {
        _fp_log_ = NULL;
    }
    virtual ~_ListModeLog_(void)
    {
        close();
    }
    virtual void open(
            __attribute__((unused)) std::string outpath,
            __attribute__((unused)) std::string prefix="",
            __attribute__((unused)) const char* mode = "w")
    {
        throw SubclassResponsabilityException("_ListModeLog_:open");
    }

    virtual void close()
    {
        std::lock_guard<std::mutex> lock(_write_lock_);
        _close_();
    }


    void flush() const
    {
        fflush(_fp_log_);
    }


protected:
    void _open_(std::string fname,
                std::string header="",
                const char* mode= "w")
    {
        std::lock_guard<std::mutex> lock(_write_lock_);
        if (_ckeck_open_())
            _close_();

        _fp_log_ = fopen(fname.c_str(), mode);
        setvbuf(_fp_log_, NULL, _IOFBF, 1<<14);
        if (header.size() >0)
            fprintf(_fp_log_, "%s\n", header.c_str());
    }

    virtual void _close_()
    {
        if (_fp_log_ != NULL){
            flush();
            fclose(_fp_log_);
            _fp_log_=NULL;
        }
    }


    bool _ckeck_open_() const
    {
        if (_fp_log_ != NULL)
        {
            return true;
        }
        return false;
    }
    FILE *_fp_log_;
    std::string _fname_;
    std::mutex _write_lock_;


    template <typename ...Args>
    void _log_(const std::string& fmt, Args && ...args)
    {
        fprintf(_fp_log_, fmt.c_str(), args...);
    }

#pragma GCC diagnostic ignored "-Wformat-nonliteral"
    template <typename ...Args>
    void _log_locked_(const std::string& fmt, Args && ...args)
    {
        std::lock_guard<std::mutex> lock(_write_lock_);
        fprintf(_fp_log_, fmt.c_str(), args...);
    }
#pragma GCC diagnostic warning "-Wformat-nonliteral"
};


class  ListModeLog: public _ListModeLog_
{

public:
    ListModeLog()
    {}

    ~ListModeLog(void)
    {}

    void open(std::string outpath, bool binary=false)
    {
        _binary_ = binary;
        if (_binary_)
            _open_(outpath + ".dat");
        else
            _open_(outpath + ".txt", string_sprintf("% 7s\t% 4s\t% 4s\t% 14s\t% 14s\t% 14s\t% 14s\t% 5s\t% 6s\t% 7s\t% 7s\t% 7s\t% 7s\t% 14s\t% 14s\t% 14s",
                                                    "idx", "col", "row", "tcc_ns","t_rise_ns", "t_fall_ns", "ToT_ns", "clk_r","fclk_r","ufclk_r",
                                                    "clk_f", "fclk_f", "ufclk_f", "peak_e", "t_peak_ns", "input_e"));
    }

    void log(std::size_t primary_index, DTYPE pixel_charge, DTYPE t0, DiscriminatorEvent &ev )
    {
        if (_binary_)
        {
            std::lock_guard<std::mutex> lock(_write_lock_);
            __fwrite__(primary_index);
            __fwrite__(ev.col());
            __fwrite__(ev.row());

            __fwrite__(t0*1e9);
            __fwrite__(ev.t_rise_async() * 1e9);
            __fwrite__(ev.t_fall_async() * 1e9);
            __fwrite__(ev.tot_async() * 1e9);

            __fwrite__(ev.clk_rise());
            __fwrite__(ev.fclk_rise());
            __fwrite__(ev.ufclk_rise());
            __fwrite__(ev.clk_fall());
            __fwrite__(ev.fclk_fall());
            __fwrite__(ev.ufclk_fall());

            __fwrite__(ev.max_amplitude());
            __fwrite__(ev.t_peak() * 1e9);
            __fwrite__(pixel_charge);
        } else {
            _log_locked_("%7lu\t%4lu\t%4lu\t%12.5f\t%12.5f\t%12.5f\t%12.5f\t%5lu\t%6lu\t%7lu\t%7lu\t%7lu\t%7lu\t%11.2f\t%12.3f\t%11.2f\n",
                         primary_index,
                         ev.col(),
                         ev.row(),
                         t0*1e9,
                         ev.t_rise_async() * 1e9,
                         ev.t_fall_async() * 1e9,
                         ev.tot_async() * 1e9,

                         ev.clk_rise(),
                         ev.fclk_rise(),
                         ev.ufclk_rise(),
                         ev.clk_fall(),
                         ev.fclk_fall(),
                         ev.ufclk_fall(),

                         ev.max_amplitude(),
                         ev.t_peak() * 1e9,
                         pixel_charge);
        }
    }
protected:
    ListModeLog(const ListModeLog&);
    ListModeLog& operator = (const ListModeLog);
    bool _binary_{false};
    template<typename T>
    size_t __fwrite__ (const T &v)
    {
        return fwrite(&v, sizeof(T), 1, _fp_log_);
    }

};


#endif // _ListModeLog__H
