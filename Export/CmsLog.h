#pragma once
#include "Export/ListModeLog.h"
#include "objects/PixelCharge.hpp"
#include "Pixel/Disc/DiscriminatorEvent.h"
#include "Export/ListModeLog.h"
#include <Math/Point3D.h>

class  CmsLog: public _ListModeLog_
{

public:
    CmsLog()
    {}

    ~CmsLog(void)
    {}

    void open(std::string outpath)
    {
        _open_(outpath, string_sprintf("% 7s\t% 3s\t% 8s\t% 8s\t% 8s\t% 8s\t% 8s\t% 8s\t% 8s\t% 8s\t% 8s\t% 8s\t% 8s",
                                       "idx", "n", "c0", "r0", "d_col", "d_row", "q_d[e]",
                                       "c_col", "c_row", "q_c[e]", "p_col", "p_row", "tot[ns]"));
    }

    void log(std::size_t event_num,
             const ROOT::Math::XYVector &pixel_size,
             const std::vector<allpix::DepositedCharge> &depositions,
             const std::vector<allpix::PixelCharge> &charges,
             const std::vector<DiscriminatorEvent> &disc_events)
    {
        //    __cms_log__
        ROOT::Math::XYZPoint pos0 = depositions[0].getLocalPosition();
        DTYPE initial_deposition_row = pos0.Y() / pixel_size.Y();
        DTYPE initial_deposition_col = pos0.X() / pixel_size.X();
        DTYPE deposition_cms_row{0}, deposition_cms_col{0}, deposition_total{0};
        for(const allpix::DepositedCharge& dep : depositions)
        {
            ROOT::Math::XYZPoint pos = dep.getLocalPosition();
            auto charge = static_cast<double>(dep.getCharge());
            DTYPE row = pos.Coordinates().Y();
            DTYPE col = pos.Coordinates().X();
            deposition_total += charge;
            deposition_cms_col += col * charge;
            deposition_cms_row += row * charge;
        }
        deposition_cms_col /= pixel_size.X() * deposition_total;
        deposition_cms_row /= pixel_size.Y() * deposition_total;
        deposition_total *= 0.5;  // double counting of holes/electrons;

        //    __cms_log__
        DTYPE charge_cms_row{0}, charge_cms_col{0}, total_charge{0};
        for(const allpix::PixelCharge& pixel_charge : charges)
        {
            auto pixel = pixel_charge.getPixel();
            auto pixel_index = pixel.getIndex();
            auto charge = static_cast<double>(pixel_charge.getCharge());
            DTYPE row = pixel_index.Y();
            DTYPE col = pixel_index.X();
            total_charge += charge;
            charge_cms_col += col * charge;
            charge_cms_row += row * charge;
        }
        charge_cms_col /= total_charge;
        charge_cms_row /= total_charge;

        DTYPE pixel_cms_row{0}, pixel_cms_col{0}, total_tot{0};
        for(const DiscriminatorEvent &ev : disc_events)
        {
            total_tot += ev.tot_async();
            pixel_cms_col += ev.col() * ev.tot_async();
            pixel_cms_row += ev.row() * ev.tot_async();
        }
        pixel_cms_row /= total_tot;
        pixel_cms_col /= total_tot;

        if (total_charge == 0)
            return;


        _log_locked_("%7lu\t%3lu\t%8.5f\t%8.5f\t%8.5f\t%8.5f\t%8.1f\t%8.5f\t%8.5f\t%8.1f\t%8.5f\t%8.5f\t%8.2f\n",
                     event_num,
                     disc_events.size(),
                     initial_deposition_col,
                     initial_deposition_row,
                     deposition_cms_col, deposition_cms_row, deposition_total,
                     charge_cms_col, charge_cms_row, total_charge,
                     pixel_cms_col, pixel_cms_row, total_tot*1e9);
    }
protected:
    CmsLog(const CmsLog&);
    CmsLog& operator = (const CmsLog);

};
