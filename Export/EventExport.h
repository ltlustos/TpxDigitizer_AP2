#ifndef EVENTPEXPORT_H
#define EVENTPEXPORT_H

#include <algorithm>
#include <valarray>
#include "Common/dtype.h"
#include <string>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TDirectory.h>
#include <TMath.h>
#include <unistd.h>
#include "Util/u_root.h"
#include "Util/u_string.h"
#include "core/utils/log.h"
#include "Common/Exceptions.h"
#include <stdio.h>
#include "Tpx.h"
#include "TGaxis.h"
#include "core/utils/log.h"
#include "TpxParameter.h"

class EventExport
{
public:

    ~EventExport() = default;
    EventExport(
        TDirectory *rootDir,
        const TpxParameter& tpxParameter)
    {
        __rootDirectory__ = rootDir;
        __output_path__ = tpxParameter.output_parm.get_output_path() / "preamp";
        __output_prefix__ = tpxParameter.output_parm.output_prefix.value;
        __initialized__ = true;
        if (tpxParameter.output_parm.output_event_csv.value || tpxParameter.output_parm.output_event_plot.value)
            std::filesystem::create_directories(__output_path__);
        reset();
        cleanup_old_export();
        __max_count_png__ = tpxParameter.output_parm.output_event_plot.value;
        __max_count_csv__ = tpxParameter.output_parm.output_event_csv.value;
        __is_active__ = __max_count_png__ || __max_count_csv__;
        __plot_disc__ = true;
        __plot_fb__ = tpxParameter.output_parm.detailed_event_plot.value;
        __plot_ileak__ = true;
        __plot_idle__ = false;
    }

    void cleanup_old_export() const
    {
        auto files_regex = [](std::string path, std::string regex)
        {
            std::vector<std::string> files;
            try{
                std::regex fmatch(regex, std::regex_constants::ECMAScript | std::regex_constants::icase);
                for(auto& p: std::filesystem::directory_iterator(path))
                {
                    if (std::regex_match(p.path().filename().c_str(), fmatch))
                        files.emplace_back(p.path().filename().string());
                }
            }catch(std::filesystem::filesystem_error const& ex)
            {}
            return files;
        };

        for (auto f : files_regex(__output_path__.string(), "^" + __output_prefix__ + "amp_pulse_ev.*\\.txt$"))
            std::remove((__output_path__ / f).string().c_str());
        for (auto f : files_regex(__output_path__.string(), "^" + __output_prefix__ + "amp_pulse_ev.*\\.png$"))
            std::remove((__output_path__ / f).string().c_str());
    }

    void reset()
    {
        __pulse_vec__.clear();
        __threshold_vec__.clear();
        __threshold_vec_nominal__.clear();
        __disc_vec__.clear();
        __disc_latch_vec__.clear();
        __time_vec__.clear();
        __fb_total__.clear();
        __idle_vec__.clear();
        __ileakc_vec__.clear();
        for (auto& e : __fb__)
            e.clear();
        __fb__.clear();
    }

    void append(const Tpx& tpx)
    {
        if (!__is_active__)
            return;
        __check_initialized__();

        __time_vec__.emplace_back(tpx.time.now_ns());
        __pulse_vec__.emplace_back(tpx.preamp->out());

        __threshold_vec__.emplace_back(tpx.disc->current_threshold() * tpx.disc->polarity());
        __threshold_vec_nominal__.emplace_back(tpx.disc->threshold());
        if (__plot_disc__)
        {
            __disc_vec__.emplace_back(tpx.disc->out());
            __disc_latch_vec__.emplace_back(tpx.disc->latched());
        }

        __idle_vec__.emplace_back(!tpx.preamp->is_idle());

        if (__plot_ileak__)
            __ileakc_vec__.emplace_back( tpx.preamp->lcc->out() / tpx.time.dt_ns() );
        if (__plot_fb__)
        {
            __fb__.resize(tpx.preamp->feedback->fb_out().size());
            for (size_t i = 0; i< tpx.preamp->feedback->fb_out().size(); ++i)
                __fb__[i].emplace_back(tpx.preamp->feedback->fb_out()[i] / tpx.time.dt_ns());
        }
        __fb_total__.emplace_back((tpx.preamp->feedback->out())/ tpx.time.dt_ns());
    }

    void export_event(const std::string& s_event_num, const std::string& s_pixel_index)
    {
        if (__is_active__)
        {
            if (__csv_count__ <= __max_count_csv__)
            {
                __export_signals__(s_event_num, s_pixel_index);
                ++__csv_count__;
            }
            if(__png_count__ <= __max_count_png__)
            {
                export_pulsegraphs(s_event_num, s_pixel_index);
                ++__png_count__;
            }
            __is_active__ = (__png_count__ <= __max_count_png__) || (__csv_count__ <= __max_count_csv__);
        }
    }


    void export_pulsegraphs(const std::string& s_event_num, const std::string& s_pixel_index)
    {

        __check_initialized__();
        std::string name = "amp_pulse_ev" + s_event_num + "_px" + s_pixel_index;
        delete_root_object_named<TMultiGraph>("amp_pulse_ev");
        TMultiGraph *mg = new TMultiGraph("amp_pulse_ev", ( s_event_num + "_px" + s_pixel_index).c_str() );

        auto t0 = __time_vec__[0];
        for (auto& t: __time_vec__)
            t -= t0;

        auto* thresh_graph =_add_graph_(mg, __threshold_vec__, kBlack, kSolid , "thresh");

        const auto [pmin, pmax] = std::minmax_element(std::begin(__pulse_vec__), std::end(__pulse_vec__));
        const auto [fbmin, fbmax] = std::minmax_element(std::begin(__fb_total__), std::end(__fb_total__));
        _fb_scale_ = 0.05*std::max(std::abs(*pmin), std::abs(*pmax)) / std::max(std::abs(*fbmin), std::abs(*fbmax)) / (__time_vec__[1]-__time_vec__[0]);

        TGraph* idle_graph = nullptr;
        if (__plot_idle__)
        {
            for (DTYPE& v : __idle_vec__) v *= 100.0;
            idle_graph =_add_graph_(mg, __idle_vec__, kGreen, kSolid, "idle");
        }
        TGraph* fb_total_graph = nullptr;
        for (DTYPE& v : __fb_total__) v *= -_fb_scale_;
        fb_total_graph =_add_graph_(mg, __fb_total__, kGreen, kSolid, string_sprintf("fb total * %.0g", _fb_scale_).c_str());

        TGraph* ileakc_pulse_graph = nullptr;
        if (__plot_ileak__)
        {
            for (DTYPE& v : __ileakc_vec__) v *= -1;
            ileakc_pulse_graph =_add_graph_(mg, __ileakc_vec__, kRed, kSolid, string_sprintf("ileakc * %.0g", _fb_scale_).c_str());
        }
        std::vector<TGraph*> __fb_vec__;
        if (__plot_fb__)
        {
            for (size_t i=0; i<__fb__.size(); ++i)
            {
                for (DTYPE& v : __fb__[i]) v *= -_fb_scale_;
                __fb_vec__.emplace_back(_add_graph_(mg, __fb__[i], kGray, kDotted, string_sprintf("fb[%lu] * %.0g", i, _fb_scale_).c_str()));
            }
        }

        TGraph* disc_graph = nullptr;
        TGraph* latch_graph = nullptr;
        if (__plot_disc__)
        {
            const auto [pmin, pmax] = std::minmax_element(std::begin(__pulse_vec__), std::end(__pulse_vec__));
            DTYPE disc_scale = std::max(std::abs(*pmin), std::abs(*pmax))  / 10.0;
            for (std::size_t i=0; i<__disc_vec__.size(); ++i)
                __disc_vec__[i] = disc_scale * __disc_vec__[i];

            for (std::size_t i=0; i<__disc_latch_vec__.size(); ++i)
                __disc_latch_vec__[i] = disc_scale * __disc_latch_vec__[i];

            disc_graph =_add_graph_(mg, __disc_vec__, kBlue, kDotted , "disc");
            latch_graph =_add_graph_(mg, __disc_latch_vec__, kRed, kDotted , "disc latch");
        }

        auto* csa_pulse_graph =_add_graph_(mg, __pulse_vec__, kBlue, kSolid , "preamp out");


        mg->GetXaxis()->SetTitle("t [ns]");
        mg->GetYaxis()->SetTitle("output [e]");
        mg->SetTitle( ("Preamplifier output in pixel (" + s_pixel_index + ")").c_str());
        __rootDirectory__->WriteTObject(mg, name.c_str());
        png_export<TMultiGraph>( mg, __output_path__ / (__output_prefix__ +  name + ".png"), "A");

        if (__plot_idle__)
            idle_graph->Clear();
        if (__plot_fb__)
            fb_total_graph->Clear();
        if (__plot_ileak__)
            ileakc_pulse_graph->Clear();
        if (fb_total_graph != nullptr)
            fb_total_graph->Clear();
        for (auto e : __fb_vec__)
            e->Clear();
        __fb_vec__.clear();

        csa_pulse_graph->Clear();
        thresh_graph->Clear();
        if (__plot_disc__)
        {
            disc_graph->Clear();
            latch_graph->Clear();
        }

        mg->Clear();
    }



private:
    std::vector<DTYPE> __time_vec__;
    std::vector<DTYPE> __pulse_vec__;
    std::vector<DTYPE> __threshold_vec__;
    std::vector<DTYPE> __threshold_vec_nominal__;
    std::vector<DTYPE> __disc_vec__;
    std::vector<DTYPE> __disc_latch_vec__;
    bool __plot_disc__{true};
    bool __plot_fb__{true};
    bool __plot_ileak__{true};
    bool __plot_idle__{false};
    std::vector<DTYPE> __fb_total__;

    std::vector<DTYPE> __idle_vec__;

    std::vector<std::vector<DTYPE>> __fb__;
    std::vector<DTYPE> __ileakc_vec__;
    DTYPE _fb_scale_{1};

    std::filesystem::path __output_path__;
    std::string __output_prefix__;
    TDirectory* __rootDirectory__;
    bool __initialized__;
    std::size_t __max_count_png__{0}; // max no of events -> png
    std::size_t __max_count_csv__{0}; // max no of events -> csv
    std::size_t __png_count__{0};
    std::size_t __csv_count__{0};
    bool __is_active__{true};

    EventExport()
    {
        __rootDirectory__ = nullptr;
        __initialized__ = false;
        __max_count_png__ = 0;
        __max_count_csv__ = 0;
        __csv_count__ = 0;
        __png_count__ = 0;
    }


    void __check_initialized__() const
    {
        if (!__initialized__)
            throw ThisShouldNotHappenException("EventExport: not initialized");
    }

    void __export_signals__(const std::string& s_event_num, const std::string& s_pixel_index)
    {
        std::string name = "amp_pulse_ev" + s_event_num + "_px" + s_pixel_index;
        size_t size = __pulse_vec__.size();
        FILE* fp = fopen((__output_path__ / (__output_prefix__ + name + ".txt")).string().c_str(), "w");
        for (size_t i = 0; i < size; ++i)
        {
            fprintf(fp,"%g,%g\n", __time_vec__[i], __pulse_vec__[i]);
        }
        fclose(fp);
    }

    TGraph* _add_graph_(TMultiGraph* mg, std::vector<DTYPE>& vec, int col, Style_t linestyle, std::string name)
    {
        auto* graph = new TGraph(int(vec.size()), &__time_vec__[0], &vec[0]);
        graph->SetLineColor(col);
        graph->SetMarkerColor(col);
        graph->SetLineStyle(linestyle);
        mg->Add(graph);
        graph->SetName(name.c_str());
        graph->SetTitle(name.c_str());
        return graph;
    }
};

#endif // EVENTPEXPORT_H


