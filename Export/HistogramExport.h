#ifndef HISTOGRAMEXPORT_H
#define HISTOGRAMEXPORT_H

#include <string>
#include "core/config/Configuration.hpp"
#include "core/utils/unit.h"
#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH2I.h>
#include "Util/u_root.h"
#include "modules/TpxDigitizer/TpxParameter.h"
#include "core/utils/log.h"
#include <TROOT.h>
#include <TLegend.h>
#include "modules/TpxDigitizer/Pixel/Disc/DiscriminatorEvent.h"

using namespace allpix;

class HistogramExport
{
public:
    HistogramExport()
    {
        h_tot_async = nullptr;
        h_tot_tpx3 = nullptr;
        h_tot_frame = nullptr;
        h_toa_async_without_cc_time = nullptr;
        h_pxq_vs_tot = nullptr;
        h_pxq_vs_tfall_clk = nullptr;
        h_pxq_vs_toa = nullptr;
        h_pxq_vs_ampl = nullptr;
        h_ampl = nullptr;
        h_t_tot_clock_tpx3 = nullptr;
        h_t_rise_clock = nullptr;
        h_t_fall_clock = nullptr;
        h_pxq_vs_tot_clock_ns = nullptr;
        _clock_period_sec = _fclock_period_sec = 0;
        _initialized_ = false;
    }
    ~HistogramExport()
    {
        // FIXIT: ROOT pointers... , doesn't work, gives segmentation error
        /*
        delete h_tot_async;
        delete h_tot_tpx3;
        delete h_tot_frame;
        delete h_toa_async_without_cc_time;
        delete h_pxq_vs_tot;
        delete  h_pxq_vs_tfall_clk;
        delete h_pxq_vs_toa;
        delete h_pxq_vs_ampl;
        delete h_ampl;
        delete h_t_tot_clock_tpx3;
        delete h_t_rise_clock;
        delete h_t_fall_clock;
        delete h_pxq_vs_tot_clock_ns;
        */

        _clock_period_sec = _fclock_period_sec = 0;
        _initialized_ = false;
    }

    void init(DTYPE max_hist_charge_ke,
              const TpxParameter &tpx_parameter,
              const std::string output_path_override="",
              const std::string title_prefix="")
    {
        max_hist_charge_ke *= 1.05;
        if (output_path_override == "")
            _output_path = tpx_parameter.output_parm.get_output_path();
        else
            _output_path  = output_path_override;
        if (tpx_parameter.output_parm.output_histogram.value)
        {
            std::filesystem::create_directories(_output_path);
            std::filesystem::create_directories(_output_path / "ecal");
        }

        _output_prefix = tpx_parameter.output_parm.output_prefix.value;

        _clock_period_sec = tpx_parameter.disc_parm.clock_period_sec.value;
        _fclock_period_sec = tpx_parameter.disc_parm.fclock_period_sec.value;
        _ufclock_period_sec = tpx_parameter.disc_parm.ufclock_period_sec.value;
        _ratio_ToA2ToT = _ufclock_period_sec / _clock_period_sec;

        LOG(TRACE) << "Creating output plots";
        // Plot axis are in kilo electrons - convert from framework units!
        double tot_max_ns;
        if (tpx_parameter.t_acq_sec.value == 0)
        {
            tot_max_ns = 2 * max_hist_charge_ke*1e3 / tpx_parameter.preamp_parm.feedback_parm.alpha_ikrum_parm.fb_ikrum_eps.value[0] *1e9;
            tot_max_ns = std::min(tot_max_ns, 1e3);
        }
        else
            tot_max_ns = tpx_parameter.t_acq_sec.value * 1e9;

        double toa_max_ns = 2 * tpx_parameter.preamp_parm.csa_parm.tau_integrator_s.value *1e9;
        toa_max_ns+= 15 * tpx_parameter.disc_parm.tau_rise.value * 1e9;

        double tot_bin_ns = tpx_parameter.disc_parm.ufclock_period_sec.value * 1e9;
        double toa_bin_ns = tpx_parameter.disc_parm.ufclock_period_sec.value * 1e9;

        std::int32_t nbins_charge = 200;
        std::int32_t nbins_tot = std::min(16384, 2*std::int32_t(std::ceil(tot_max_ns / tot_bin_ns)));  // limit the damage
        std::int32_t nbins_tot_clock = nbins_tot;
        tot_bin_ns = tot_max_ns / nbins_tot;


        std::int32_t  nbins_toa = std::int32_t(std::ceil(toa_max_ns / toa_bin_ns));
        std::int32_t  nbins_toa_clock = std::int32_t(std::ceil(toa_max_ns / toa_bin_ns));
        std::string name = (title_prefix + "cumulative ToT");
        h_tot_frame = create_TH2I (
                    name.c_str(),
                    "cumulative ToT [ns]",
                    tpx_parameter.n_cols(),
                    0,
                    tpx_parameter.n_cols(),
                    tpx_parameter.n_rows(),
                    0,
                    tpx_parameter.n_rows());

        h_ampl = create_TH1D (
                    (title_prefix + "Max. Amplitude").c_str(),
                    "preamp aplitude [ke];preamp maximum [ke];count [1]",
                    nbins_charge,
                    0,
                    max_hist_charge_ke);

        h_tot_async = create_TH1D (
                    (title_prefix + "ToT").c_str(),
                    "Time-over-Threshold;ToT_{async} [ns];pixels [1]" ,
                    nbins_tot,
                    0,
                    tot_max_ns);

        h_tot_tpx3 = create_TH1D (
                    (title_prefix + "ToT_{Tpx3}").c_str(),
                    string_sprintf("ToT, clk %gns ;ToT_{Tpx3} [clock %gns];pixels [1]",
                                   tpx_parameter.disc_parm.fclock_period_sec.value,
                                   tpx_parameter.disc_parm.fclock_period_sec.value * 1e9).c_str(),
                    int(nbins_tot/_ratio_ToA2ToT),  // sampling bin ToAclock
                    0,
                    nbins_tot);

        h_toa_async_without_cc_time = create_TH1D (
                    (title_prefix + "ToA").c_str(),
                    "Time-of-Arrival;ToA_{async, no tcc} [ns];pixels [1]",
                    nbins_toa,
                    0,
                    toa_max_ns);

        h_pxq_vs_tot = create_TH2D (
                    (title_prefix + "csa_h_pxq_vs_tot").c_str(),
                    "ToT vs raw pixel charge;pixel charge [ke];ToT_{async} [ns]",
                    nbins_charge,
                    0,
                    max_hist_charge_ke,
                    nbins_tot,
                    0,
                    tot_max_ns);
        h_pxq_vs_tot ->SetTitleOffset(10., "z");

        h_pxq_vs_tfall_clk = create_TH2D (
                    (title_prefix + "csa_h_pxq_vs_tfall_clk").c_str(),
                    string_sprintf("t_disc_fall[%g] vs raw pixel charge;pixel charge [ke];t_disc_fall [clk]",  tpx_parameter.disc_parm.fclock_period_sec.value *1e9).c_str() ,
                    nbins_charge,
                    0,
                    max_hist_charge_ke,
                    nbins_tot_clock,
                    0,
                    nbins_tot_clock);

        h_pxq_vs_toa = create_TH2D (
                    (title_prefix + "csa_h_pxq_vs_toa").c_str(),
                    "ToA vs raw pixel charge;pixel charge [ke];ToA_{async} [ns]",
                    nbins_charge,
                    0.,
                    max_hist_charge_ke,
                    nbins_toa,
                    0.,
                    toa_max_ns);

        h_pxq_vs_ampl = create_TH2D (
                    (title_prefix + "csa_h_pxq_vs_ampl").c_str(),
                    "amplitude vs raw pixel charge;pixel charge [ke];amplitude [ke]",
                    nbins_charge,
                    0,
                    max_hist_charge_ke,
                    nbins_tot,
                    0,
                    max_hist_charge_ke);

        h_t_tot_clock_tpx3 = create_TH1D (
                    (title_prefix + "ToT_clock").c_str(),
                    string_sprintf("ToT, clk %gns ;ToT_{Tpx3} [clock %gns];pixels [1]",
                                   tpx_parameter.disc_parm.clock_period_sec.value * 1e9,
                                   tpx_parameter.disc_parm.clock_period_sec.value * 1e9).c_str(),
                    nbins_tot_clock,
                    0,
                    nbins_tot_clock);

        h_t_rise_clock = create_TH1D (
                    (title_prefix + "Disc_rise").c_str(),
                    string_sprintf("Discriminator rising edge / ToA, clk %gns ;Disc_{rise} / ToA [clock %gns];pixels [1]",
                                   tpx_parameter.disc_parm.fclock_period_sec.value * 1e9,
                                   tpx_parameter.disc_parm.fclock_period_sec.value * 1e9).c_str(),
                    nbins_toa_clock,
                    0,
                    nbins_toa_clock);

        h_t_fall_clock = create_TH1D (
                    (title_prefix + "Disc_fall").c_str(),
                    string_sprintf("Discriminator falling edge, clk %gns ;Disc_{fall} [clock %gns];pixels [1]",
                                   tpx_parameter.disc_parm.clock_period_sec.value,
                                   tpx_parameter.disc_parm.clock_period_sec.value * 1e9).c_str(),
                    nbins_toa_clock,
                    0,
                    nbins_toa_clock);

        h_pxq_vs_tot_clock_ns = create_TH2D (
                    (title_prefix + "csa_h_pxq_vs_totclk").c_str(),
                    "ToT vs raw pixel charge;pixel charge [ke]; ToT_{TPX3} [ns]",
                    nbins_charge,
                    0.,
                    max_hist_charge_ke,
                    nbins_tot,
                    0.,
                    tot_max_ns);

        h_pxq_vs_toa_clock_ns = create_TH2D (
                    (title_prefix + "csa_h_pxq_vs_toaclk").c_str(),
                    "ToA vs raw pixel charge;pixel charge [ke]; ToA_clk [ns]",
                    nbins_charge,
                    0.,
                    max_hist_charge_ke,
                    nbins_toa_clock,
                    0.,
                    nbins_toa_clock);

        _initialized_ = true;
    }

    void fill(double input_charge_ke, const DiscriminatorEvent &e, DTYPE t0_charge_induction)
    {
        check_initialized();
        if (e.is_valid())
        {
            DTYPE tot_async_ns = e.tot_async() * 1e9;
            DTYPE t_rise_async_ns = (e.t_rise_async() -t0_charge_induction)  * 1e9;
            h_tot_frame->Fill( e.col(), e.row(), tot_async_ns);
            h_tot_async->Fill(tot_async_ns);
            h_tot_tpx3->Fill(e.clk_fall() - e.fclk_rise() * _ratio_ToA2ToT);
            h_ampl->Fill(e.max_amplitude()*1e-3);
            h_toa_async_without_cc_time->Fill(t_rise_async_ns);
            h_pxq_vs_tot->Fill(input_charge_ke, tot_async_ns);

            h_pxq_vs_tfall_clk->Fill(input_charge_ke, e.clk_fall());
            h_pxq_vs_toa->Fill(input_charge_ke, t_rise_async_ns);
            h_pxq_vs_ampl->Fill(input_charge_ke, e.max_amplitude() * 1e-3);
            h_t_rise_clock->Fill(e.fclk_rise());
            h_t_fall_clock->Fill(e.clk_fall());
            h_t_tot_clock_tpx3->Fill(e.clk_fall() - e.fclk_rise() * _ratio_ToA2ToT);  // Tpx3-like
            h_pxq_vs_tot_clock_ns->Fill(input_charge_ke, (e.clk_fall() * _clock_period_sec - e.fclk_rise() * _fclock_period_sec) * 1e9); // Tpx3-like
            h_pxq_vs_toa_clock_ns->Fill(input_charge_ke, e.fclk_rise() * _fclock_period_sec * 1e9); // Tpx3-like
        }
    }


    void scale_hist()
    {
        check_initialized();
        LOG(TRACE) << "scale histograms";
        hist_scale(h_toa_async_without_cc_time);
        hist_scale(h_tot_async);
        hist_scale(h_tot_tpx3);
        hist_scale(h_ampl);
        hist_scale(h_t_rise_clock);
        hist_scale(h_t_fall_clock);
        hist_scale(h_t_tot_clock_tpx3);
        hist_scale(h_pxq_vs_ampl);
        hist_scale(h_pxq_vs_tot);
        hist_scale(h_pxq_vs_tfall_clk);
        hist_scale(h_pxq_vs_toa);
        hist_scale(h_pxq_vs_tot_clock_ns);
        hist_scale(h_pxq_vs_toa_clock_ns);
    }


    void write_root_histograms() const
    {
        check_initialized();
        LOG(TRACE) << "Writing Root histograms";
        h_tot_frame->Write();
        h_toa_async_without_cc_time->Write();
        h_tot_async->Write();
        h_tot_tpx3->Write();
        h_ampl->Write();
        h_t_rise_clock->Write();
        h_t_fall_clock->Write();
        h_t_tot_clock_tpx3->Write();
        h_pxq_vs_ampl->Write();
        h_pxq_vs_tot->Write();
        h_pxq_vs_tfall_clk->Write();
        h_pxq_vs_toa->Write();
        h_pxq_vs_tot_clock_ns->Write();
        h_pxq_vs_toa_clock_ns->Write();
    }


    void write_csv_histograms() const
    {
        check_initialized();
        // Write histograms
        csv_export(h_toa_async_without_cc_time, _output_path / ( _output_prefix +"csa_h_toa_async.csv"));

        csv_export(h_tot_async, _output_path / ( _output_prefix +"csa_h_tot_async.csv"));
        csv_export(h_tot_tpx3, _output_path / ( _output_prefix +"csa_h_tot_tpx3.csv"));
        csv_export(h_ampl, _output_path / ( _output_prefix +"amplitude.csv"));
        csv_export(h_t_rise_clock, _output_path / ( _output_prefix +"csa_h_t_rise_clock.csv"));
        csv_export(h_t_fall_clock, _output_path / ( _output_prefix +"csa_h_t_fall_clock.csv"));
        csv_export(h_t_tot_clock_tpx3, _output_path / ( _output_prefix +"csa_h_t_tot_clock_tpx3.csv"));
        csv_export(h_pxq_vs_ampl, _output_path / ( _output_prefix +"csa_h_pxq_vs_ampl.csv"));

        csv_export(h_pxq_vs_tot, _output_path / ( "ecal/" +  _output_prefix +"csa_h_pxq_vs_tot.csv"));
        csv_export(h_pxq_vs_tfall_clk, _output_path / ( "ecal/" +  _output_prefix +"csa_h_pxq_vs_tfall_clk.csv"));
        csv_export(h_pxq_vs_toa, _output_path / ( _output_prefix +"csa_h_pxq_vs_toa.csv"));
        csv_export(h_pxq_vs_tot_clock_ns, _output_path / ( "ecal/" + _output_prefix +"csa_h_pxq_vs_tot_clock.csv"));
        csv_export(h_pxq_vs_toa_clock_ns, _output_path / ( "ecal/" + _output_prefix +"csa_h_pxq_vs_toa_clock.csv"));
    }

    void write_png_histograms() const
    {
        check_initialized();
        // Write histograms
        png_export<TH1D>(h_toa_async_without_cc_time, _output_path / ( _output_prefix +"csa_h_toa_async.png"), "COLZ");
        png_export<TH1D>(h_tot_async, _output_path / ( _output_prefix +"csa_h_tot_async.png"), "COLZ");
        png_export<TH1D>(h_tot_tpx3, _output_path / ( _output_prefix +"csa_h_tot_tpx3.png"), "COLZ");
        png_export<TH1D>(h_ampl, _output_path / ( _output_prefix +"amplitude.png"), "COLZ");
        png_export<TH1D>(h_t_rise_clock, _output_path / ( _output_prefix +"csa_h_t_rise_clock.png"), "COLZ");
        png_export<TH1D>(h_t_fall_clock, _output_path / ( _output_prefix +"csa_h_t_fall_clock.png"), "COLZ");
        png_export<TH1D>(h_t_tot_clock_tpx3, _output_path / ( _output_prefix +"csa_h_t_tot_clock_tpx3.png"), "COLZ");
        png_export<TH2D>(h_pxq_vs_ampl, _output_path / ( _output_prefix +"csa_h_pxq_vs_ampl.png"), "COLZ");
        png_export<TH2D>(h_pxq_vs_toa, _output_path / ( _output_prefix +"csa_h_pxq_vs_toa.png"), "COLZ");

        png_export<TH2D>(h_pxq_vs_tot, _output_path / ( "ecal/" + _output_prefix +"_csa_h_pxq_vs_tot" + ".png"), "COLZ");
        png_export<TH2D>(h_pxq_vs_tfall_clk, _output_path / ( "ecal/" + _output_prefix +"csa_h_pxq_vs_tfall_clk.png"), "COLZ");
        png_export<TH2D>(h_pxq_vs_tot_clock_ns, _output_path / ( "ecal/" + _output_prefix +"csa_h_pxq_vs_tot_clock.png"), "COLZ");
        png_export<TH2D>(h_pxq_vs_toa_clock_ns, _output_path / ( "ecal/" + _output_prefix +"csa_h_pxq_vs_toa_clock.png"), "COLZ");
        png_export<TH2I>(h_tot_frame, _output_path / ( _output_prefix +"ToT.png"), "COLZ");
    }

    void finalize()
    {
        h_tot_async->Reset();
        h_tot_tpx3->Reset();
        h_tot_frame->Reset();
        h_toa_async_without_cc_time->Reset();
        h_pxq_vs_tot->Reset();
        h_pxq_vs_tfall_clk->Reset();
        h_pxq_vs_toa->Reset();
        h_pxq_vs_ampl->Reset();
        h_ampl->Reset();
        h_t_tot_clock_tpx3->Reset();
        h_t_rise_clock->Reset();
        h_t_fall_clock->Reset();
        h_pxq_vs_tot_clock_ns->Reset();

        h_tot_async->Clear();
        h_tot_tpx3->Clear();
        h_tot_frame->Clear();
        h_toa_async_without_cc_time->Clear();
        h_pxq_vs_tot->Clear();
        h_pxq_vs_tfall_clk->Clear();
        h_pxq_vs_toa->Clear();
        h_pxq_vs_ampl->Clear();
        h_ampl->Clear();
        h_t_tot_clock_tpx3->Clear();
        h_t_rise_clock->Clear();
        h_t_fall_clock->Clear();
        h_pxq_vs_tot_clock_ns->Clear();
    }

private:

    std::filesystem::path _output_path;
    std::string _output_prefix;
    TH1D *h_tot_async;
    TH1D *h_toa_async_without_cc_time;
    TH1D *h_ampl;
    TH1D *h_tot_tpx3;  // tpx3 like: t_fall_clock [ns] - t_rise_clock [s]
    TH2I *h_tot_frame;
    TH2D * h_pxq_vs_tot;
    TH2D * h_pxq_vs_tfall_clk;
    TH2D * h_pxq_vs_toa;
    TH2D * h_pxq_vs_ampl;

    TH1D *h_t_tot_clock_tpx3;
    TH1D *h_t_rise_clock;
    TH1D *h_t_fall_clock;
    TH2D * h_pxq_vs_tot_clock_ns;
    TH2D * h_pxq_vs_toa_clock_ns;
    DTYPE _clock_period_sec;
    DTYPE _fclock_period_sec;
    DTYPE _ufclock_period_sec;
    DTYPE _ratio_ToA2ToT;
    bool _initialized_;

    void check_initialized() const
    {
        if (!_initialized_)
        {
            throw ThisShouldNotHappenException("HistogramExport not initialized");
        }
    }

    void hist_scale(TH1D *h)
    {
        std::int32_t x = h->FindLastBinAbove(0, 1);
        h->GetXaxis()->SetRangeUser(0, 1.1 * h->GetXaxis()->GetBinCenter(x));

        double max_y = h->GetMaximum();
        h->GetYaxis()->SetRangeUser(0, 1.1 * max_y);
    }

    void hist_scale(TH2D *h)
    {
        std::int32_t x = h->FindLastBinAbove(0, 1);
        std::int32_t y = h->FindLastBinAbove(0, 2);
        h->GetXaxis()->SetRangeUser(0, 1.1 * h->GetXaxis()->GetBinCenter(x));
        h->GetYaxis()->SetRangeUser(0, 1.1 * h->GetYaxis()->GetBinCenter(y));
    }
};

#endif // HISTOGRAMEXPORT_H
