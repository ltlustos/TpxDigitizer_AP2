#pragma once
#ifndef CONST_H
#define CONST_H
#include "Common/dtype.h"
const DTYPE EV_PER_EH_SI = 3.62;
const DTYPE QE = 1.602176634e-19;
const DTYPE RT = 295;
const DTYPE T_ref = 295;
#endif // CONST_H
