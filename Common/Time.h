#ifndef TIME_H
#define TIME_H

#include "Common/dtype.h"
#include "stdio.h"
#include "core/utils/log.h"

class Time
{
public:
    Time()
    {
        setup(0, 0);
    }

    Time(DTYPE _dt, DTYPE t0=0)
    {
        setup(_dt, t0);
    }

    ~Time() = default;
    void setup(DTYPE _dt, DTYPE t0)
    {
        __now__ = t0;
        __dt__ = _dt;
        __dt_ns__ = __dt__ * 1e9;
    }

    void reset(DTYPE t0=0)
    {
        __now__ = t0;
    }

    void next()
    {
        __now__ += __dt__;

    }

    std::size_t digitize(DTYPE clock_period, DTYPE t, DTYPE offset=0)
    {
        return std::size_t(std::ceil((t + offset) / clock_period));
    }

    DTYPE now() const
    {
        return __now__;
    }

    DTYPE now_ns() const
    {
        return __now__ * 1e9;
    }

    DTYPE dt() const
    {
        return __dt__;
    }

    DTYPE dt_ns() const
    {
        return __dt_ns__;
    }

private:
    DTYPE __now__;
    DTYPE __dt__;
    DTYPE __dt_ns__;

};

#endif // TIME_H

