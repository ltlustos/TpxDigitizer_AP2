/* Mpx3Sim ver. 0.1, 2018.08.27 */
/* Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>
#include "cstdio"
#include <cstdarg>
#include <string>
#include "Util/u_string.h"
#define ERR_MSG_SIZE 2046
class _AbstractException_: public std::exception
{

public:
    _AbstractException_()
    {
        initialize(-1, "%s", "none");
    }

    template <typename ...Args>
    _AbstractException_(int _id, const char *_msg,  Args && ...args){
        initialize(_id, _msg, args...);
    }

    template <typename ...Args>
    _AbstractException_(const char *_msg,  Args && ...args){
        initialize(-1, _msg, args...);
    }

    virtual const char* what() const throw()
    {
        return this->msg.c_str();
    }

protected:
    int id;
    std::string msg;

    template <typename ...Args>
    void initialize(int _id, const char *fmt,  Args && ...args){
        id = _id;
        msg = string_sprintf(fmt, args...);
    }

};

class ThisIsABug: public _AbstractException_
{
public:
    ThisIsABug(void):_AbstractException_()
    {}
    template <typename ...Args>
    ThisIsABug(int _id, const char *fmt,  Args && ...args):
        _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    ThisIsABug(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}

};

class ParameterException: public _AbstractException_
{
public:
    ParameterException(void):_AbstractException_()
    {}

    template <typename ...Args>
    ParameterException(int _id, const char *fmt, Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    ParameterException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}

};



class LimitsException: public _AbstractException_
{
public:
    LimitsException(void):_AbstractException_()  {}

    template <typename ...Args>
    LimitsException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    LimitsException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};

class CheckException: public _AbstractException_
{
public:
    CheckException(void):_AbstractException_()  {}

    template <typename ...Args>
    CheckException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    CheckException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};

class UnusedException: public _AbstractException_
{
public:
    UnusedException(void):_AbstractException_()  {}

    template <typename ...Args>
    UnusedException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    UnusedException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};

class ParameterOutOfRangeException: public _AbstractException_
{
public:
    ParameterOutOfRangeException(void):_AbstractException_(){}

    template <typename ...Args>
    ParameterOutOfRangeException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    ParameterOutOfRangeException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
    ParameterOutOfRangeException(double min, double max, double val):_AbstractException_("Parameter %g out of range [%g, %g]", val, min, max) {}

};


class NotFoundException: public _AbstractException_
{
public:
    NotFoundException(void):_AbstractException_(){}

    template <typename ...Args>
    NotFoundException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    NotFoundException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};



class ForbiddenException: public _AbstractException_
{
public:
    ForbiddenException(void):_AbstractException_(){}

    template <typename ...Args>
    ForbiddenException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    ForbiddenException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};


class SubclassResponsabilityException: public _AbstractException_
{
public:
    SubclassResponsabilityException(void)
        :_AbstractException_()
    {}

    template <typename ...Args>
    SubclassResponsabilityException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    SubclassResponsabilityException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};

class NotYetImplementedException: public _AbstractException_
{
public:
    NotYetImplementedException(void):_AbstractException_(){}

    template <typename ...Args>
    NotYetImplementedException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    NotYetImplementedException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};


class ShouldNotBeUsedException: public _AbstractException_
{
public:
    ShouldNotBeUsedException(void):_AbstractException_(){}

    template <typename ...Args>
    ShouldNotBeUsedException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    ShouldNotBeUsedException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};



class ThisShouldNotHappenException: public _AbstractException_
{
public:
    ThisShouldNotHappenException(void):_AbstractException_(){}

    template <typename ...Args>
    ThisShouldNotHappenException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    ThisShouldNotHappenException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
    ThisShouldNotHappenException(int _id, char const* _msg):_AbstractException_(_id, _msg){}
};



class ParseException: public _AbstractException_
{
public:
    ParseException(void):_AbstractException_()  {}

    template <typename ...Args>
    ParseException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    ParseException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};


class NaNException: public _AbstractException_
{
public:
    NaNException(void):_AbstractException_()  {}

    template <typename ...Args>
    NaNException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    NaNException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};

class INFException: public _AbstractException_
{
public:
    INFException(void):_AbstractException_()  {}

    template <typename ...Args>
    INFException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    INFException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};

class FileNotFoundNException: public _AbstractException_
{
public:
    FileNotFoundNException(void):_AbstractException_()  {}

    template <typename ...Args>
    FileNotFoundNException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    FileNotFoundNException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};


class OtherNException: public _AbstractException_
{
public:
    OtherNException(void):_AbstractException_()  {}

    template <typename ...Args>
    OtherNException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    OtherNException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
};


class OutOfRangeException: public _AbstractException_
{
public:
    OutOfRangeException(void):_AbstractException_()  {}

    template <typename ...Args>
    OutOfRangeException(int _id, const char *fmt,  Args && ...args):  _AbstractException_(_id, fmt, args...)    {}

    template <typename ...Args>
    OutOfRangeException(const char *fmt,  Args && ...args):   _AbstractException_(fmt, args...)  {}
    };

#endif // EXCEPTIONS_H
