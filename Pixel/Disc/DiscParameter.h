#ifndef DISCPARAMETER_H
#define DISCPARAMETER_H
#include "Common/dtype.h"
#include "Util/u_math.h"
#include "math.h"
#include "modules/TpxDigitizer/Parameter/_ParameterSet_.h"
#include "modules/TpxDigitizer/Pixel/Preamp/Parameter/NoiseChannelParameter.h"
#include "modules/TpxDigitizer/Parameter/Parameter.h"

class DiscParameter: public _Parameter_
{
public:

    enum class DiscType : int8_t {
        IDEAL=0, //  ideal, inf. bandwidth, rectangular preamp
        INTEGRATOR=1,
        ASYM=2
    };

    PARAMETER::TEnumParameter<DiscParameter::DiscType> type{"disc_type", false, DiscType::ASYM};
    PARAMETER::PathParameter threshold_config_file{"threshold_config", false, ""};
    PARAMETER::TParameter<DTYPE> threshold{"threshold", true, 750, "e", "discriminator threshold"};
    PARAMETER::TParameter<DTYPE> threshold_sigma {"threshold_sigma", true, 0, "e", "interpixel discriminator threshold dispersion"};
    PARAMETER::TParameter<DTYPE>tau_rise{ "disc_tau_rise", false, 2.0e-9, "s", "discriminator rise time constant"};
    PARAMETER::TParameter<DTYPE>tau_fall{ "disc_tau_fall", false, 15.0e-9, "s", "discriminator fall time constant"};

    PARAMETER::TParameter<DTYPE> glitch_filter_tau {"disc_glitch_tau", false, 411.0e-9, "s", "paralysis intervall after hit (Tpx3 411ns), if <0: will be set to clockToT_sec"};
    PARAMETER::TParameter<DTYPE>clock_period_sec{"clock_period", false, 25.0e-9, "s", "coarse clock period"};
    PARAMETER::TParameter<DTYPE>fclock_period_sec{"fclock_period", false, 25.0e-9 / 16, "s", "fine clock period"};
    PARAMETER::TParameter<DTYPE>ufclock_period_sec{"ufclock_period", false, 25.0e-9 / 16 / 8, "s", "ultra fine coarse clock period"};

    NoiseChannel_Parameter threshold_noise_parm{"threshold_noise_enc", 7,
                                                "threshold_noise_white", 0.0,
                                                "threshold_noise_bw", {10.0, 1.0e5}};  // bw_cutoff: this is guesswork


    DiscParameter() = default;
    virtual ~DiscParameter() = default;

    DiscParameter& operator = (const DiscParameter& o) = default;

    DTYPE random_t_clock_offset_sec(allpix::RandomNumberGenerator& random_generator)
    {
        allpix::uniform_real_distribution<DTYPE> rand_unit(0, fclock_period_sec.value);  // FIXIT for TP clockphase drift within fastest clock for TPX4
        return rand_unit(random_generator);
    }

    void check_values() const override
    {
        throw ThisShouldNotHappenException("DiscParameter::check_values()");
    }

    void check_values(DTYPE dt_sec) const
    {
//        threshold.check_ge(0.0);
        threshold_sigma.check_in_range( 0., 100.0);
        threshold_noise_parm.ENC_bw.check_in_range( 0., std::abs(threshold.value/6));
        tau_rise.check_in_range( dt_sec, 55e-9);
        tau_fall.check_in_range( dt_sec, 155e-9);

        ufclock_period_sec.check_ge( dt_sec);
        fclock_period_sec.check_gt( ufclock_period_sec.value);
        clock_period_sec.check_gt( fclock_period_sec.value);

        threshold_noise_parm.check_values();
    }

    void adjust_values() override
    {
        if (glitch_filter_tau.value < 0)
            glitch_filter_tau.value =  clock_period_sec.value;
        threshold_noise_parm.adjust_values();
    }

    void adjust_timestep_values(DTYPE dt_sec)
    {
        threshold_noise_parm.adjust_timestep_values(dt_sec);
    }




    void parse_config(const allpix::Configuration &config) override
    {
        type.from_parameter_file(config);
        threshold_config_file.from_parameter_file(config);
        threshold.from_parameter_file(config);
        threshold_sigma.from_parameter_file(config);
        threshold_noise_parm.from_parameter_file(config);
        // coarse clock
        clock_period_sec.from_parameter_file(config);
         // fine clock
        fclock_period_sec.from_parameter_file(config);
         // ultra fine clock
        ufclock_period_sec.from_parameter_file(config);
        tau_rise.from_parameter_file(config);
        tau_fall.from_parameter_file(config);
        glitch_filter_tau.from_parameter_file(config);

    }
};

#endif // DISCPARAMETER_H
