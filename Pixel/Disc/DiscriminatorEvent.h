/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#pragma once

#include "modules/TpxDigitizer/Common/dtype.h"
#include "modules/TpxDigitizer/Util/u_string.h"
#include "Clock.h"
#define DISC_NONE -1

class DiscriminatorEvent
{
public:
    DiscriminatorEvent(
            std::size_t row,
            std::size_t col,
            const std::unique_ptr<Clock> &clock,
            DTYPE t_rise,
            DTYPE t_fall,
            DTYPE max_amplitude,
            DTYPE t_max_amplitude)
    {
        _row = row;
        _col = col;
        _t_rise_async = t_rise;
        _t_fall_async = t_fall;

        clock->digitize(_t_rise_async, _clk_rise_, _fclk_rise_, _ufclk_rise_);
        clock->digitize(_t_fall_async, _clk_fall_, _fclk_fall_, _ufclk_fall_);

        _max_amplitude = max_amplitude;
        _t_max_amplitude =t_max_amplitude;
    }

    bool is_valid() const
    {
        return _t_rise_async != DISC_NONE && _t_fall_async > 0;
    }

    std::size_t row(void) const
    {
        return _row;
    }

    std::size_t col(void) const
    {
        return _col;
    }

    DTYPE  tot_async() const
    {
        return _t_fall_async - _t_rise_async;
    }


    DTYPE  t_rise_async() const
    {
        return _t_rise_async;
    }

    DTYPE  t_fall_async() const{
        return _t_fall_async;
    }

    std::size_t  clk_rise() const
    {
        return _clk_rise_;
    }

    std::size_t  fclk_rise() const
    {
        return _fclk_rise_;
    }

    std::size_t  ufclk_rise() const
    {
        return _ufclk_rise_;
    }

    std::size_t  clk_fall() const
    {
        return _clk_fall_;
    }

    std::size_t  fclk_fall() const
    {
        return _fclk_fall_;
    }

    std::size_t  ufclk_fall() const
    {
        return _ufclk_fall_;
    }

    DTYPE  max_amplitude() const
    {
        return _max_amplitude;
    }

    DTYPE  t_peak() const
    {
        return _t_max_amplitude;
    }

    std::string info_str() const
    {
        return string_sprintf( "row= %3lu, col = %3lu, t_rise = %9.2fns, t_fall = %7.2fns, max_amplitude = %6.1fe, t_max =  %7.2fns",
                               _row,
                               _col,
                               _t_rise_async *1e9,
                               _t_fall_async *1e9,
                               _max_amplitude,
                               _t_max_amplitude * 1e9);
    }

private:
    std::size_t _row{};
    std::size_t _col{};
    DTYPE _t_rise_async{};
    DTYPE _t_fall_async{};
    DTYPE _max_amplitude{};
    DTYPE _t_max_amplitude{};
    std::size_t _clk_rise_{};
    std::size_t _fclk_rise_{};
    std::size_t _ufclk_rise_{};
    std::size_t _clk_fall_{};
    std::size_t _fclk_fall_{};
    std::size_t _ufclk_fall_{};

};
