#ifndef CLOCK_H
#define CLOCK_H

#include "../../Common/dtype.h"
#include "stdio.h"
#include <array>

class Clock
{
public:
    Clock() = delete;

    Clock(DTYPE clock_period_sec, DTYPE fclock_period_sec, DTYPE ufclock_period_sec, DTYPE offset_sec=0)
    {
        __clock_period_sec__ = clock_period_sec;
        __fclock_period_sec__ = fclock_period_sec;
        __ufclock_period_sec__ = ufclock_period_sec;
        __offset_sec__ = offset_sec;
    }
    ~Clock() = default;
    void digitize(DTYPE t_sec, std::size_t &clock, std::size_t &fclock, std::size_t &ufclock) const
    {
        clock = __digitize__(t_sec, __clock_period_sec__);
        fclock = __digitize__(t_sec, __fclock_period_sec__);
        ufclock = __digitize__(t_sec, __ufclock_period_sec__);
    }

    DTYPE clock_period_sec() const
    {
        return __clock_period_sec__;
    }

    DTYPE fclock_period_sec() const
    {
        return __fclock_period_sec__;
    }

    DTYPE ufclock_period_sec() const
    {
        return __ufclock_period_sec__;
    }

    DTYPE offset_sec() const
    {
        return __offset_sec__;
    }

private:
    DTYPE  __clock_period_sec__;
    DTYPE  __fclock_period_sec__;
    DTYPE  __ufclock_period_sec__;
    DTYPE __offset_sec__;
    std::size_t __digitize__(DTYPE t_sec, DTYPE period) const
    {
        return std::size_t(std::ceil((t_sec - __offset_sec__) / period));
    }
};
#endif // CLOCK_H
