/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef DISC_H
#define DISC_H

#include "Common/dtype.h"
#include <vector>
#include "DiscParameter.h"
#include "Util/u_math.h"
#include "DiscriminatorEvent.h"
#include "H/H_Integrator.h"
#include "H/H_Differentiator.h"
#include "H/H_DeltaExp.h"
#include "H/H_SquareExp.h"
#include "H/H_Gaussian.h"
#include "H/H_Ramp.h"
#include "math.h"
#include "modules/TpxDigitizer/Parameter/_ParameterSet_.h"
#include "Pixel/Preamp/NoiseChannel.h"
#include "Common/Time.h"
#include "Clock.h"
#include "Butterworth/Lowpass.h"
/************************************************************************************************************/
class Comparator
{
public:

    Comparator()
    {
        reset();
    }

    void reset()
    {
        _state = _last_state = false;
    }
    bool process(DTYPE in, DTYPE threshold)
    {
        _last_state = _state;
        _state = (in >= threshold) ;
        return _state;
    }

    bool state() const
    {
        return _state;
    }

    bool is_rising() const
    {
        return _state  && !_last_state;
    }

    bool is_falling() const
    {
        return _last_state  && !_state;
    }

    DTYPE edge() const
    {
        return _state - _last_state;
    }

private:
    bool _state;
    bool _last_state;
};


/************************************************************************************************************/
// Abstract Base Class
class _Disc_
{
public:
    std::vector<DiscriminatorEvent*> events;
    _Disc_(const DiscParameter &parm, Time &time, std::size_t row=0, std::size_t col=0)
    {
        _row = row;
        _col = col;
        _time = &time;
        _polarity = sign(parm.threshold.value);
        _threshold = parm.threshold.value * _polarity;
        _glitch_filter_tau = parm.glitch_filter_tau.value;
        _clock_offset_s_ = 0;

        _noise_channel_ = std::make_unique<NoiseChannel_BP>(parm.threshold_noise_parm, time.dt());
        _clock_ = std::make_unique<Clock>(parm.clock_period_sec.value, parm.fclock_period_sec.value, parm.ufclock_period_sec.value);
        reset();
    }

    virtual ~_Disc_()
    {
        clear_events();
    }

    void set_position(const std::size_t row, const std::size_t col)
    {
        _row = row;
        _col = col;
    }

    void set_threshold(DTYPE val)
    {
        _threshold = val * _polarity;
        reset();
    }

    std::size_t row() const
    {
        return _row;
    }

    std::size_t col() const
    {
        return _col;
    }

    DTYPE threshold() const
    {
        return _threshold * _polarity;
    }

    DTYPE current_threshold() const
    {
        return _current_threshold;
    }


    virtual void reset(DTYPE time0=0)
    {
        clear_events();
        _clock_offset_s_ = time0;
        _input = _last_input = 0;
        _max_amplitude = 0;
        _latched = false;
        _current_threshold = _last_threshold = _threshold;
        _t_rise = 0;
        _t_fall = - - 1.1*_glitch_filter_tau;
        _out = _out_last =0;
        _t_comp_falling = _t_max_amplitude = - 1.1 * _glitch_filter_tau;
        _comparator_.reset();
    }


    virtual void process(allpix::RandomNumberGenerator& random_generator, DTYPE in)
    {
        _last_input = _input;
        _input = in * _polarity;
        _process_(random_generator); // implemeted in subclasses
        check_max_amplitude();
    }

    virtual void unlatch(bool register_event=true) = 0;
    virtual void latch() = 0;

    void closeShutter()
    {
        if (_latched)
        {
            _force_unlatch_(true);
        }
    }

    virtual double out() const
    {
        return _out;
    }

    virtual int polarity() const
    {
        return int(_polarity);
    }

    virtual bool latched() const
    {
        return _latched;
    }

    DTYPE edge() const
    {
        return _out - _out_last;
    }
    Comparator _comparator_;

protected:

    std::size_t _row;
    std::size_t _col;
    DTYPE _threshold; // polarity indepedent threshold
    DTYPE _polarity;
    Time *_time;

    std::unique_ptr<Clock> _clock_;
    DTYPE _glitch_filter_tau;
    DTYPE _clock_offset_s_;
    DTYPE _input, _last_input;
    DTYPE _current_threshold, _last_threshold;
    DTYPE _t_rise, _t_fall;
    DTYPE _t_comp_rising, _t_comp_falling;
    DTYPE _max_amplitude, _t_max_amplitude;
    bool _latched;
    DTYPE _out;
    DTYPE _out_last;
    std::unique_ptr<NoiseChannel_BP> _noise_channel_;

    virtual void _process_(__attribute__((unused)) allpix::RandomNumberGenerator& random_generator) = 0;

    void clear_events()
    {
        while (!events.empty())
        {
            delete events.back();
            events.pop_back();
        }
    }

    void log_rising() const
    {
        if (events.size() > 0)
        {
            LOG(DEBUG) << "additional hit rising disc ["<<_row << ", " << _col << "] rising @" << _time->now()* 1e9
                       << "ns, _t_rise_disc " << _t_rise *1e9
                       << "ns, _input " << _input
                       << "e, last_input " << _last_input
                       << "e, threshold " << _threshold
                       << ", _t_fall_last " << _t_comp_falling *1e9 <<"ns";
        } else {
            LOG(DEBUG) << "disc ["<<_row << ", " << _col << "] rising @" << _time->now()* 1e9
                       << "ns, _t_rise_disc " << _t_rise *1e9
                       << "ns, _input " << _input
                       << "e, last_input " << _last_input
                       << "e, threshold " << _threshold
                       << ", _t_fall_last " << _t_comp_falling *1e9 <<"ns";
        }
    }

    void log_falling() const
    {
        if (events.size() > 1)
        {
            LOG(DEBUG) << "additional hit falling disc ["<<_row << ", " << _col << "] rising @" << _time->now()* 1e9
                       << "ns, _t_fall_disc " << _t_fall *1e9
                       << "ns, _input " << _input
                       << "e, last_input " << _last_input
                       << "e, threshold " << _threshold << "e";

            LOG(DEBUG) << "Hitlist ";
            for (auto e : events)
                LOG(DEBUG) << e->info_str();
        } else {
            LOG(DEBUG) << "disc ["<<_row << ", " << _col << "] falling @" << _time->now()* 1e9
                       << "ns, _t_fall_disc " << _t_fall *1e9
                       << "ns, _input " << _input
                       << "e, last_input " << _last_input
                       << "e, threshold " << _threshold << "e";
        }
    }

    void check_max_amplitude()
    {
        if (_latched && _input > _max_amplitude)
        {
            _t_max_amplitude = _time->now();
            _max_amplitude = _input;
        }
    }

    virtual DTYPE _interpolate_dt_() = 0;
    void _register_event_(){
        events.emplace_back(new DiscriminatorEvent(_row,
                                                   _col,
                                                   _clock_,
                                                   _t_rise,
                                                   _t_fall,
                                                   _max_amplitude,
                                                   _t_max_amplitude
                                                   ));
    }
    void _force_unlatch_(bool register_event)
    {
        _t_comp_falling = _time->now();
        _t_fall = _clock_offset_s_ + _time->now();
        _latched = false;
        if (register_event)
            _register_event_();
        log_falling();
        _max_amplitude = _t_max_amplitude = 0.0;
    }
};


/************************************************************************************************************/

class DiscIdeal: public _Disc_
{
public:
    DiscIdeal(const DiscParameter &parm, Time &clock)
        : _Disc_(parm, clock)
    {}

    virtual ~DiscIdeal()
    {}

    void _process_(allpix::RandomNumberGenerator& random_generator) override
    {
        _last_threshold = _current_threshold;
        _current_threshold = _threshold + _noise_channel_->next(random_generator);
        _out = _comparator_.process(_input, _current_threshold);

        if (_comparator_.is_rising())
        {
            if (_time->now() > _t_comp_falling + _glitch_filter_tau )
            {
                latch();
                _t_comp_rising = _time->now();
            }

        }
        else if (_comparator_.is_falling())
        {
            if (_time->now() > _t_comp_falling + _glitch_filter_tau)
                unlatch();
            _t_comp_falling = _time->now();
        }
    }
protected:
    virtual DTYPE _interpolate_dt_() final
    {
        DTYPE r= - _time->dt() * (_input - _current_threshold) / (_input - _last_input - _current_threshold + _last_threshold);
        if (std::abs(r) > _time->dt())
            throw ThisShouldNotHappenException("%g, %g >%g", r, std::abs(r), _time->dt());
        return r;
    }
private:
    virtual void latch()
    {
        if (_time->now() > _t_comp_falling + _glitch_filter_tau )
        {
            _latched = true;
            _t_rise = _clock_offset_s_ + _time->now();
            _t_rise += _interpolate_dt_();
            log_rising();
        }
    }

    virtual void unlatch(bool register_event=true)
    {
        _t_fall = _clock_offset_s_ + _time->now();
        _t_fall += _interpolate_dt_();
        if (_latched && (_t_fall - _t_rise) > _clock_->clock_period_sec())
        {
            _latched = false;
            if (register_event)
                _register_event_();
            log_falling();
            _max_amplitude = _t_max_amplitude = 0.0;
        }
    }
};

/************************************************************************************************************/
class DiscInt: public _Disc_
{
public:
    DiscInt(const DiscParameter &parm, Time &clock)
        : _Disc_(parm, clock)
    {
        _integrator_ = std::make_unique<H_Integrator>(parm.tau_rise.value, _time->dt(), 1.0e-1, true);
        _integrator_->normalize_integral_h();
    }

    virtual ~DiscInt() = default;

    void reset(DTYPE time0=0) override
    {
        _Disc_::reset(time0);
        _integrator_->reset();
    }

protected:
    DTYPE _last_out_{0};

    virtual void _process_(allpix::RandomNumberGenerator& random_generator) override
    {

        _last_threshold = _current_threshold;
        _current_threshold = _threshold + _noise_channel_->next(random_generator);
        _comparator_.process(_input, _current_threshold);
        _last_out_ = _out;
        _out = _integrator_->process_input( _comparator_.edge());
        _state_disc_.process(_out, 0.5);
        if (_state_disc_.is_rising())
        {
            latch();
            _t_comp_rising = _time->now();
        }
        else if (_state_disc_.is_falling())
        {
            // glitch filtering, only pulses longer than 1 ToT clock cycle and with an interval between subsequent events > _glitch_filter_tau
            unlatch((_time->now() -_t_comp_falling) > _glitch_filter_tau && (_time->now()- _t_rise) > _clock_->clock_period_sec());
            _t_comp_falling = _time->now();
        } else if (_latched && !_state_disc_.state() && (_time->now() - _t_comp_falling) > _clock_->clock_period_sec()) {
            unlatch(true);
        }
    }

private:
    std::unique_ptr<H_Integrator> _integrator_;
    Comparator _state_disc_;

    void latch()
    {
        if (_time->now() > _t_comp_falling + _glitch_filter_tau )
        {
            _latched = true;
            _t_rise = _clock_offset_s_ + _time->now();
            _t_rise += _interpolate_dt_();
            log_rising();
        }
    }

    void unlatch(bool register_event)
    {
        _t_fall = _clock_offset_s_ + _time->now();
        _t_fall += _interpolate_dt_();
        if (_latched && ( (_t_fall - _t_rise) > _clock_->clock_period_sec() || ( (_time->now() - _t_comp_falling) > _clock_->clock_period_sec())) )
        {
            _latched = false;
            if (register_event)
                _register_event_();
            log_falling();
            _max_amplitude = _t_max_amplitude = 0.0;
        }
    }
    virtual DTYPE _interpolate_dt_() final
    {
        return -_time->dt() * (_out - 0.5) / (_out - _last_out_);
    }
};


/************************************************************************************************************/
class DiscAsymmetric: public _Disc_
{
public:
    DiscAsymmetric(const DiscParameter &parm, Time &time)
        : _Disc_(parm, time)
    {
        _integrator_ = std::make_unique<H_Saturated_Integrator>(3*_time->dt(), _time->dt(), 0.0, 1.0, time.dt(), true);
        _rising_ = std::make_unique<H_Differentiator>(parm.tau_rise.value, _time->dt(), 1.0e-20, true);
        _falling_ = std::make_unique<H_SquareExp>(0.30844915256542504 * parm.tau_fall.value, _time->dt(), 1.0e-20, true); // = 1-exp(-1) @ t = parm.tau_fall
        _rising_->normalize_integral_h();
        _falling_->normalize_integral_h();
        _integrator_->normalize_integral_h();  // FIXIT CHECK AMPLITUDE
    }

    virtual ~DiscAsymmetric() = default;

    void reset(DTYPE time0=0) override
    {
        _Disc_::reset(time0);
        _integrator_->reset();
        _falling_->reset();
        _rising_->reset();
        _state_disc_.reset();

#ifdef __DEBUG__
        __comparator_fall_time__ = 0;
        __comparator_rise_time__ = 0;
#endif
    }

protected:


    virtual void _process_(allpix::RandomNumberGenerator& random_generator) override
    {

        _last_threshold = _current_threshold;
        _current_threshold = _threshold + _noise_channel_->next(random_generator);
        _comparator_.process(_input, _current_threshold);
        if (_comparator_.edge() == 1)
        {
#ifdef __DEBUG__
            __comparator_rise_time__ = _time->now();
#endif
            _rising_->input_add(1.0);
            _falling_->reset();
        }
        else if (_comparator_.edge() == -1)
        {
#ifdef __DEBUG__
            __comparator_fall_time__ = _time->now();
#endif
            _rising_->reset();
            _falling_->input_add(-1.0);
        }


        DTYPE in = _falling_->process() + _rising_->process();
        sum += in;
        _last_out_ = _out;
        _out = _integrator_->process_input(in);
        _state_disc_.process(_out, 0.5);

        if (_state_disc_.is_rising() || (!_latched && _state_disc_.state()) )
        {
            latch();
            _t_comp_rising = _time->now();
#ifdef __DEBUG__
            LOG(DEBUG) << "\ndelta_t rise =" << ((_t_comp_rising - __comparator_rise_time__) * 1e9) << " ns";
#endif
        }
        else if (_state_disc_.is_falling())
        {
            // glitch filtering, only pulses longer than 1 ToT clock cycle and with an interval between subsequent events > _glitch_filter_tau
            if (_latched)
                unlatch( (_time->now()- _t_rise) > _clock_->clock_period_sec());
            _t_comp_falling = _time->now();
#ifdef __DEBUG__
            LOG(DEBUG) << "\ndelta_t fall=" << ((_t_comp_falling - __comparator_fall_time__) * 1e9) << " ns";
#endif
        } else if (_latched && !_state_disc_.state() && (_time->now() - _t_comp_falling) > _clock_->clock_period_sec()) {
            unlatch(true);
        }
    }

private:
    std::unique_ptr<H_Saturated_Integrator> _integrator_;
    std::unique_ptr<H_Differentiator> _rising_;
    std::unique_ptr<H_SquareExp> _falling_;
    Comparator _state_disc_;
    DTYPE sum{0};
    DTYPE _last_out_{0};
#ifdef __DEBUG__
    DTYPE __comparator_fall_time__;
    DTYPE __comparator_rise_time__;
#endif

    void latch()
    {
        if (_time->now() > _t_comp_falling + _glitch_filter_tau )
        {
            _latched = true;
            _t_rise = _clock_offset_s_ + _time->now();
            _t_rise += _interpolate_dt_();
            log_rising();
        }
    }

    void unlatch(bool register_event)
    {
        _t_fall = _clock_offset_s_ + _time->now();
        _t_fall += _interpolate_dt_();
        if (_latched && ( (_t_fall - _t_rise) > _clock_->clock_period_sec() ||
                         ( (_time->now() - _t_comp_falling) > _clock_->clock_period_sec())) )
        {
            _latched = false;
            if (register_event)
                _register_event_();
            log_falling();
            _max_amplitude = _t_max_amplitude = 0.0;
        }
    }

    virtual DTYPE _interpolate_dt_() final
    {
        DTYPE offs = _out == _last_out_ ? 0.0 : -_time->dt() * (_out - 0.5) / (_out - _last_out_);
        if (std::abs(offs) > _time->dt())
            throw ThisShouldNotHappenException("Disc::_interpolate_dt_");
        return offs;
    }
};

#endif // DISC_H

