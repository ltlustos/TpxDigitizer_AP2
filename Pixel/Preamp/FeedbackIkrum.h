#pragma once
/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#include "Parameter/FeedbackParameter.h"
#include "H/H_Differentiator.h"
#include "Taper/_TaperFunction_.h"
#include "AlphaIkrum.h"
#include "Common/Time.h"
#include "H/H_Differentiator.h"
#include "ProportionalNoiseChannel.h"
#include "Preamp.h"

class Preamp;
class FeedbackIkrum
{
protected:
    FeedbackIkrum() = default;
public:
    FeedbackIkrum(const FeedbackParameter &parm, Time& time, Preamp &preamp, DTYPE h_cutoff);
    virtual ~FeedbackIkrum() = default;
    FeedbackIkrum(const FeedbackIkrum &other) = delete;
    FeedbackIkrum& operator =(const FeedbackIkrum& other) = delete;
    virtual DTYPE process_input(allpix::RandomNumberGenerator& random_generator);
    virtual void reset();  
    virtual void set_alpha_ikrum(const std::vector<DTYPE> fb_thresholds_e, const std::vector<DTYPE> fb_ikrum_eps);
    virtual DTYPE out() const;
    virtual const std::vector<DTYPE>& fb_out() const;
    virtual DTYPE fb_sum() const;
    virtual DTYPE alpha() const;
    virtual bool is_idle() const;
    void set_idle(bool idle);
    void invert();
    void activate();

protected:
    Preamp* _preamp_;
    std::unique_ptr<PNoiseChannel_BP> _noise_channel_;
    FeedbackParameter _parm_;
    std::unique_ptr<AlphaIkrum> _fb_ikrum_eps_;

    Time *_time_;
    DTYPE _out_{0.0};
    std::vector<DTYPE> _fb_vals_{};
    DTYPE _alpha_{0};

    std::vector<DTYPE> fb_weights{};
    std::unique_ptr<_TaperFunction_> _fb_taper_;
    std::vector<std::unique_ptr<H_Atomic>> _h_feedback_;
};

