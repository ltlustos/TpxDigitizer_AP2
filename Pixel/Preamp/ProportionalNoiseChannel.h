/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#pragma once

#include "Common/dtype.h"
#include "core/utils/prng.h"
#include "Common/Exceptions.h"
#include "Parameter/NoiseChannelParameter.h"
#include "core/utils/distributions.h"

#include "H/H_DeltaExp.h"
#include "Butterworth/Lowpass.h"
#include "Butterworth/Highpass.h"
#include "Butterworth/Bandpass.h"

class  _NoiseChannelBW_
{
public:
    _NoiseChannelBW_(const ProportionalNoiseChannel_Parameter &parm, DTYPE dt)
    {
        initialize(parm, dt);
    }

    virtual ~_NoiseChannelBW_(void)
    {}


    virtual void reset()
    {
        throw SubclassResponsabilityException("_NoiseChannelBW_::reset()");
    }

    virtual DTYPE next(allpix::RandomNumberGenerator&, DTYPE val) = 0;

protected:
    DTYPE _ENC_fraction_{0};  // ENC bandwidth filtered noise
    DTYPE _f_N_{0};
    DTYPE _enc_scale_{0};

    void initialize(const ProportionalNoiseChannel_Parameter &parm, DTYPE dt)
    {
        _f_N_ = 0.5 / dt;
        _ENC_fraction_ = parm.ENC_perc.value;
    }
};


class  PNoiseChannel_HP: public _NoiseChannelBW_
{
public:
    PNoiseChannel_HP(const ProportionalNoiseChannel_Parameter &parm, DTYPE dt)
        : _NoiseChannelBW_(parm, dt)
    {
        initialize(parm, dt);
    }

    virtual ~PNoiseChannel_HP(void)
    {}

    void reset() override
    {
        _highpass_->reset();
    }

    DTYPE next(allpix::RandomNumberGenerator& random_generator, DTYPE value) override
    {
        DTYPE r = 0;
        if(value > 0)
        {
            allpix::normal_distribution<double> _rgauss_(0, value * _enc_scale_);
            r += _highpass_->process_input(_rgauss_(random_generator));
        }

        return r;
    }

private:
    std::unique_ptr<BW_HP1> _highpass_;

    virtual void initialize(const ProportionalNoiseChannel_Parameter &parm, DTYPE dt)
    {
        _NoiseChannelBW_::initialize(parm, dt);
        _enc_scale_ = std::sqrt( 2* (_f_N_ - parm.bw_cutoff.value[0]) / _f_N_) * _ENC_fraction_;
        _highpass_ = std::make_unique<BW_HP1>(1/dt, parm.bw_cutoff.value[0], dt, false);
    }
};

class  PNoiseChannel_BP: public _NoiseChannelBW_
{
public:
    PNoiseChannel_BP(const ProportionalNoiseChannel_Parameter &parm, DTYPE dt)
        : _NoiseChannelBW_(parm, dt)
    {
        initialize(parm, dt);
    }

    virtual ~PNoiseChannel_BP(void)
    {}

    void reset() override
    {
        _bandpass_->reset();
    }

    DTYPE next(allpix::RandomNumberGenerator& random_generator, DTYPE value) override
    {
        DTYPE r = 0;
        if(value > 0)
        {
            allpix::normal_distribution<double> _rgauss_(0, value * _enc_scale_);
            r += _bandpass_->process_input(_rgauss_(random_generator));
        }
        return r;
    }

private:
    std::unique_ptr<BW_BP1> _bandpass_;

    virtual void initialize(const ProportionalNoiseChannel_Parameter &parm, DTYPE dt)
    {
        _NoiseChannelBW_::initialize(parm, dt);
        _enc_scale_ = 1.45 / (std::pow(2*(parm.bw_cutoff.value[1] - parm.bw_cutoff.value[0]) / _f_N_, 0.5)) * _ENC_fraction_;
        _bandpass_ = std::make_unique<BW_BP1>( 1/dt, parm.bw_cutoff.value[1], parm.bw_cutoff.value[0], 0, false);
    }
};
