/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef ALPHAIKRUMPARAMETER_H
#define ALPHAIKRUMPARAMETER_H


#include "Common/dtype.h"
#include "modules/TpxDigitizer/Common/const.h"
#include <vector>
#include <string>
#include "Util/u_string.h"
#include "Util/u_math.h"
#include "core/config/Configuration.hpp"
#include "core/utils/unit.h"
#include "modules/TpxDigitizer/Parameter/_ParameterSet_.h"
#include "modules/TpxDigitizer/Parameter/Parameter.h"
#include "NoiseChannelParameter.h"

class AlphaIkrumParameter: public _Parameter_
{
public:
    PARAMETER::PathParameter config_file{"ikrum_config", false, "ikrum_config.txt"};
    PARAMETER::TArrayParameter<DTYPE> fb_ikrum_eps      {"fb_ikrum", true, {1.5e10}, "e/s", "discharge current [e/s]"};
    PARAMETER::TArrayParameter<DTYPE> fb_thresholds_e   {"fb_ikrum_threshold", false, {0}, "e", "thresholds for discharge current ranges, lowest entry will be set automatically to 0"};

    PARAMETER::TArrayParameter<DTYPE> fb_tau    {"fb_tau", false, {70.0e-9, 720.0e-9, 1.2e-6}, "s"};
    PARAMETER::TArrayParameter<DTYPE> fb_weight {"fb_weight", false, {1.0, 0.6, 1.0}};
    PARAMETER::TParameter<DTYPE>fb_taper_width  {"fb_taper_width", false, 1600.0, "e"};

    PARAMETER::TParameter<DTYPE> fb_ikrum_sigma {"fb_ikrum_sigma", false, 0.0, "e/s"};  // [e/s]
    PARAMETER::TParameter<DTYPE> fb_ikrum_d_T   {"fb_ikrum_d_T", false, 0.3, "e/ns/T"};  // [e/ns/T]

    ProportionalNoiseChannel_Parameter noise_parm{"fb_ikrum_enc", 2.5,
                                                  "fb_noise_bw", {1.0e1, 1.0e7}}; //  bandwitdh is guesswork
    AlphaIkrumParameter() = default;
    ~AlphaIkrumParameter() =default;

    AlphaIkrumParameter& operator = (const AlphaIkrumParameter& o) =default;

    void check_values() const override
    {
        if (fb_ikrum_eps.size() != fb_thresholds_e.size())
            throw ParameterException("fb_ikrum and alpha_ikrum_threshold array length do not match.");

        fb_ikrum_d_T.check_in_range(0.0, 0.5e9);
        fb_ikrum_sigma.check_in_range(0.0, 2.0e9);
        if (fb_thresholds_e.value[0] != 0)
            throwException( "first ikrum threshold must be 0.");

        fb_ikrum_eps.check_in_range_all(1.e9, 10000.0e9);

        for (auto e : fb_ikrum_eps.value)
            if(fb_ikrum_sigma.value > e / 8)
                throwException( "fb_ikrum_sigma is too large, should be > min(fb_ikrum_sigma)/8");


        DTYPE val = -1;
        for (auto v : fb_thresholds_e.value)
        {
            if (v < 0)
                throwException( string_sprintf("fb_ikrum_sigma threshold value must be >=0, found %g", v));
            if (v <= val)
                throwException( string_sprintf("fb_ikrum_sigma thresholds must be increasing, %g <= %g.", v, val));
            val = v;
        }
        noise_parm.ENC_perc.check_in_range(0.0, 0.1e9);
    }

    void check_values(__attribute__((unused)) DTYPE dt_sec) const override
    {}

    void adjust_values() override
    {}

    void adjust_timestep_values(DTYPE dt_sec) override
    {
        noise_parm.adjust_timestep_values(dt_sec);
    }

    std::size_t size() const
    {
        return fb_ikrum_eps.size();
    }

    std::vector<DTYPE> alpha_eff(DTYPE T, DTYPE offset) const
    {
        std::vector<DTYPE> alpha;
        std::transform(fb_ikrum_eps.value.begin(), fb_ikrum_eps.value.end(), std::back_inserter(alpha),
                       [this, T, offset](const DTYPE& val) { return val + fb_ikrum_d_T.value *(T - T_ref) + offset; });
        return alpha;
    }


    void parse_config(const allpix::Configuration &config) override
    {
        config_file.from_parameter_file(config);
        fb_ikrum_eps.from_parameter_file(config);
        fb_thresholds_e.from_parameter_file(config);
        fb_tau.from_parameter_file(config);
        fb_weight.from_parameter_file(config);
        fb_taper_width.from_parameter_file(config);
        fb_ikrum_sigma.from_parameter_file(config);
        fb_ikrum_d_T.from_parameter_file(config);
        noise_parm.from_parameter_file(config);
    }


};
#endif // ALPHAIKRUMPARAMETER_H
