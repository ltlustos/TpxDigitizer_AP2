#pragma once
#include "modules/TpxDigitizer/Parameter/_ParameterSet_.h"
#include "modules/TpxDigitizer/Parameter/Parameter.h"

class  _NoiseChannel_Parameter_: public _Parameter_
{
public:
    PARAMETER::TArrayParameter<DTYPE> bw_cutoff{"bw_cutoff", false, {1.0e4, 1.0e5}, "Hz", "noise bandwidth limits", 2};

    _NoiseChannel_Parameter_(std::string key_bandwidth, std::vector<DTYPE> bw_low)
    {
        if (bw_low.size() != 2)
            throw ParameterException("bandwidth");
        bw_cutoff.set_key(key_bandwidth);
        bw_cutoff.value = bw_low;

    }


    void adjust_values() override
    {}

    void check_values() const override
    {
        _check_in_range_(bw_cutoff.value[0], 1e1, 1e6 , "noise bw_low", "Hz");
        _check_in_range_(bw_cutoff.value[1], 1e4, 1e10, "noise bw_high", "Hz");
    }
    void adjust_timestep_values(DTYPE dt_s) override
    {
        if (bw_cutoff.value[1] > 0.5 / dt_s)
        {
            LOG(WARNING) << "NoiseChannel_Parameter: invalid noise bw_high: " << bw_cutoff.value[1] << " -> set to Nyquist frequency.";
            bw_cutoff.value[1] = 0.5 / dt_s;
        }
    }

    bool from_parameter_file(const allpix::Configuration &config, bool check_used=true)
    {
        return bw_cutoff.from_parameter_file(config, check_used);
    }
};

class  NoiseChannel_Parameter: public _NoiseChannel_Parameter_
{
public:

    PARAMETER::TParameter<DTYPE> ENC_bw{"ENC", false, 0.0, "e", "band width limited noise ENC"};
    PARAMETER::TParameter<DTYPE> ENC_white{"ENC_white", false, 0.0, "e", "white noise ENC"};
    PARAMETER::TArrayParameter<DTYPE> bw_cutoff{"bw_cutoff", false, {1.0e4, 1.0e5}, "Hz", "noise bandwidth limits", 2};

    NoiseChannel_Parameter(std::string key_enc, DTYPE enc,
                           std::string key_enc_white, DTYPE enc_white,
                           std::string key_bandwidth, std::vector<DTYPE> bw_low)
        :_NoiseChannel_Parameter_(key_bandwidth, bw_low)
    {
        // defaults
        ENC_bw.set_key(key_enc);
        ENC_bw.value = enc;

        ENC_white.set_key(key_enc_white);
        ENC_white.value = enc_white;
    }


    void check_values() const override
    {

        ENC_bw.check_in_range(0.0, 500.0);
        ENC_white.check_in_range(0.0, 50.0);
        _NoiseChannel_Parameter_::check_values();
    }

    bool from_parameter_file(const allpix::Configuration &config, bool check_used=true)
    {
        return ENC_bw.from_parameter_file(config, check_used) &
                ENC_white.from_parameter_file(config, check_used) &
                _NoiseChannel_Parameter_::from_parameter_file(config, check_used);
    }
};

class  ProportionalNoiseChannel_Parameter: public _NoiseChannel_Parameter_
{
public:

    PARAMETER::TParameter<DTYPE> ENC_perc{"ENC_perc", false, 0.0, "e", "band width limited noise ENC"};
    PARAMETER::TArrayParameter<DTYPE> bw_cutoff{"bw_cutoff", false, {1.0e4, 1.0e5}, "Hz", "noise bandwidth limits", 2};

    ProportionalNoiseChannel_Parameter(std::string key_enc_perc, DTYPE enc_perc,
                                       std::string key_bandwidth, std::vector<DTYPE> bw_low)
        :_NoiseChannel_Parameter_(key_bandwidth, bw_low)
    {
        // defaults
        ENC_perc.set_key(key_enc_perc);
        ENC_perc.value = enc_perc;
    }


    void check_values() const override
    {

        ENC_perc.check_in_range(0.0, 10.0);
        _NoiseChannel_Parameter_::check_values();
    }

    bool from_parameter_file(const allpix::Configuration &config, bool check_used=true)
    {
        return ENC_perc.from_parameter_file(config, check_used) &
                _NoiseChannel_Parameter_::from_parameter_file(config, check_used);
    }
};
