#ifndef PREAMPPARAMETER_H
#define PREAMPPARAMETER_H

#include "modules/TpxDigitizer/Parameter/_ParameterSet_.h"
#include "Util/u_math.h"
#include "modules/TpxDigitizer/Common/const.h"
#include "modules/TpxDigitizer/Parameter/Parameter.h"
#include "modules/TpxDigitizer/Pixel/Preamp/Parameter/CSAParameter.h"
#include "FeedbackParameter.h"
#include "LccParameter.h"

class PreampParameter: public _Parameter_
{
public:
    CSAParameter csa_parm;
    FeedbackParameter feedback_parm;
    LccParameter lcc_parm;

    NoiseChannel_Parameter noise_parm{"noise_ENC", 80.0,
                                      "noise_ENC_white", 0.0,
                                      "noise_bw", {1.0e4, 1.1e7}};

    PreampParameter() = default;
    ~PreampParameter() =default;
    PreampParameter& operator = (const PreampParameter& o) = default;

    void check_values() const override
    {
        csa_parm.check_values();
        feedback_parm.check_values();
        lcc_parm.check_values();
        noise_parm.check_values();
    }

    void check_values(DTYPE dt_sec) const override
    {
        lcc_parm.check_values(dt_sec);
    }

    void adjust_values() override
    {
        feedback_parm.adjust_values();
        if (noise_parm.bw_cutoff.value[1] < 0)
            noise_parm.bw_cutoff.value[1]  = 1. / (2 * M_PI * csa_parm.tau_integrator_s.value);
        _check_in_range_(noise_parm.bw_cutoff.value[1], 1.0e4, 1.0e10, "preamp: bw_high", "Hz");
        noise_parm.adjust_values();
        lcc_parm.adjust_values();
    }

    void adjust_timestep_values(DTYPE dt_sec) override
    {
        csa_parm.adjust_timestep_values(dt_sec);
        feedback_parm.adjust_timestep_values(dt_sec);
        noise_parm.adjust_timestep_values(dt_sec);
        lcc_parm.adjust_timestep_values(dt_sec);
    }


    void parse_config(const allpix::Configuration &config) override
    {

        csa_parm.parse_config(config);
        feedback_parm.parse_config(config);

        noise_parm.from_parameter_file(config);
        if (noise_parm.bw_cutoff.value[1] < 0)
            noise_parm.bw_cutoff.value[1]  = 1. / (2 * csa_parm.tau_integrator_s.value);
        lcc_parm.parse_config(config);
    }
};

#endif // PREAMPPARAMETER_H
