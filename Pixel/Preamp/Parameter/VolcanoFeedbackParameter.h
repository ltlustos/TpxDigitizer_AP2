/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef VOLCANOFEEDBACKPARAMETER_H
#define VOLCANOFEEDBACKPARAMETER_H

#include "Common/dtype.h"
#include "modules/TpxDigitizer/Parameter/_ParameterSet_.h"
#include "modules/TpxDigitizer/Parameter/Parameter.h"

class PreampVolcanoParameter: public _Parameter_
{
public:
    PreampVolcanoParameter() {};
    ~PreampVolcanoParameter() = default;
    PreampVolcanoParameter& operator = (const PreampVolcanoParameter& o) = default;

    void check_values() const override
    {
        // FIXIT: define limits
    }


    void parse_config(const allpix::Configuration &config) override
    {
        limit_amplitude.from_parameter_file(config);
        undershoot.from_parameter_file(config);
        rebound_injection.from_parameter_file(config);
        inversion_delay.from_parameter_file(config);
    }


    PARAMETER::TParameter<DTYPE>limit_amplitude{"volcano_limit_amplitude", false, 600e3, "e"};
    PARAMETER::TParameter<DTYPE>undershoot{"volcano_undershoot", false, -3.0e3, "e"};
    PARAMETER::TParameter<DTYPE>rebound_injection{"#volcano_rebound_injection", false, 0.0, "e"};  // optional extra charge injection when reversing to nominal polarity
    PARAMETER::TParameter<DTYPE>inversion_delay{"volcano_inversion_delay", false, 150e-9, "s"};
protected:

};


#endif // VOLCANOFEEDBACKPARAMETER_H
