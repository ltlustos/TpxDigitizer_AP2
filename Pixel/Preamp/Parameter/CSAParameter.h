#pragma once
#ifndef CSAParameter_H
#define CSAParameter_H

#include "modules/TpxDigitizer/Parameter/_ParameterSet_.h"
#include "modules/TpxDigitizer/Parameter/Parameter.h"

class CSAParameter: public _Parameter_
{
public:
    PARAMETER::TParameter<DTYPE> tau_integrator_s {"tau_integrator", false, 4.0e-9, "s", "integrator time constant"};  // [s]
    PARAMETER::PathParameter gain_config_file {"gain_config", false, ""};
    PARAMETER::TParameter<DTYPE> gain {"gain", false, 1.0, "", "preamp gain"};
    PARAMETER::TParameter<DTYPE> gain_asymmetry {"gain_asymmetry", false, 2.5, "", "preamp gain asymmetry"};
    PARAMETER::TParameter<DTYPE> gain_dispersion{"gain_dispersion", false, 0.0, "", "interpixel gain dispersion"};

    CSAParameter() = default;
    ~CSAParameter() =default;
    CSAParameter& operator = (const CSAParameter& o) = default;

    void check_values() const override
    {}

    void check_values(DTYPE dt_sec) const override
    {
        tau_integrator_s.check_in_range( dt_sec, 200.0e-9);
        gain.check_in_range( 0.005, 10);
        gain_asymmetry.check_in_range( 0., 4.);
        gain_dispersion.check_in_range( 0., 4.);
    }

    void adjust_values() override
    {}

    void adjust_timestep_values(DTYPE dt_sec) override
    {}

    void parse_config(const allpix::Configuration &config) override
    {

        tau_integrator_s.from_parameter_file(config);
        gain_config_file.from_parameter_file(config);
        gain.from_parameter_file(config);
        gain_asymmetry.from_parameter_file(config);
        gain_dispersion.from_parameter_file(config);
    }
};

#endif // CSAParameter_H
