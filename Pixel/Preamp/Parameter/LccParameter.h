#ifndef LCCPARAMETER_H
#define LCCPARAMETER_H


#include <vector>
#include <string>
#include "modules/TpxDigitizer/Common/dtype.h"
#include "modules/TpxDigitizer/Common/const.h"
#include "modules/TpxDigitizer/Util/u_string.h"
#include "modules/TpxDigitizer/Util/u_math.h"
#include "core/config/Configuration.hpp"
#include "core/utils/unit.h"
#include "modules/TpxDigitizer/Parameter/_ParameterSet_.h"
#include "modules/TpxDigitizer/Parameter/Parameter.h"
#include "NoiseChannelParameter.h"

class LccParameter: public _Parameter_
{
public:
    PARAMETER::TParameter<DTYPE> tau    {"lcc_tau", false, 10e-6, "s"};
    PARAMETER::TParameter<DTYPE> weight {"lcc_weight", false, 0.04};

    LccParameter() = default;
    ~LccParameter() =default;

    LccParameter& operator = (const LccParameter& o) =default;

    void check_values() const override
    {
        tau.check_in_range(1e-6, 100e-6);
        weight.check_in_range(0.0, 1.0);
    }

    void check_values(__attribute__((unused)) DTYPE dt_sec) const override
    {}

    void adjust_values() override
    {}

    void adjust_timestep_values(DTYPE) override
    {}


    void parse_config(const allpix::Configuration &config) override
    {
        tau.from_parameter_file(config);
        weight.from_parameter_file(config);
    }
};

#endif // LCCPARAMETER_H
