#ifndef FEEDBACKPARAMETER_H
#define FEEDBACKPARAMETER_H
#include "modules/TpxDigitizer/Parameter/_ParameterSet_.h"
#include "NoiseChannelParameter.h"
#include "AlphaIkrumParameter.h"
#include "VolcanoFeedbackParameter.h"
#include <magic_enum/magic_enum.hpp>
#include "modules/TpxDigitizer/Parameter/Parameter.h"

class FeedbackParameter:public _Parameter_
{
public:
    enum class FeedbackType: int8_t {
        DEFAULT=0, //  "ideal" preamp
        VOLCANO=1, //  volcano effect, preamp inverson and modified ToT
    };

    PARAMETER::TEnumParameter<FeedbackType> feedback_type{"feedback_type", false, FeedbackType::DEFAULT};
    AlphaIkrumParameter alpha_ikrum_parm;
    PreampVolcanoParameter volcano_parm;

    FeedbackParameter() = default;
    ~FeedbackParameter() = default;
    FeedbackParameter& operator = (const FeedbackParameter& o) = default;


    void check_values() const override
    {
        alpha_ikrum_parm.check_values();
        volcano_parm.check_values();
    }


    void adjust_values() override
    {
        alpha_ikrum_parm.adjust_values();
        volcano_parm.adjust_values();
    }

    void adjust_timestep_values(DTYPE dt_sec) override
    {
        alpha_ikrum_parm.adjust_timestep_values(dt_sec);
        volcano_parm.adjust_timestep_values(dt_sec);
    }


    void check_values(DTYPE dt_sec) const override
    {
        alpha_ikrum_parm.check_values(dt_sec);
    }

    void parse_config(const allpix::Configuration &config) override
    {
        feedback_type.from_parameter_file(config);
        alpha_ikrum_parm.parse_config(config);
        volcano_parm.parse_config(config);
    }
};

#endif // FEEDBACKPARAMETER_H
