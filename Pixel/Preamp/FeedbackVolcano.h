#pragma once


#include "FeedbackIkrum.h"

class FeedbackVolvcano: public FeedbackIkrum
{
protected:
    FeedbackVolvcano() = default;
public:
    FeedbackVolvcano(const FeedbackParameter &parm, Time &time, Preamp& preamp, DTYPE h_cutoff);
    virtual ~FeedbackVolvcano() = default;
    FeedbackVolvcano(const FeedbackVolvcano &other) = delete;
    FeedbackVolvcano& operator =(const FeedbackVolvcano& other) = delete;
    virtual DTYPE process_input(allpix::RandomNumberGenerator& random_generator) override;
private:
    DTYPE calc_alpha_ikrum();
    DTYPE _limit_amplitude_;        // [e]
    DTYPE _undershoot_;             // [e]
    DTYPE _rebound_injection_;      // [e]
    DTYPE _inversion_delay_;        // [e]
    bool _preamp_inverted_{false};
    DTYPE _inversion_time_{0};
};

