#include "Lcc.h"


Lcc::Lcc(const PreampParameter &preamp_parm,
         Time& time,
         DTYPE h_cutoff)
{
    _time_ = &time;
    _weight_ = preamp_parm.lcc_parm.weight.value;
    _h_int_input_ = std::make_unique<H_Integrator>(preamp_parm.csa_parm.tau_integrator_s.value,
                                                   _time_->dt(),
                                                   h_cutoff * _weight_,
                                                   true);
    _h_int_input_->normalize_integral_h();
    _lcc_max_ = preamp_parm.feedback_parm.alpha_ikrum_parm.fb_ikrum_eps.value[0] * _time_->dt();
    _frac_ = _time_->dt() / preamp_parm.lcc_parm.tau.value;
    LOG(DEBUG) << "preamp_parm.lcc_parm.tau.value : " << preamp_parm.lcc_parm.tau.value;
    LOG(DEBUG) << "preamp_parm.lcc_parm.weight.value: " <<preamp_parm.lcc_parm.weight.value;
}


DTYPE  Lcc::process_input(DTYPE fb_csa)
{
    _out_ =  _h_int_input_->process_input(_weight_ * fb_csa -  _fb_);
    _fb_ =  std::min(_lcc_max_ , std::abs(_out_ * _frac_)) * sign(_out_);
    return _out_;
}

void  Lcc::reset()
{
    _h_int_input_->reset();
    _out_ = _fb_ = 0;
}

DTYPE  Lcc::out() const
{
    return _out_;
}
bool  Lcc::is_idle() const
{
    return _h_int_input_->is_idle();
}
void  Lcc::set_idle(bool idle)
{
    _h_int_input_->set_idle(idle);
}
void  Lcc::invert()
{
    _h_int_input_->invert();
}
void  Lcc::activate()
{
    _h_int_input_->set_active(true);
}
