#include "FeedbackVolcano.h"


FeedbackVolvcano::FeedbackVolvcano(const FeedbackParameter &parm, Time &time, Preamp& preamp, DTYPE h_cutoff)
    : FeedbackIkrum(parm, time, preamp, h_cutoff)
{
    _preamp_inverted_ = false;
    _limit_amplitude_ = parm.volcano_parm.limit_amplitude.value;
    _undershoot_ = parm.volcano_parm.undershoot.value;
    _rebound_injection_ = parm.volcano_parm.rebound_injection.value;
    _inversion_delay_ = parm.volcano_parm.inversion_delay.value;
}


DTYPE FeedbackVolvcano::calc_alpha_ikrum()
{
    if (_preamp_inverted_)
        // -> Rafa:  25 * alpha_ikrum_base [e/s] * preamp_q_load [e] * 1.602e-19 / 700e-15 [F]
        return 5.721428571428571e-06 * _fb_ikrum_eps_->alpha_base() * _preamp_->_charge_load_ * _time_->dt();
    else
        return _fb_ikrum_eps_->alpha(_preamp_->_output_internal_) * _time_->dt();

}
DTYPE FeedbackVolvcano::process_input(allpix::RandomNumberGenerator& random_generator)
{
    _out_ = FeedbackIkrum::process_input(random_generator);

    // _preamp_->_out holds internal value
    // _preamp_->_output_ holds the visible output

    if (_preamp_->_output_internal_ > _limit_amplitude_ && !_preamp_inverted_)
    {
        _preamp_->invert();  // invert transfer function
        _preamp_inverted_ = true;
        DTYPE t = _time_->now();
        _inversion_time_ = t;
    }    else if (_preamp_inverted_) {
        if (_time_->now() - _inversion_time_ < _inversion_delay_)
        {
            _preamp_->_out_ = _limit_amplitude_;
        } else if (_preamp_->_output_internal_  > _undershoot_) {
            _preamp_->invert();  // switch back to normal, invert transfer function only, fb continues without inversion
            _preamp_inverted_ = false;
            reset();
            _preamp_->csa->input_add(_rebound_injection_);
            _preamp_->csa->process(random_generator);
            _preamp_->activate();
        }
        else
            _preamp_->_out_ = _undershoot_;
    } else
        _preamp_->_out_ = _preamp_->_output_internal_ ;
    return _out_;
}

