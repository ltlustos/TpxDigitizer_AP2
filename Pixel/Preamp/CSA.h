#pragma once

#include <stddef.h>
#include "Common/Time.h"
#include "H/_H_IO_Node_.h"
#include "H/H_Integrator.h"
#include "core/utils/prng.h"
#include "PreampGain.h"
#include "H/H_Integrator.h"

class  CSA: public H_IO_Node_DTYPE
{
public:
    CSA(const CSAParameter &parm, Time& time, DTYPE h_cutoff);
    virtual ~CSA() = default;
    virtual void set_gain(DTYPE gain, DTYPE gain_asymmetry);
    virtual void input_add(DTYPE val);
    void invert();
    virtual void reset();
    DTYPE process(allpix::RandomNumberGenerator& random_generator);

    void activate();
    bool is_idle() const;
    bool is_active() const;
    void set_idle(bool idle);
    DTYPE dt() const;
    DTYPE dt_ns() const;
    DTYPE now() const;
    DTYPE out() const;
    DTYPE total_charge() const;

protected:
    CSA() = default;
    std::unique_ptr<PreampGain> _gain_;
    std::unique_ptr<H_Integrator> _h_int_input_;
    DTYPE _charge_load_{0};
    Time *_time;
    DTYPE _total_charge_{0};
    bool _inverted_{false};
};

