#include "Preamp.h"
#include "FeedbackVolcano.h"


Preamp::Preamp(const PreampParameter &preamp_parm, Time& time, DTYPE h_cutoff)
{
    _time = &time;
    csa = std::make_unique<CSA>(preamp_parm.csa_parm, time, h_cutoff);

    if (preamp_parm.feedback_parm.feedback_type.value == FeedbackParameter::FeedbackType::DEFAULT)
        feedback = std::make_unique<FeedbackIkrum>(preamp_parm.feedback_parm, time, *this, h_cutoff);
    else if (preamp_parm.feedback_parm.feedback_type.value == FeedbackParameter::FeedbackType::VOLCANO)
        feedback = std::make_unique<FeedbackVolvcano>(preamp_parm.feedback_parm, time, *this, h_cutoff);
    else
        throw ParameterException(string_sprintf("Unknown feedback type '%s'", preamp_parm.feedback_parm.feedback_type).c_str());

    lcc = std::make_unique<Lcc>(preamp_parm, time, h_cutoff);

    _noise_out_ = 0.0;
    _noise_channel_ = std::make_unique<NoiseChannel_BP>(preamp_parm.noise_parm, _time->dt());

}

void Preamp::set_alpha_ikrum(const std::vector<DTYPE> fb_thresholds_e, const std::vector<DTYPE> fb_ikrum_eps)
{
    feedback->set_alpha_ikrum( fb_thresholds_e, fb_ikrum_eps);
}

void Preamp::set_gain(DTYPE gain, DTYPE gain_asymmetry)
{
    csa->set_gain(gain, gain_asymmetry);
}

void Preamp::input_add(DTYPE val)
{
    if (val != 0.0)
    {
        activate();
        csa->input_add(val);
        _total_charge_ += val;
    }
}

void Preamp::reset()
{
    csa->reset();
    feedback->reset();
    lcc->reset();
    _noise_channel_->reset();
    _total_charge_ = 0.0;
    _inverted_ = false;
    _charge_load_ = 0;
    _output_internal_ = 0;
    _noise_out_ = 0;
}

void Preamp::invert()
{
    csa->invert();
    feedback->invert();
    lcc->invert();
    _inverted_ = !_inverted_;
}

DTYPE Preamp::process(allpix::RandomNumberGenerator& random_generator)
{
    if (is_idle()) {
        _noise_out_ = 0.0;
    } else {
        csa->input_add(-feedback->out());
        _charge_load_ += csa->in();
        _output_internal_ = csa->process(random_generator);
        _out_ = _output_internal_  - lcc->out();
        _noise_out_ = _noise_channel_->next(random_generator);
        feedback->process_input(random_generator);
        lcc->process_input(feedback->out());
    }
    return out();
}

void Preamp::activate()
{
    set_idle(false);
}


bool Preamp::is_idle() const
{
    return csa->is_idle() || feedback->is_idle();
}

bool Preamp::is_active() const
{
    return !is_idle();
}

void Preamp::set_idle(bool idle)
{
    csa->set_idle(idle);
    feedback->set_idle(idle);
}

DTYPE Preamp::ENC() const
{
    return _noise_channel_->ENC_BW();
}

DTYPE Preamp::ONC() const
{
    return _noise_channel_->ENC_White();
}

DTYPE Preamp::dt() const
{
    return _time->dt();
}

DTYPE Preamp::now() const
{
    return _time->now();
}

DTYPE Preamp::dt_ns() const
{
    return _time->dt_ns();
}

DTYPE Preamp::out() const
{
    return _out_ + _noise_out_;
}

DTYPE Preamp::noise_out() const
{
    return _noise_out_;
}

DTYPE Preamp::total_charge() const
{
    return _total_charge_;
}



