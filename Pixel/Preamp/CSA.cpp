#include "CSA.h"

CSA::CSA(const CSAParameter &parm, Time& time, DTYPE h_cutoff)
{
    _time = &time;

    _gain_ = std::make_unique<PreampGain>(parm);
    _h_int_input_ = std::make_unique<H_Integrator>(parm.tau_integrator_s.value,
                                                   _time->dt(),
                                                   h_cutoff,
                                                   true);
    _h_int_input_->normalize_integral_h();
}

void CSA::set_gain(DTYPE gain, DTYPE gain_asymmetry)
{
    _gain_->set(gain, gain_asymmetry);
}

void CSA::input_add(DTYPE val)
{
    if (val != 0.0)
    {
        activate();
        H_IO_Node_DTYPE::input_add(val);
        _total_charge_ += val;
    }
}

void CSA::reset()
{
    H_IO_Node_DTYPE::reset();
    _total_charge_ = 0.0;
    _h_int_input_->reset();
    _inverted_ = false;
    _charge_load_ = 0;
}

void CSA::invert()
{
    _h_int_input_->invert();
    _inverted_ = !_inverted_;
}

DTYPE CSA::process(allpix::RandomNumberGenerator& random_generator)
{
    if (!is_idle())
    {
        _charge_load_ += _in;
        _out = _h_int_input_->process_input(_gain_->processs(_in));
    }
    _in = 0;
    return out();
}

void CSA::activate()
{
    set_idle(false);
}


bool CSA::is_idle() const
{
    return _h_int_input_->is_idle();
}

bool CSA::is_active() const
{
    return !is_idle();
}

void CSA::set_idle(bool idle)
{
    _h_int_input_->set_idle(idle);

}

DTYPE CSA::dt() const
{
    return _time->dt();
}

DTYPE CSA::now() const
{
    return _time->now();
}

DTYPE CSA::dt_ns() const
{
    return _time->dt_ns();
}

DTYPE CSA::out() const
{
    return _out ;
}

DTYPE CSA::total_charge() const
{
    return _total_charge_;
}



