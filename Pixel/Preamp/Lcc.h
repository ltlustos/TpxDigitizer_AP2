#ifndef LCC_H
#define LCC_H
#include "Parameter/LccParameter.h"
#include "Parameter/FeedbackParameter.h"
#include "Parameter/PreampParameter.h"
#include "Common/Time.h"
#include "H/H_Integrator.h"

class Preamp;
class Lcc
{
protected:
    Lcc() = default;
public:
    Lcc(const PreampParameter &preamp_parm, Time& time, DTYPE h_cutoff);
    ~Lcc() = default;
    Lcc(const Lcc &other) = delete;
    Lcc& operator =(const Lcc& other) = delete;
    virtual DTYPE process_input(DTYPE fb_csa);
    virtual void reset();
    virtual DTYPE out() const;
    virtual bool is_idle() const;
    void set_idle(bool idle);
    void invert();
    void activate();

protected:
    std::unique_ptr<H_Integrator> _h_int_input_;
    Preamp* _preamp_;
    FeedbackParameter _fb_parm_;
    LccParameter _lcc_parm_;

    Time *_time_;
    DTYPE _out_{0.0};
    DTYPE _weight_;
    DTYPE _lcc_max_;
    DTYPE _frac_{0.0};
    DTYPE _fb_{0.0};
};

#endif // LCC_H
