#include "FeedbackIkrum.h"
#include "Parameter/FeedbackParameter.h"
#include "Taper/Tanh.h"
#include "Taper/Taper.h"
#include "AlphaIkrum.h"
#include "Util/u_math.h"
#include "NoiseChannel.h"
#include "H/H_NOP.h"

FeedbackIkrum::FeedbackIkrum(const FeedbackParameter &parm,  Time &time, Preamp &preamp, DTYPE h_cutoff)
{
    _preamp_ = &preamp;
    _time_ = &time;
    _parm_ = parm;
    DTYPE norm = 0;
    fb_weights = _parm_.alpha_ikrum_parm.fb_weight.value;
    for (const auto& w: fb_weights)
        norm += w;
    for (auto& w: fb_weights)
        w /= norm;

    _fb_ikrum_eps_ = std::make_unique<AlphaIkrum>(
        _parm_.alpha_ikrum_parm.fb_thresholds_e.value,
        _parm_.alpha_ikrum_parm.fb_ikrum_eps.value);

    _alpha_ = _out_ = 0.0;

    _noise_channel_ = std::make_unique<PNoiseChannel_BP>(parm.alpha_ikrum_parm.noise_parm, time.dt());


    for (auto tau : parm.alpha_ikrum_parm.fb_tau.value)
    {
        DTYPE fc = 1.0 / (2.0 * M_PI * tau);
        _h_feedback_.emplace_back(std::make_unique<BW_LP1>(1/time.dt(), fc, 1e-3*h_cutoff, true));
        _fb_vals_.emplace_back(0.0);
    }
    _fb_taper_ = std::make_unique<Tanh_Width>(parm.alpha_ikrum_parm.fb_taper_width.value);
}

DTYPE FeedbackIkrum::process_input(allpix::RandomNumberGenerator& random_generator)
{
    _alpha_ = _fb_ikrum_eps_->alpha(_preamp_->_output_internal_);
    _alpha_ *= _time_->dt();
    DTYPE fb_in = _preamp_->_output_internal_;
    _fb_vals_[0] =  fb_weights[0] * _h_feedback_[0]->process_input(_alpha_ * _fb_taper_->calc(fb_in));
    _out_ = _fb_vals_[0] ;
    for (size_t i=1; i<_h_feedback_.size(); ++i)
    {
        if (fb_weights[i] > 0 )
        {
            _fb_vals_[i] = fb_weights[i] * _h_feedback_[i]->process_input(_alpha_ * _fb_taper_->calc(fb_in));
            _out_ += _fb_vals_[i];
        }
    }
    _out_ = std::min(std::abs(_out_),  std::abs(_alpha_)) * sign(_out_);
    _out_ += _noise_channel_->next(random_generator, _out_);
    return _out_;
}

bool FeedbackIkrum::is_idle() const
{
    bool r = true;
    for (auto& fb : _h_feedback_)
        r = r && fb->is_idle();
    return r;
}

void FeedbackIkrum::set_idle(bool idle)
{
    for (size_t i=0; i<_h_feedback_.size(); ++i)
        _h_feedback_[i]->set_idle(idle || (fb_weights[i]==0));
}

void FeedbackIkrum::activate()
{
    set_idle(false);
}

void FeedbackIkrum::reset()
{
    for (size_t i=0; i<_h_feedback_.size();++i)
    {
        _h_feedback_[i]->reset();
        _fb_vals_[i] = 0.0;
    }
    _noise_channel_->reset();
    _alpha_ = _out_ = 0.0;
}

void FeedbackIkrum::invert()
{
    for (auto &fb: _h_feedback_)
    {
        fb->invert();
    }
}

void FeedbackIkrum::set_alpha_ikrum(const std::vector<DTYPE> fb_thresholds_e, const std::vector<DTYPE> fb_ikrum_eps)
{
    _fb_ikrum_eps_->set( fb_thresholds_e, fb_ikrum_eps);
}

DTYPE FeedbackIkrum::out() const
{
    return _out_;
}

const std::vector<DTYPE>& FeedbackIkrum::fb_out() const
{
    return _fb_vals_;
}

DTYPE FeedbackIkrum::fb_sum() const
{
    return  std::reduce(_fb_vals_.begin(), _fb_vals_.end());
}

DTYPE FeedbackIkrum::alpha() const
{
    return _alpha_;
}
