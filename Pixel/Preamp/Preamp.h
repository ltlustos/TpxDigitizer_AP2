#pragma once

#include <stddef.h>
#include "Common/Time.h"
#include "NoiseChannel.h"
#include "core/utils/prng.h"
#include "CSA.h"
#include "FeedbackIkrum.h"
#include "Lcc.h"


class FeedbackIkrum;
class FeedbackVolvcano;
class TpxFeedbackSaturation;

class  Preamp
{
public:
    friend class FeedbackIkrum;
    friend class FeedbackSaturation;
    friend class FeedbackVolvcano;
public:
    std::unique_ptr<CSA> csa;
    std::unique_ptr<FeedbackIkrum> feedback;
    std::unique_ptr<Lcc> lcc;
    Preamp(const PreampParameter &preamp_parm, Time& time, DTYPE h_cutoff);

    virtual ~Preamp() = default;
    virtual void set_alpha_ikrum(const std::vector<DTYPE> fb_thresholds_e, const std::vector<DTYPE> fb_ikrum_eps);
    virtual void set_gain(DTYPE gain, DTYPE gain_asymmetry);
    virtual void input_add(DTYPE val);
    void invert();
    virtual void reset();


    DTYPE process(allpix::RandomNumberGenerator& random_generator);

    void activate();
    bool is_idle() const;
    bool is_active() const;
    void set_idle(bool idle);
    DTYPE ENC() const;
    DTYPE ONC() const;
    DTYPE dt() const;
    DTYPE now() const;
    virtual DTYPE noise_out() const;

    DTYPE dt_ns() const;
    DTYPE out() const;
    DTYPE total_charge() const;

protected:
    Preamp() = default;
    std::unique_ptr<NoiseChannel_BP> _noise_channel_;
    DTYPE _noise_out_;
    DTYPE _charge_load_{0};
    DTYPE _output_internal_{0};
    DTYPE _out_{0};
    Time *_time;
    DTYPE _total_charge_{0};
    bool _inverted_{false};

};

