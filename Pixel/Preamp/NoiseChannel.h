/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#pragma once

#include "Common/dtype.h"
#include "core/utils/prng.h"
#include "Common/Exceptions.h"
#include "Parameter/NoiseChannelParameter.h"
#include "core/utils/distributions.h"

#include "H/H_DeltaExp.h"
#include "Butterworth/Lowpass.h"
#include "Butterworth/Highpass.h"
#include "Butterworth/Bandpass.h"

class  _NoiseChannel_
{
public:
    _NoiseChannel_(const NoiseChannel_Parameter &parm, DTYPE dt)
    {
        initialize(parm, dt);
    }

    virtual ~_NoiseChannel_(void)
    {}

    DTYPE ENC_White() const
    {
        return _ENC_white_;
    }

    DTYPE ENC_BW() const
    {
        return _ENC_bw_;
    }

    virtual void reset()
    {
        throw SubclassResponsabilityException("_NoiseChannel_::reset()");
    }

    virtual DTYPE scale_ENC(__attribute__((unused)) const NoiseChannel_Parameter &parm) const
    {
        throw SubclassResponsabilityException("_NoiseChannel_:scale_ENC");
    }

    virtual DTYPE next(allpix::RandomNumberGenerator&)
    {
        throw SubclassResponsabilityException("_NoiseChannel_:next");
    }

protected:
    DTYPE _ENC_bw_{0};  // ENC bandwidth filtered noise
    DTYPE _ENC_white_{0};  // ENC white noise
    DTYPE _f_N_{0};

    void initialize(const NoiseChannel_Parameter &parm, DTYPE dt)
    {
        _f_N_ = 0.5 / dt;
        _ENC_bw_ = parm.ENC_bw.value;
        _ENC_white_ = parm.ENC_white.value;
    }
};


class  NoiseChannel_HP: public _NoiseChannel_
{
public:
    NoiseChannel_HP(const NoiseChannel_Parameter &parm, DTYPE dt)
        : _NoiseChannel_(parm, dt)
    {
        initialize(parm, dt);
    }

    virtual ~NoiseChannel_HP(void)
    {}

    void reset() override
    {
        _highpass_->reset();
    }

    DTYPE next(allpix::RandomNumberGenerator& random_generator) override
    {
        DTYPE val = 0;
        if(_ENC_bw_ > 0)
        {
            allpix::normal_distribution<double> _rgauss_(0, _ENC_bw_);
            val += _highpass_->process_input(_rgauss_(random_generator));
        }

        if(_ENC_white_ > 0)
        {
            allpix::uniform_real_distribution<double> _rgauss_white_(-0.5*_ENC_white_, 0.5*_ENC_white_);
            val += _rgauss_white_(random_generator);
        }

        return val;
    }

    DTYPE scale_ENC(const NoiseChannel_Parameter &parm) const override
    {
        return parm.ENC_bw.value * std::sqrt( 2* (_f_N_ - parm.bw_cutoff.value[0]) / _f_N_) ;
    }

private:
    std::unique_ptr<BW_HP1> _highpass_;

    virtual void initialize(const NoiseChannel_Parameter &parm, DTYPE dt)
    {
        _NoiseChannel_::initialize(parm, dt);
        _ENC_bw_ = scale_ENC(parm);
        _highpass_ = std::make_unique<BW_HP1>(1/dt, parm.bw_cutoff.value[0], dt, false);
    }
};

class  NoiseChannel_BP: public _NoiseChannel_
{
public:
    NoiseChannel_BP(const NoiseChannel_Parameter &parm, DTYPE dt)
        : _NoiseChannel_(parm, dt)
    {
        initialize(parm, dt);
    }

    virtual ~NoiseChannel_BP(void)
    {}

    void reset() override
    {
        _bandpass_->reset();
    }

    DTYPE next(allpix::RandomNumberGenerator& random_generator) override
    {
        DTYPE val = 0;
        if(_ENC_bw_ > 0)
        {
            allpix::normal_distribution<double> _rgauss_(0, _ENC_bw_);
            val += _bandpass_->process_input(_rgauss_(random_generator));
        }
        if(_ENC_white_ > 0)
        {
            allpix::uniform_real_distribution<double> _rgauss_white_(-0.5*_ENC_white_, 0.5*_ENC_white_);
            val += _rgauss_white_(random_generator);
        }
        return val;
    }

    DTYPE scale_ENC(const NoiseChannel_Parameter &parm) const override
    {
        return parm.ENC_bw.value *  1.45 / (std::pow(2*(parm.bw_cutoff.value[1] - parm.bw_cutoff.value[0]) / _f_N_, 0.5));
    }

private:
    std::unique_ptr<BW_BP1> _bandpass_;

    virtual void initialize(const NoiseChannel_Parameter &parm, DTYPE dt)
    {
        _NoiseChannel_::initialize(parm, dt);
        _ENC_bw_ = scale_ENC(parm);
        _bandpass_ = std::make_unique<BW_BP1>( 1/dt, parm.bw_cutoff.value[1], parm.bw_cutoff.value[0], 0, false);
    }
};
