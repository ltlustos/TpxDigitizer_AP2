/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef ALPHAIKRUM_H
#define ALPHAIKRUM_H

#include "Common/dtype.h"
#include "Common/Exceptions.h"
#include "Util/u_math.h"

class _AlphaIkrum_
{
protected:
    _AlphaIkrum_()
    {}
    virtual ~_AlphaIkrum_()
    {}
public:
    virtual DTYPE alpha(DTYPE input)
    {
        throw SubclassResponsabilityException("_AlphaIkrum_:alpha");
        return input;
    }
};


class AlphaIkrum: public _AlphaIkrum_
{
public:
    AlphaIkrum(const std::vector<DTYPE> &fb_thresholds_e, const std::vector<DTYPE> &fb_ikrum_eps)
    {
        _fb_thresholds_e = fb_thresholds_e;
        _fb_ikrum_eps = fb_ikrum_eps;
        if (_fb_thresholds_e.size() == 1)
        {
            _fb_thresholds_e.emplace_back(std::numeric_limits<DTYPE>::infinity());
            _fb_ikrum_eps.emplace_back(_fb_ikrum_eps.front());
        }
        _piecewise_= std::make_unique<Piecewise<DTYPE>>(_fb_thresholds_e, _fb_ikrum_eps);
    }

    virtual ~AlphaIkrum()
    {}

    DTYPE alpha_base() const
    {
        return _fb_ikrum_eps[0];
    }

    DTYPE alpha(DTYPE input) override
    {
        return _piecewise_->calc(std::abs(input));
    }

    virtual void set(const std::vector<DTYPE> fb_thresholds_e, const std::vector<DTYPE> fb_ikrum_eps)
    {
        _fb_thresholds_e = fb_thresholds_e;
        _fb_ikrum_eps = fb_ikrum_eps;
        _fb_ikrum_eps.emplace_back(_fb_ikrum_eps.back());
    }

private:
    std::vector<DTYPE> _fb_thresholds_e;
    std::vector<DTYPE> _fb_ikrum_eps;
    std::unique_ptr<Piecewise<DTYPE>> _piecewise_;


};


#endif // ALPHAIKRUM_H
