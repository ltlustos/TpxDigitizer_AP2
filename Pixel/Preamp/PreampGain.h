#pragma once
#include "Parameter/CSAParameter.h"

class PreampGain
{
public:
    PreampGain(const CSAParameter &parm)
    {
        set(parm.gain.value, parm.gain_asymmetry.value);
    }

    void set(DTYPE gain, DTYPE gain_asymmetry)
    {
        _gain_pos_ = gain * (1.0 + 0.5* gain_asymmetry);
        _gain_neg_ = gain * (1.0 - 0.5* gain_asymmetry);
    }
    DTYPE processs(DTYPE in)
    {
        if (in < 0)
            return in *_gain_neg_;
        else
            return in * _gain_pos_;
    }

private:
    DTYPE _gain_pos_{1.0};
    DTYPE _gain_neg_{1.0};

};

