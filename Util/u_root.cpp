#include "u_root.h"

TH1D* create_TH1D(const char *name,const char *title,Int_t nbinsx,Double_t xlow,Double_t xup)
{
    delete_root_object_named<TH1D>(name);
    TH1D *h =  new TH1D(name, title, nbinsx, xlow, xup);
    h->SetCanExtend(TH1::kXaxis);
    h->SetCanExtend(TH1::kYaxis);
    return h;
}

TH2D* create_TH2D(const char *name,const char *title,Int_t nbinsx,Double_t xlow,Double_t xup
                  ,Int_t nbinsy,Double_t ylow,Double_t yup)
{
    delete_root_object_named<TH2D>(name);  // deleting root object the standard way gives segmentation error, trying to cleanup previous instances here.
    TH2D *h =  new TH2D(name, title, nbinsx,  xlow,  xup, nbinsy,  ylow,  yup);
    h->SetCanExtend(TH2::kXaxis);
    h->SetCanExtend(TH2::kYaxis);
    return h;
}

TH2I* create_TH2I(const char *name,const char *title,Int_t nbinsx,Double_t xlow,Double_t xup
                  ,Int_t nbinsy,Double_t ylow,Double_t yup)
{
    delete_root_object_named<TH2I>(name);
    TH2I *h =  new TH2I(name, title, nbinsx,  xlow,  xup, nbinsy,  ylow,  yup);
    h->SetCanExtend(TH2I::kXaxis);
    h->SetCanExtend(TH2I::kYaxis);
    return h;
}


void png_export_canvas(TCanvas *c,  std::filesystem::path fname)
{
    gSystem->ProcessEvents();
    TImage *img = TImage::Create();
    img->FromPad(c);
    img->WriteImage(fname.string().c_str());
    delete img;
}


void csv_export(TH1D *h, std::filesystem::path fname, const char* delimiter )
{
    std::ofstream file;
    file.open(fname.string().c_str());
    Int_t idx_max = h->  GetNbinsX();
    for (int i = 0; i <= idx_max; ++i)
    {
        double x = h->GetBinCenter(i);
        double y = h->GetBinContent(i);
        file << x << delimiter << y << "\n";
    }
    file.close();
}


void csv_export(TH2D *h, std::filesystem::path fname, const char* delimiter)
{
    std::ofstream file;
    file.open(fname.string().c_str());
    Int_t idx_max = h->GetNbinsX();
    Int_t idy_max = h->GetNbinsY();
    for (int i = 0; i <= idx_max; ++i)
        for (int j = 0; j <= idy_max; ++j)
        {
            int bin = h->GetBin(i, j);
            double val = h->GetBinContent(bin);
            if (val == 0)
                continue;
            file << h->GetXaxis()->GetBinCenter(i) << delimiter <<
                    h->GetYaxis()->GetBinCenter(j) << delimiter <<
                    val << "\n";
        }
    file.close();
}

void cleanup_tcanvas()
{
    TIter next(gROOT->GetListOfCanvases());
    while(TCanvas* c = static_cast<TCanvas*> (next()))
        delete c;
}

double norm(const ROOT::Math::XYPoint x)
{
    return std::sqrt(x.Mag2());
}

double norm(const ROOT::Math::XYZPoint x)
{
    return std::sqrt(x.Mag2());
}

