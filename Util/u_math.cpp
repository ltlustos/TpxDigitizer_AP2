#include "u_math.h"

bool is_odd(int n){
    return bool( n %2 );
}
int minus_one_to_n(int n) {
    return is_odd(n) ? -1 : 1;
}

int flip_sign_if_odd(int n) {
    return is_odd(n) ? -n : n;
}
