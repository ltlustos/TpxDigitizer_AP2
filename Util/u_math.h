/* Mpx3Sim ver. 0.1, 2018.08.27 */
/* Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef MATH_H
#define MATH_H

#include <cmath>
#include "math.h"
#include "assert.h"
#include "Common/Exceptions.h"
#include <vector>
#include "u_string.h"
#include <limits>
#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>
#include "core/utils/prng.h"
#include "core/utils/distributions.h"
#include <Eigen/Core>

#define SQRT2 std::sqrt(2.0)
#define SQRTPI std::sqrt(M_PI)

using Fp_info = std::numeric_limits<double>;
bool is_odd(int n);
int minus_one_to_n(int n);
int flip_sign_if_odd(int n);

template <typename T>
void div(std::vector<T> &vec, T val)
{
    for ( auto &v : vec ) v /= val;
}

template <typename T>
T square(T x)
{
    return x * x;
}

template <typename T>
T cube(T x)
{
    return x * x *x;
}

template <typename T>
int sign(T x)
{
    return (x > T(0)) - (x < T(0));
}

template < typename T>
bool ISNAN(T x)
{
    return std::isnan(x) || (x != x) || (x == Fp_info::quiet_NaN());
}


template < typename T>
T erf_norm(T x, T x0=0.0, T sigma=1.0){
    return std::erf( (x - x0) / (sigma * SQRT2) );
}

template < typename T>
T logistic(T x, T x0, T sigma)
{
    return 2 / (1 + std::exp(-(x -x0) / sigma)) -1;
}

template < typename T>
T sexp(T x, T x0, T sigma)
{
    return sign(x-x0)* (1.0 - std::exp(-std::abs(x-x0)/(sigma)));
}

template < typename T>
T erf_tanh(T x)
{
    return std::tanh( 1.202782416318701 * x);
}


template < typename T>
T gauss(T x, T mu, T sigma){
    T v = ( x - mu ) / sigma;
    return std::exp(- 0.5 * v * v );
}

template < typename T>
T gauss_norm(T x, T mu, T sigma){
    return gauss(x, mu, sigma) / (SQRT2 * SQRTPI * sigma);
}


template < typename T>
T erf_tanh(T x, T mu, T sigma){
    return erf_tanh( (x - mu) / sigma );
}


template < typename T>
T stdev(std::vector<T> v)
{
    T m1 = 0;
    T m2 = 0;
    for (auto x : v)
    {
        m1 += x;
        m2 += x * x;
    }

    m1 /= v.size();
    m2 /= v.size();
    return std::sqrt(m2 - m1 * m1);
}

template < typename T>
T min(std::vector<T> v)
{
    T m1 = 0;
    T m2 = 0;
    for (auto x : v)
    {
        m1 += x;
        m2 += x * x;
    }

    m1 /= v.size();
    m2 /= v.size();
    return std::sqrt(m2 - m1 * m1);
}

template < typename T>
T max(std::vector<T> v)
{
    T m1 = 0;
    T m2 = 0;
    for (auto x : v)
    {
        m1 += x;
        m2 += x * x;
    }

    m1 /= v.size();
    m2 /= v.size();
    return std::sqrt(m2 - m1 * m1);
}

template < typename T>
double polyval(std::vector<T> p, T x)
{
    T r = 0;
    for (size_t i = 0; i < p.size(); ++i)
        r += p[i]* std::pow(x, p.size() -1 -i);
    return r;
}


template < typename T>
T norm(T x, T y){
    return std::sqrt(x*x + y*y);
}

template < typename T>
T norm(T x, T y, T z){
    return std::sqrt(x*x + y*y + z*z);
}


template < typename T>
T linear(T x, T x0, T x1, T v0,   T v1)
{

    if ( std::isnan(v0) || std::isnan(v1) )
    {
        throw ThisShouldNotHappenException("liniear: nan input.");
    }

    T offs_x;
    if (x1 == x0)
        offs_x = 0;
    else
        offs_x = (x - x0) / (x1 - x0);

    T p = v0 * ( 1.0 - offs_x ) + v1 * offs_x;

    if ( std::isnan(p) ){
        throw NaNException();
    }
    return p;
}

template < typename T>
T bilinear(T x00,   T y00,
           T x11,   T y11,
           T x,      T y,
           T v00,   T v01,
           T v10,   T v11)
{
    T offs_x = 0.0;
    if (x11 != x00)
        offs_x = (x - x00) / (x11 - x00);

    T offs_y = 0.0;
    if (y11 != y00)
        offs_y = (y - y00) / (y11 - y00);

    T p0 = v00 * ( 1.0 - offs_x ) + v10 * offs_x;
    T p1 = v01 * ( 1.0 - offs_x ) + v11 * offs_x;
    T p  =  p0 * ( 1.0 - offs_y ) + p1 * offs_y;
    return p;
}

template < typename T>
T trilinear(T x000,   T y000,    T z000,
            T x111,   T y111,    T z111,
            T x,      T y,       T z,
            T v000,   T v010,    T v001, T v011,
            T v100,   T v110,    T v101, T v111)
{
    T offs_x =0.0;
    if (x111 != x000)
        offs_x = (x - x000) / (x111 - x000);

    T offs_y =0.0;
    if (y111 != y000)
        offs_y = (y - y000) / (y111 - y000);

    T offs_z =0.0;
    if (z111 != z000)
        offs_z = (z - z000) / (z111 - z000);

    T p00 = v000 * ( 1.0 - offs_x ) + v100 * offs_x;
    T p01 = v001 * ( 1.0 - offs_x ) + v101 * offs_x;
    T p10 = v010 * ( 1.0 - offs_x ) + v110 * offs_x;
    T p11 = v011 * ( 1.0 - offs_x ) + v111 * offs_x;

    T p0 = p00 * ( 1.0 - offs_y ) + p10 * offs_y;
    T p1 = p01 * ( 1.0 - offs_y ) + p11 * offs_y;

    T p =  p0  * ( 1.0 - offs_z ) + p1 * offs_z;

    return p;
}


template < typename T>
void diff_inplace(T* y, int len=-1) {
    if (len ==0)
        return;
    for (int i=0; i<len-1; i++){
        y[i] = y[i+1] - y[i];
    }
    y[len-1] = 0;
}

template < typename T>
void diff_inplace(std::vector<T> &y, int len=-1) {
    if (len==-1)
        len = y.size();
    for (int i=0; i<len-1; i++){
        y[i] = y[i+1] - y[i];
    }
    y[len-1] = 0;
}

template < typename T>
void gradient_inplace(std::vector<T> &x) {

    std::size_t n = x.size() -1;

    if (n==0)
        x[0] = 0;
    else {
        T last_1 = x[n-1];
        T x_1 = x[0];
        x[0] = x[1] - x[0];
        for (std::size_t i = 1; i < n-1; ++i)
        {
            x_1 = x[i];
            x[i] = (x[i+1] - x_1) / 2;
        }
        x[n] = x[n] -x[n-1];
    }
}


template < typename T>
std::vector<T> diff(std::vector<T> &x) {
    std::vector<T> g;
    std::size_t n = x.size() -1;
    if (n==0)
        g.emplace_back(0);
    for (std::size_t i=0; i < n; i++)
        g.emplace_back( x[i+1] - x[i]);
    return g;
}

template < typename T>
std::vector<T> gradient(std::vector<T> &x) {
    std::vector<T> g;
    std::size_t n = x.size() -1;
    if (n==0)
        g.emplace_back(0);
    else {
        g.emplace_back(x[1] - x[0]);
        for (std::size_t i = 1; i < n-1; ++i)
            g.emplace_back((x[i+1] - x[i-1]) / 2);
        g.emplace_back(x[n] - x[n-1]);
    }
    return g;
}

template <typename T>
class Data2 {
public:

    Data2()
    {
        __data__        = NULL;
    }

    Data2(size_t n1, size_t n2)
    {
        initialize(n1, n2);
    }

    Data2(size_t n1, size_t n2, T val):Data2(n1, n2)
    {
        reset(val);
    }

    Data2(const Data2<T>& other)
    {
        *this = other;
    }

    Data2& operator =(const Data2& other)
    {
        initialize(other.__shape__[0], other.__shape__[1]);
        for (size_t i =0; i < __shape__[0]; ++i)
            memcpy(__data__[i], other.__data__[i], __shape__[1] * sizeof(T));
        return *this;
    }

    virtual ~Data2(){
        __free_data__();
    }

    void resize(size_t n1, size_t n2){
        __free_data__();
        initialize(n1, n2);
    }

    void setZero(){
        for (size_t i =0; i < __shape__[0]; ++i)
            memset(__data__[i], 0, __shape__[1] * sizeof(T));
    }

    void fill_randn(allpix::RandomNumberGenerator& random_generator, T mean, T std)
    {
        allpix::normal_distribution<T> r(mean, std);
        for (size_t i =0; i < __shape__[0]; ++i)
            for (size_t j=0; j<__shape__[1]; j++)
                __data__[i][j] = r(random_generator);
    }


    void fill_poisson(allpix::RandomNumberGenerator& random_generator, T mean)
    {
        allpix::poisson_distribution<T> r(mean);
        for (size_t i =0; i < __shape__[0]; ++i)
            for (size_t j=0; j<__shape__[1]; j++)
                __data__[i][j] = r(random_generator, 1);
    }


    void list() const
    {
        for (size_t i =0; i < __shape__[0]; ++i)
        {
            printf("[");
            for (size_t j=0; j<__shape__[1]; j++)
                printf(" %5g", __data__[i][j]);
            printf("]\n");
        }
    }

    void reset(T val)
    {
        for (size_t i =0; i < __shape__[0]; ++i)
#pragma omp simd
            for (size_t j=0; j<__shape__[1]; j++)
                __data__[i][j] = val;
    }

    T operator() (const size_t i, const size_t j)
    {
        return __data__[i][j];
    }

    T& coeff(const size_t i, const size_t j)  const
    {
        return __data__[i][j];
    }

    T operator() (size_t idxx[2]) const
    {
        return this(idxx[0], idxx[1]);
    }


    T* operator[](size_t i) const
    {
        return __data__[i];
    }

    Data2 operator + (T val)
    {
        Data2 d(this);
        d += val;
        return *d;
    }

    Data2& operator += (T val)
    {

        for (size_t i = 0; i < __shape__[0]; ++i)
#pragma omp simd
            for (size_t j = 0; j < __shape__[1]; ++j)
                __data__[i][j] += val;
        return *this;
    }


    Data2 operator - (T val)
    {
        Data2 d(this);
        d -= val;
        return *d;
    }

    Data2& operator -= (T val)
    {
        for (size_t i = 0; i < __shape__[0]; ++i)
#pragma omp simd
            for (size_t j = 0; j < __shape__[1]; ++j)
                __data__[i][j] -= val;
        return *this;
    }
    Data2 operator * (T val)
    {
        Data2 d(this);
        d *= val;
        return *d;
    }

    Data2& operator *= (T val)
    {
        for (size_t i = 0; i < __shape__[0]; ++i)
#pragma omp simd
            for (size_t j = 0; j < __shape__[1]; ++j)
                __data__[i][j] *= val;
        return *this;
    }


    Data2 operator / (T val)
    {
        Data2 d(this);
        d /= val;
        return *d;
    }

    Data2& operator /= (T val)
    {
        T f = 1.0 / val;
        for (size_t i = 0; i < __shape__[0]; ++i)
#pragma omp simd
            for (size_t j = 0; j < __shape__[1]; ++j)
                __data__[i][j] *= f;
        return *this;
    }

    void save_ascii(std::string fname)  const
    {
        FILE *fp = fopen(fname.c_str(), "w");
        for (size_t i = 0; i < __shape__[0]; ++i){
            for (size_t j = 0; j < __shape__[1]-1; ++j)
                fprintf(fp, "%8.3f\t", __data__[i][j]);
            fprintf(fp, "%8.3f\n", __data__[i][__shape__[1]-1]);
        }
        fclose(fp);
    }

    void load_ascii(std::string fname)
    {
        std::filebuf fb;
        if (fb.open (fname.c_str(), std::ios::in))
        {
            std::istream is(&fb);
            std::string line;
            std::vector<std::string> r;
            size_t i = 0;
            while (std::getline(is, line))
            {
                r = str_split(line, "\t", false);
                for (size_t j =0; j < r.size(); ++j)
                    __data__[i][j] = std::stod(r[j]);
                ++i;
            }
            fb.close();
        } else
            throw FileNotFoundNException(fname.c_str());
    }
    void set(const size_t i, const size_t j, T val){
        __data__[i][j] = val;
    }


    size_t* shape()
    {
        return __shape__;
    }


    void _load_ (FILE *fp)
    {
        std::size_t s[2];
        for (int i =0; i<2; i++){
            fread(s+i,   sizeof(std::size_t), 1, fp);
            if ( s[i] != __shape__[i])
                throw ParameterOutOfRangeException("Array2 dimension do not match.");
        }
        for (std::size_t i = 0; i<__shape__[0]; i++)
            fread(__data__[i], sizeof(T), __shape__[1], fp);
    }

    void _save_ (FILE *fp){
        for (int i = 0; i<2; i++)
            fwrite(&(__shape__[i]), sizeof(std::size_t), 1, fp);

        for (std::size_t i = 0; i<__shape__[0]; i++)
            fwrite(__data__[i], sizeof(T), __shape__[1], fp);
    }


    void load (const char* fname)
    {
        FILE *fp = fopen(fname, "rb");
        _load_(fp);
        fclose(fp);
    }

    void save (const char* fname) const
    {
        FILE *fp = fopen(fname, "wb");
        _save_(fp);
        fclose(fp);
    }


protected:
    size_t __shape__[2];
    T **__data__;


private:

    void __free_data__(){
        if (__data__ != NULL){
            for (size_t i =0; i < __shape__[0]; ++i)
                delete [] __data__[i];
            delete  [] __data__;
            __data__ = NULL;
        }
    }

    void initialize(size_t n1, size_t n2){
        __shape__[0] = n1;
        __shape__[1] = n2;
        __data__ = new T*[__shape__[0]];
        for (size_t i =0; i < __shape__[0]; ++i){
            __data__[i] = new T[__shape__[1]]{0};
        }
    }
};


template <typename T>
class Data3 {
public:

    Data3(){
        __data__  = nullptr;
    }

    Data3(size_t n1, size_t n2, size_t n3){
        initialize(n1, n2, n3);
    }

    Data3(size_t s[3]){
        initialize(s[0], s[1], s[2]);
    }

    Data3(size_t n1, size_t n2, size_t n3, T val):Data3(n1, n2, n3){
        reset(val);
    }

    virtual ~Data3(){
        __free_data__();
    }

    void resize(size_t n1, size_t n2, size_t n3){
        __free_data__();
        initialize(n1, n2, n3);
    }

    void setZero(){
        for (size_t i =0; i < __shape__[0]; ++i)
            __data__->setZero();
    }

    void reset(T val){
        for (size_t i =0; i < __shape__[0]; ++i)
            __data__[i].reset(val);

    }

    T operator() (const size_t i, const size_t j, const size_t k) const
    {
        return __data__[i](j, k);
    }

    T operator() (std::size_t idxx[3]) const
    {
        return this(idxx[0], idxx[1], idxx[2]);
    }


    Data2<T>& operator[](size_t i) const
    {
        return __data__[i];
    }



    size_t* shape()
    {
        return __shape__;
    }

    void load (const char* fname)
    {
        FILE *fp = fopen(fname, "rb");
        std::size_t s[3];
        for (int i =0; i<3; i++){
            fread(s+i,   sizeof(std::size_t), 1, fp);
            if ( s[i] != __shape__[i])
                throw ParameterOutOfRangeException("Array3 dimension do not match.");
        }
        for (std::size_t i = 0; i<__shape__[0]; i++)
            __data__[i]->load(fp);
        fclose(fp);
    }

    void save (const char* fname) const
    {
        FILE *fp = fopen(fname, "wb");
        for (int i =0; i < 3; i++)
            fwrite(&(__shape__[i]), sizeof(std::size_t), 1, fp);

        for (std::size_t i = 0; i<__shape__[0]; i++)
            __data__[i].save(fp);
        fclose(fp);
    }

    void set(const size_t i, const size_t j, const size_t k, T val){
        __data__[i].set(j, k, val);
    }


protected:
    size_t __shape__[3];
    Data2<T> *__data__;


private:
    void __free_data__(){
        if (__data__ == nullptr){
            delete  [] __data__;
            __data__ = nullptr;
        }
    }

    void initialize(size_t n1, size_t n2, size_t n3){
        __shape__[0] = n1;
        __shape__[1] = n2;
        __shape__[2] = n3;
        __data__ = new Data2<T>[__shape__[0]];
        for (std::size_t i = 0; i<__shape__[0]; i++)
            __data__[i].resize(n2, n3);

    }
};


// ##############################################################################################################################################

template < typename T>
class Moment
{
public:
    Moment(){
        _m0 = _m1 = _m2 = 0.0;
        mean = stdev = std::numeric_limits<double>::quiet_NaN();
        _n = 0;
    }
    ~Moment(){}
    T mean;
    T stdev;
    void add(T x, T weight=1, bool update=false){
        _m0 += weight;
        _m1 += weight * x;
        _m2 += weight * x * x;
        ++_n;
        if (update){
            calc();
        }
    }
    void calc()
    {
        mean = _m1 / _m0;
        if (_n<2)
            stdev = 0.0;
        else
            stdev  = sqrt(_m2 / _m0 - mean * mean);
    }

    T m0() const
    {
        return _m0;
    }

    std::size_t n()  const
    {
        return _n;
    }

private:
    T _m0, _m1, _m2;
    std::size_t _n;
};

// ##############################################################################################################################################


template <typename T>
class _Piecewise_func_
{
public:
    _Piecewise_func_() = default;
    virtual ~_Piecewise_func_() = default;
    virtual T calc(T)
    {
        throw SubclassResponsabilityException();
    }
};

template <typename T>
class _Piecewise_Constant_: public _Piecewise_func_<T>
{
public:
    ~_Piecewise_Constant_() = default;
    _Piecewise_Constant_(T value)
        :_Piecewise_func_<T>()
    {
        _value_ = value;
    }

    T calc(T) override
    {
        return _value_;
    }
protected:
    _Piecewise_Constant_() = default;
    T _value_{0};
};


template <typename T>
class _Piecewise_Linear_: public _Piecewise_func_<T>
{
public:
    ~_Piecewise_Linear_() = default;
    _Piecewise_Linear_(T k, T d)
        :_Piecewise_func_<T>()
    {
        _k_ = k;
        _d_ = d;
    }

    T calc(T x) override
    {
        return _k_ * x  + _d_;
    }
protected:
    _Piecewise_Linear_() = default;

    T _k_{0}, _d_{0};
};

//#######################################################################################

template < typename T, size_t N>
class MomentsN {
public:
    MomentsN(){
        _sum = 0.0;
        m1.setZero();
        m2.setZero();
        mean.setConstant(std::numeric_limits<double>::quiet_NaN());
        stdev.setConstant(std::numeric_limits<double>::quiet_NaN());
        _n = 0;
    }
    ~MomentsN(){}

    MomentsN operator  = (const MomentsN& other){
        _sum     = other._sum;
        m1      = other.m1;
        m2      = other.m2;
        mean    = other.mean;
        stdev   = other.stdev;
        _n       = other._n;
    }

    Eigen::Array<T, N, 1> mean;
    Eigen::Array<T, N, 1> stdev;

    void add(Eigen::Array<T, N, 1> vec, T weight, bool update=false){
        _sum += weight;
        m1  += vec * weight;
        m2  += vec * vec * weight ;
        ++_n;
        if (update){
            calc();
        }
    }

    void calc(){
        mean = m1 / _sum;
        if (_n < 2)
            stdev.setZero();
        else
            for (int i = 0; i<3; ++i){
                stdev[i] = std::max(T(0.0), sqrt( m2[i] / _sum - mean[i] * mean[i] ));
            }
    }

    std::size_t n() const
    {
        return _n;
    }

    std::size_t sum() const
    {
        return _sum;
    }

private:
    Eigen::Array<T, N, 1> m1;
    Eigen::Array<T, N, 1> m2;
    T _sum;
    std::size_t _n;
};

template <typename T>
using Moments3 = MomentsN<T, 3>;
//#######################################################################################
template <typename T>
class Piecewise
{
public:
    Piecewise(std::vector<T> thresholds, std::vector<T> values )
    {
        _thresholds_ = thresholds;
        if (_thresholds_.size() != values.size())
            throw ParameterException("no of thresholds and no of values do not match.");
        for (size_t i =0; i < _thresholds_.size()-1; ++i)
        {
            if (values[i] == values[i+1])
            {
                _funcs_.emplace_back(std::make_unique<_Piecewise_Constant_<T>> (values[i]));
            }
            else
            {
                T k = (values[i+1] - values[i])  / (_thresholds_[i+1] - _thresholds_[i]) ;
                T d = values[i] - k * _thresholds_[i];
                _funcs_.emplace_back(std::make_unique<_Piecewise_Linear_<T>>(k, d));
            }
        }
    }

    T calc(T val)
    {
        auto t = std::upper_bound(_thresholds_.begin(), _thresholds_.end(),  val);
        std::size_t idx = t - _thresholds_.begin() - 1;
        return _funcs_[idx]->calc(val);
    }
protected:
    std::vector<std::unique_ptr<_Piecewise_func_<T>>> _funcs_;
    std::vector<T> _thresholds_;
};
#endif // MATH_H
