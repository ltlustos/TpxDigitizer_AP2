#ifndef U_OS_H
#define U_OS_H
#include <chrono>
#include <string>
#include <exception>
#include <stdexcept>

std::string get_date_string(std::chrono::system_clock::time_point t = std::chrono::system_clock::now());


#endif // U_OS_H
