/* Mpx3Sim ver. 0.1, 2018.08.27 */
/* Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#pragma once
#include <string>
#include <cstdarg>
#include <regex>
#include <vector>

#pragma GCC diagnostic ignored "-Wformat-nonliteral"
#pragma GCC diagnostic ignored "-Wformat-security"
template<typename ... Args>
std::string string_sprintf( const std::string& format, Args ... args )
{
    std::size_t size = std::size_t(std::snprintf( nullptr, 0, format.c_str(), args ... ) + 1); // +1 for '\0'
    std::unique_ptr<char[]> buf( new char[ size ] );
    std::snprintf( buf.get(), size, format.c_str(), args ... );
    return std::string( buf.get(), buf.get() + size - 1 ); // -1 to remove \0
}
#pragma GCC diagnostic warning "-Wformat-nonliteral"
#pragma GCC diagnostic warning "-Wformat-security"

std::string to_upper(const std::string str);
std::string to_lower(const std::string str);
std::vector<std::string> str_split(std::string s, std::string sep, bool stop_first);
std::vector<std::string> str_split(const std::string &text, char sep, bool stop_first);

#pragma GCC diagnostic ignored "-Wmissing-declarations"
std::pair<std::string, std::string> _split_at_(std::string astr, size_t sep);
#pragma GCC diagnostic warning "-Wmissing-declarations"

std::vector<std::string> split(std::string s, std::string sep, bool stop_first);
std::vector<std::string> split(const std::string &text, char sep, bool stop_first);

std::pair<std::string, std::string> str_split_first(std::string astr, std::string sep);
std::pair<std::string, std::string> str_split_last(std::string astr, std::string sep);
bool str_starts_with(const std::string str, const std::string& substr);
bool str_ends_with(const std::string str, const std::string& substr);


bool str_equal(std::string s1, std::string s2, bool case_sensitive);
bool str_equal(const char *s1, const char *s2, bool case_sensitive);
bool str_includes(std::string s1, std::string s2, bool case_sensitive);

std::vector<std::string> regfilter(std::string s, std::regex r);


std::string& rtrim(std::string& s, const char* c);

std::string& ltrim(std::string& s, const char* c);

std::string& trim(std::string& s, const char* c);

