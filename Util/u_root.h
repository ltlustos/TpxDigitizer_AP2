/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#ifndef U_ROOT_H
#define U_ROOT_H
#include <TCanvas.h>
#include <TImage.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TSystem.h>
#include <fstream>
#include <filesystem>
#include <cstdio>
#include "TROOT.h"
#include <Math/Cartesian2D.h>
#include <Math/Point2D.h>
#include <Math/Point3D.h>


template <class T>
void delete_root_object_named(const char *name)
{
    T *h = static_cast<T*>( gROOT->FindObject(name));
    delete h;
}

template <class T>
void delete_root_object(T *obj)
{
    obj->Reset();
    delete_root_object_named<T>(obj->GetName());
}

TH1D* create_TH1D(const char *name,const char *title,Int_t nbinsx,Double_t xlow,Double_t xup);
TH2D* create_TH2D(const char *name,const char *title,Int_t nbinsx,Double_t xlow,Double_t xup, Int_t nbinsy,Double_t ylow,Double_t yup);
TH2I* create_TH2I(const char *name,const char *title,Int_t nbinsx,Double_t xlow,Double_t xup, Int_t nbinsy,Double_t ylow,Double_t yup);

template <class T>
void png_export(T *h, std::filesystem::path fname, const char* draw_option="A", bool grid=true, bool logy=false)
{
    TCanvas c(true);
    if (logy)
        c.SetLogy();
    if (grid)
        c.SetGrid();
    h->Draw(draw_option);
    TLegend* l = c.BuildLegend(0.5, 0.7, 0.8, 0.9);
    l->SetTextSize(0.020);
    l->SetBorderSize(0.0);
    l->SetFillStyle(0);
    gSystem->ProcessEvents();
    TImage *img = TImage::Create();
    img->FromPad(&c);
    img->WriteImage(fname.string().c_str());
    delete img;
}

void png_export_canvas(TCanvas *c, std::filesystem::path fname);
void csv_export(TH1D *h, std::filesystem::path fname, const char* delimiter="\t" );
void csv_export(TH2D *h, std::filesystem::path fname, const char* delimiter="\t");

void cleanup_tcanvas();

double norm(const ROOT::Math::XYPoint x);
double norm(const ROOT::Math::XYZPoint x);

#endif // U_ROOT_H
