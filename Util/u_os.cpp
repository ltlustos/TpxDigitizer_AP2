#include "u_os.h"


std::string get_date_string(std::chrono::system_clock::time_point t)
{
    auto as_time_t = std::chrono::system_clock::to_time_t(t);
    struct tm tm;
    char buf[64] = { 0 };
    if (::gmtime_r(&as_time_t, &tm))
        if (std::strftime(buf, 64, "%d-%m-%Y %H:%M:%S", &tm))
            return std::string{buf};
    throw std::runtime_error("Failed to get current date as string");
}

