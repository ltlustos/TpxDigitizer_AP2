/* Mpx3Sim ver. 0.1, 2018.08.27 */
/* Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#include "u_string.h"
#include "string.h"
#include "limits.h"
#include <unistd.h>
#include "Common/Exceptions.h"
#include "Util/u_string.h"

#include <sstream>
#include "stdarg.h"
#include <memory>
#include <iostream>
#include <cstdio>


std::vector<std::string> split(std::string s, std::string sep, bool stop_first) {
    std::string _s = s; // copy
    std::vector<std::string> tokens;
    size_t pos = 0;
    while ((pos = _s.find(sep)) != std::string::npos) {
        tokens.push_back(_s.substr(0, pos));
        _s.erase(0, pos + sep.length());
        if (stop_first){
            tokens.push_back(_s);
            break;
        }
    }
    if (tokens.size() == 0)
        tokens.push_back(s);
    else
        tokens.push_back(_s);
    return tokens;
}
std::vector<std::string> split(const std::string &text, char sep, bool stop_first) {
    char _sep[2]={'\0'};
    _sep[0] = sep;
    return split(std::string(text), std::string(_sep), stop_first);
}


std::string to_upper(const std::string str){
    std::string c = str;
    std::transform(c.begin(), c.end(), c.begin(), ::toupper);
    return c;
}

std::string to_lower(const std::string str){
    std::string c = str;
    std::transform(c.begin(), c.end(), c.begin(), ::tolower);
    return c;
}



// trim from end of string (right)
std::string& rtrim(std::string& s, const char* c)
{
    s.erase(s.find_last_not_of(c) + 1);
    return s;
}

// trim from beginning of string (left)
std::string& ltrim(std::string& s, const char* c)
{
    s.erase(0, s.find_first_not_of(c));
    return s;
}

std::string& trim(std::string& s, const char* c){
    return ltrim(rtrim(s, c), c);
}

bool str_starts_with(const std::string str,  const std::string &s){
    return strncmp(str.c_str(), s.c_str(), s.size()) == 0;
}

bool str_ends_with(const std::string str, const std::string &s)
{
    if(str.size() >= s.size() &&
            str.compare(str.size() - s.size(), s.size(), s) == 0)
        return true;
    else
        return false;
}

bool str_equal(std::string s1, std::string s2, bool case_sensitive)
{
    if (case_sensitive)
        return s1.compare(s2) == 0;
    else
        return to_upper(s1).compare(to_upper(s2)) == 0;
}

bool str_equal(const char *s1, const char *s2, bool case_sensitive)
{
    return str_equal(std::string(s1), std::string(s2), case_sensitive);
}

bool str_includes(std::string s1, std::string s2, bool case_sensitive)
{
    if (case_sensitive)
        return (s1.find(s2) != std::string::npos);
    else
        return (to_upper(s1).find(to_upper(s2)) != std::string::npos);
}

