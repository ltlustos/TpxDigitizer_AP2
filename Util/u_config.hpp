#ifndef U_CONFIG_H
#define U_CONFIG_H

#include "core/config/Configuration.hpp"
#include "core/config/ConfigReader.hpp"
#include <paths.h>
namespace allpix
{
void include_config( allpix::Configuration* __config__, std::string key);
void include_config( allpix::Configuration* __config__, std::string key)
{
    if (__config__->has(key))
    {
        std::string config_file_name = __config__->getPath(key, true);
        std::ifstream config_file(config_file_name);
        allpix::ConfigReader config_reader(config_file, config_file_name);
        auto default_configs = config_reader.getConfigurations();
        __config__->merge(default_configs[0]);
    }
}
}

#endif // U_CONFIG_H
