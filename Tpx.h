/* MpxSim 2021, Lukas Tlustos, CERN, Univ Houston, FMF Freiburg */

#pragma once
#include "Pixel/Preamp/Preamp.h"
#include "Pixel/Disc/Disc.h"
#include "TpxParameter.h"
#include "Common/Time.h"
#include "core/utils/prng.h"

class Tpx
{
public:
    std::unique_ptr<Preamp> preamp;
    std::unique_ptr<_Disc_> disc;
    Time time;
    DTYPE charge_arrival_time{-1.0};


    Tpx(const TpxParameter &parm)
    {
        _parm_ = &parm;
        time.setup(_parm_->dt_sec.value, 0);
        preamp = std::make_unique<Preamp>(parm.preamp_parm, time, parm.h_cutoff.value);
        if (parm.disc_parm.type.value == DiscParameter::DiscType::IDEAL)
            disc = std::make_unique<DiscIdeal>(parm.disc_parm, time);
        else if (parm.disc_parm.type.value == DiscParameter::DiscType::INTEGRATOR)
            disc = std::make_unique<DiscInt>(parm.disc_parm, time);
        else if (parm.disc_parm.type.value == DiscParameter::DiscType::ASYM)
            disc = std::make_unique<DiscAsymmetric>(parm.disc_parm, time);
        else
            throw ParameterException();
    }

    ~Tpx() =default;

    std::size_t row() const
    {
        return disc->row();
    }

    std::size_t col() const
    {
        return disc->col();
    }

    void input_add(DTYPE val)
    {
        if (charge_arrival_time <= 0 && std::abs(val) > 0.01)
            charge_arrival_time = time.now();
        preamp->input_add(val);
    }

    void set_position(std::size_t row, std::size_t col)
    {
        disc->set_position(row, col);
    }

    void set_threshold(DTYPE val)
    {
        disc->set_threshold(val);
        preamp->reset();
    }

    void set_preamp_gain(DTYPE val)
    {
        preamp->csa->set_gain(val, _parm_->preamp_parm.csa_parm.gain_asymmetry.value);
    }

    void set_alpha_ikrum(const std::vector<DTYPE> thresholds_e, const std::vector<DTYPE> alpha_ikrum_eps)
    {
        preamp->set_alpha_ikrum(thresholds_e, alpha_ikrum_eps);
    }

    void reset(DTYPE time0=0)
    {
        preamp->reset();
        disc->reset(time0);
        time.reset(0.0);
        charge_arrival_time = -1.0;
    }

    bool is_idle() const
    {
        return preamp->is_idle();
    }

    void set_idle(bool idle)
    {
        preamp->set_idle(idle);
    }

    void process(allpix::RandomNumberGenerator& random_generator)
    {
        preamp->process(random_generator);
        disc->process(random_generator, preamp->out());
    }

    void closeShutter()
    {
        disc->closeShutter();
    }

    DTYPE out() const
    {
        return preamp->out();
    }


private:
    Tpx() = default;
    const TpxParameter* _parm_;
};

