#ifndef OUTPUTPARAMETER_H
#define OUTPUTPARAMETER_H

#include <string>
#include "modules/TpxDigitizer/Parameter/_ParameterSet_.h"
#include "Util/u_os.h"
#include "modules/TpxDigitizer/Parameter/Parameter.h"

class OutputParameter: public _Parameter_
{
public:
    OutputParameter()
    {}

    ~OutputParameter() = default;

    std::filesystem::path config_path{"./"};


    PARAMETER::StringParameter output_prefix{"output_prefix", false, ""};
    PARAMETER::BooleanParameter output_event{"output_event", false, true};  // -> root
    PARAMETER::BooleanParameter output_histogram{"output_histogram", false, true};  // -> root
    PARAMETER::BooleanParameter timestamp_output_path{"timestamp_output_path", false, false};
    PARAMETER::BooleanParameter output_tot_frame{"output_tot_frame", false, true};
    PARAMETER::TParameter<std::size_t> output_event_plot{"output_event_plot", false, 0};
    PARAMETER::BooleanParameter detailed_event_plot{"detailed_event_plot", false, false};
    PARAMETER::TParameter<std::size_t> output_event_csv{"output_event_csv", false, 0};
    PARAMETER::BooleanParameter output_histogram_csv{"output_histogram_csv", false, false};
    PARAMETER::BooleanParameter output_histogram_png{"output_png", false, true};
    PARAMETER::BooleanParameter output_listmode{"output_listmode", false, true};

    PARAMETER::BooleanParameter output_single_pixel_histogram{"output_single_pixel_histogram", false, false};
    PARAMETER::BooleanParameter output_single_pixel_listmode{"output_single_pixel_listmode", false, false};

    OutputParameter& operator = (const OutputParameter& o) = default;

    std::filesystem::path fullpath(const std::string filename)
    {
        return _output_path_.value / ( output_prefix.value + filename);
    }

    void check_values() const override
    {}

    void adjust_values() override
    {
        if (timestamp_output_path.value)
            _output_path_.value /= get_date_string();
    }

    void adjust_output_path(const allpix::Configuration& global_config)
    {
        _full_output_path_ = _output_path_.value;
        if (_full_output_path_.is_relative())
        {
            std::filesystem::path output_path = config_path.parent_path();
            if (global_config.has("output_directory"))
                output_path = global_config.getPath("output_directory");
            _full_output_path_ = output_path / _output_path_.value;
        }
    }

    std::filesystem::path get_output_path() const
    {
        return _full_output_path_;
    }

    void parse_config(const allpix::Configuration &config) override
    {
        config_path = config.getFilePath();

        _output_path_.from_parameter_file(config);
        timestamp_output_path.from_parameter_file(config);
        output_prefix.from_parameter_file(config);
        output_histogram.from_parameter_file(config);
        output_event.from_parameter_file(config);
        output_single_pixel_histogram.from_parameter_file(config);
        output_listmode.from_parameter_file(config);
        output_event_plot.from_parameter_file(config);
        detailed_event_plot.from_parameter_file(config);
        output_event_csv.from_parameter_file(config);
        output_histogram_csv.from_parameter_file(config);
        output_histogram_png.from_parameter_file(config);
        output_single_pixel_listmode.from_parameter_file(config);
    }
private:
    PARAMETER::PathParameter _output_path_{"output_path", false, "./"};
    std::filesystem::path _full_output_path_;
};

#endif // OUTPUTPARAMETER_H
