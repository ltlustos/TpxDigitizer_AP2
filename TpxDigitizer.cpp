#include "TpxDigitizer.h"
#include "core/config/ConfigManager.hpp"

using namespace allpix;

TpxDigitizer::TpxDigitizer(std::shared_ptr<TpxParameter> tpx_parameter, TDirectory *rootDirectory)
{
    __rootDirectory__ = rootDirectory;
    RandomNumberGenerator random_engine;
    random_engine.seed(tpx_parameter->rand_seed());
    __tpx_parameter__ = tpx_parameter;

    DTYPE max_hist_charge_ke = tpx_parameter->max_hist_charge_ke.value;
    max_hist_charge_ke += 6* 1e-3 * std::sqrt(square(__tpx_parameter__->preamp_parm.noise_parm.ENC_bw.value) + square(__tpx_parameter__->preamp_parm.noise_parm.ENC_white.value)) ;
    __current_event_clock_phase = 0;
    tpx = std::make_unique<Tpx>(*__tpx_parameter__);
    __gain_matrix__.resize(__tpx_parameter__->n_rows(), __tpx_parameter__->n_cols());

    __gain_matrix__.fill_randn(random_engine,
                               __tpx_parameter__->preamp_parm.csa_parm.gain.value,
                               __tpx_parameter__->preamp_parm.csa_parm.gain.value * __tpx_parameter__->preamp_parm.csa_parm.gain_dispersion.value);

    __alpha_ikrum_offset_matrix.resize(__tpx_parameter__->n_rows(), __tpx_parameter__->n_cols());
    __alpha_ikrum_offset_matrix.fill_randn(random_engine,
                                           0.0,
                                           __tpx_parameter__->preamp_parm.feedback_parm.alpha_ikrum_parm.fb_ikrum_sigma.value);

    __threshold_matrix__.resize(__tpx_parameter__->n_rows(), __tpx_parameter__->n_cols());
    __threshold_matrix__.fill_randn(random_engine,
                                    __tpx_parameter__->disc_parm.threshold.value,
                                    __tpx_parameter__->disc_parm.threshold_sigma.value);

    std::filesystem::create_directories(__tpx_parameter__->output_parm.get_output_path());
    const auto copyOptions = std::filesystem::copy_options::overwrite_existing;
    std::filesystem::copy(__tpx_parameter__->output_parm.config_path,
                          __tpx_parameter__->output_parm.get_output_path() / (__tpx_parameter__->output_parm.output_prefix.value + "run.conf"), copyOptions);


    if (__tpx_parameter__->output_parm.output_listmode.value)
            __lm_log__.open(__tpx_parameter__->output_parm.get_output_path() / (__tpx_parameter__->output_parm.output_prefix.value + "lm_log"));
    if (__tpx_parameter__->output_parm.output_single_pixel_listmode.value)
        __lm_log_sp__.open(__tpx_parameter__->output_parm.get_output_path() / ( __tpx_parameter__->output_parm.output_prefix.value + "lm_log_sp"));


    if (__tpx_parameter__->output_parm.output_histogram.value)
        __hist_export__.init(max_hist_charge_ke, *__tpx_parameter__);
    if (__tpx_parameter__->output_parm.output_single_pixel_histogram.value)
    {
        std::string sp_path = __tpx_parameter__->output_parm.get_output_path() / "sp/";
        std::filesystem::create_directories(sp_path);
        __hist_export_single_pixel_hit__.init(max_hist_charge_ke, *__tpx_parameter__, sp_path, "single pixel - ");
    }

    __event_export__ = std::make_unique<EventExport> (__rootDirectory__, *__tpx_parameter__);

    //FIXIT: to be FIXED/CHECKED for multithreading, should run only once
    if (__tpx_parameter__->run_enc.value)
        __run_enc_std__(random_engine);
}

void TpxDigitizer::process_event(allpix::RandomNumberGenerator& random_generator,
                                 const PixelCharge &pixel_charge,
                                 unsigned int event_num,
                                 bool is_sp_event)
{

    auto pixel = pixel_charge.getPixel();
    auto pixel_index = pixel.getIndex();
    auto total_input_charge = static_cast<double>(pixel_charge.getCharge());
    const auto& pulse = pixel_charge.getPulse(); // the pulse containing charges and times
    double timestep = pulse.getBinning() *1e-9;  // [s]

    if ((__tpx_parameter__->pileup_event.value>0) && event_num > 1)
    {

        Pulse pulse_orig = pulse; // the vector of the charges
        std::vector<double>  offsets(__tpx_parameter__->pileup_event.value, __tpx_parameter__->pileup_period.value);
        if (__tpx_parameter__->pileup_poisson.value)
        {
            LOG(DEBUG) << "pileup_period: " << __tpx_parameter__->pileup_period.value;
            allpix::exponential_distribution<DTYPE> exponential_distribution(1.0);
            LOG(DEBUG)  << "exponential_distribution(random_generator): " << exponential_distribution(random_generator);
            for (size_t k=0; k < __tpx_parameter__->pileup_event.value; ++k)
            {

                offsets[k] = exponential_distribution(random_generator) * __tpx_parameter__->pileup_period.value;
                LOG(DEBUG) << "offsets[" << k << "]: " << offsets[k];
            }
        }
        LOG(DEBUG) << "--------------";
        for (size_t k=1; k < __tpx_parameter__->pileup_event.value; ++k)
        {
            LOG(DEBUG) << "offsets[" << k << "]: " << offsets[k];
            LOG(DEBUG) << "offsets[" << (k-1) << "]: " << offsets[k-1];
            offsets[k] += offsets[k-1];
            LOG(DEBUG) << "cumulated offsets[" << k << "]: " << offsets[k];
        }
        LOG(DEBUG) << "--------------";
        size_t total_size = size_t(offsets.back() / timestep);
        total_size += pulse_orig.size();

        for (size_t i = 0; i < total_size; ++i)
            pulse_orig.emplace_back(0.0);

        for (size_t k=0; k < __tpx_parameter__->pileup_event.value; ++k)
        {
            size_t offs = size_t(offsets[k]/timestep);
            DTYPE pol = __tpx_parameter__->pileup_alternate_polarity.value ? std::pow(int(-1),int(k)) : 1;
            LOG(STATUS) << "pol: " << pol  << " " <<k;
            for (size_t i=0; i<pulse_orig.size(); ++i)
                pulse_orig[i + offs] += pol * pulse[i];
        }
    }

    size_t row = pixel_index.Y();
    size_t col = pixel_index.X();

    if (total_input_charge >0)
        LOG(STATUS) << "Pixel [" << row <<", " << col << "], charge " << Units::display(total_input_charge, "e");

    if ((row >= __threshold_matrix__.shape()[0]) || (col >= __threshold_matrix__.shape()[1]) ||
        (row >= __gain_matrix__.shape()[0]) || (col >= __gain_matrix__.shape()[1]) ||
        (row >= __alpha_ikrum_offset_matrix.shape()[0]) || (col >= __alpha_ikrum_offset_matrix.shape()[1])
        )
    {
        LOG(STATUS) << "sensor pixel [" << row <<", " << col << "] outside digitizer matrix [" << __tpx_parameter__->n_rows() <<", " << __tpx_parameter__->n_cols() << "]";
    }


    tpx->set_position(row, col);
    tpx->set_threshold(__threshold_matrix__[row][col]);
    tpx->set_preamp_gain(__gain_matrix__[row][col]);

    tpx->set_alpha_ikrum(__tpx_parameter__->preamp_parm.feedback_parm.alpha_ikrum_parm.fb_thresholds_e.value,
                         __tpx_parameter__->preamp_parm.feedback_parm.alpha_ikrum_parm.alpha_eff(__tpx_parameter__->temperature.value, __alpha_ikrum_offset_matrix[row][col]));

    size_t idx_bin_input_charge_vec = 0;
    DTYPE total_input = 0;

    if (__tpx_parameter__->randomize_event_clock_phase.value)
    {
        if (__current_event_idx__ != event_num)  // new event -> new clock phase
        {
            __current_event_clock_phase = __tpx_parameter__->disc_parm.random_t_clock_offset_sec(random_generator);
            __current_event_idx__ = event_num;
        }
    }
    __event_export__->reset();

    // add event time offset, otherwise all particles will be in constant phase shift with respect to pixel clock
    tpx->reset(__current_event_clock_phase);
    // run some noise
    while(tpx->time.now() < (__tpx_parameter__->t0_sec.value))
    {
        allpix::normal_distribution<double> _rgauss_(0, __idle_noise__);  // feed in __idle_noise__ e rms noise to wake up the transfer functions
        tpx->input_add(_rgauss_(random_generator));
        tpx->time.next();
        tpx->process(random_generator);
        __event_export__->append(*tpx.get());
    }
    double t_preamp = 0;
    double max_input_charge_time =  timestep * double(pulse.size()) ;  // [s]
    bool is_running = true;
    while (is_running && (t_preamp <= max_input_charge_time))
    {
        t_preamp += tpx->time.dt();  // tpx internal time <> (i*timestep) input charge time
        for (size_t i = idx_bin_input_charge_vec;
             i < pulse.size() && double(i) * timestep < t_preamp;
             ++i)
        {
            double charge = pulse.at(i);
            if (charge != 0)
            {
                total_input += charge;
                tpx->input_add(charge);
                // LOG(DEBUG) << "pixel input [" << row <<", " << col << "], @" << tpx->time.now_ns() <<"ns, charge " << Units::display(charge, "e") << " total " << Units::display(total_input, "e") << " total " << Units::display(tpx->preamp->total_charge(), "e");
            }
            ++idx_bin_input_charge_vec;
        }
        tpx->time.next();
        tpx->process(random_generator);
        __event_export__->append(*tpx.get());
        if (__tpx_parameter__->t_acq_sec.value > 0.0)  // fixed acquisistion time
        {
            is_running = t_preamp < __tpx_parameter__->t_acq_sec.value;
        }
    }
    //run until preamp is idle or shutter time is exceeded
    while (is_running &&  !tpx->is_idle())
    {
        tpx->time.next();
        tpx->process(random_generator);
        if (__tpx_parameter__->output_parm.output_event_plot.value)
            __event_export__->append(*tpx.get());
        if (__tpx_parameter__->t_acq_sec.value > 0.0)  // fixed acquisistion time
        {
            is_running = tpx->time.now() < __tpx_parameter__->t0_sec.value + __tpx_parameter__->t_acq_sec.value;
            if (!is_running)
                tpx->closeShutter();
        }
    }

    if(tpx->disc->events.size() > 0)
    {
        __event_export__->export_event(
            string_sprintf("%03u", event_num),
            std::to_string(row) + "-" + std::to_string(col));
    }


    for (auto e : tpx->disc->events)
        LOG(DEBUG) << "Pixel [" << row <<", " << col << "]: time "
                   << std::to_string(e->t_rise_async()*1.0e9) + "ns"
                   << ", signal "
                   << std::to_string(e->tot_async()*1.0e9) + "ns";

    // Fill histograms if requested
    if(__tpx_parameter__->output_parm.output_histogram.value)
    {
        if (is_sp_event && __tpx_parameter__->output_parm.output_histogram.value){
            for (auto e : tpx->disc->events)
                __hist_export__.fill(total_input_charge / 1e3, *e, tpx->charge_arrival_time);
        }
        if (is_sp_event and __tpx_parameter__->output_parm.output_single_pixel_histogram.value and tpx->disc->events.size())
            __hist_export_single_pixel_hit__.fill(total_input_charge / 1e3, * tpx->disc->events.front(), tpx->charge_arrival_time);
    }

    hits.clear();
    for (auto e : tpx->disc->events)
    {
        hits.emplace_back(pixel,
                          pixel_charge.getGlobalTime(),
                          e,
                          &pixel_charge);
        LOG(STATUS) << "Digitized " << tpx->disc->events.size() << " pixel hits in [" <<  tpx->row() <<", " << tpx->col() <<"], ToT " << e->tot_async()*1e6 << " ns";
    }

    if (__tpx_parameter__->output_parm.output_listmode.value)
    {
        for (auto e : tpx->disc->events)
            __lm_log__.log(event_num, total_input, tpx->charge_arrival_time, *e);
    }

    if (is_sp_event)
    {
        if (__tpx_parameter__->output_parm.output_single_pixel_listmode.value)
            for (auto e : tpx->disc->events)
                __lm_log_sp__.log(event_num,  total_input, tpx->charge_arrival_time, *e);
    }

}

void TpxDigitizer::__run_enc_std__(allpix::RandomNumberGenerator& random_generator)
{
    // run digitizer with miniaml input for ENC time series
    Moment<DTYPE> moment;
    size_t row = 0;
    size_t col = 0;
    tpx->set_position(row, col);
    tpx->set_threshold(__threshold_matrix__[row][col]);
    tpx->set_preamp_gain(__gain_matrix__[row][col]);
    tpx->set_alpha_ikrum(__tpx_parameter__->preamp_parm.feedback_parm.alpha_ikrum_parm.fb_thresholds_e.value,
                         __tpx_parameter__->preamp_parm.feedback_parm.alpha_ikrum_parm.alpha_eff(__tpx_parameter__->temperature.value, __alpha_ikrum_offset_matrix[row][col]));
    __event_export__->reset();
    allpix::normal_distribution<DTYPE> rand_gauss(0, __idle_noise__);
    for (int i=0; i < int(10.0e-6/tpx->time.dt()); ++i)
    {
        tpx->input_add(rand_gauss(random_generator));
        tpx->process(random_generator);
        __event_export__->append(*tpx.get());
        moment.add(tpx->out());
        tpx->time.next();
    }
    moment.calc();
    LOG(STATUS) << "ENC mean = " << moment.mean << ", std = " << moment.stdev;
    __event_export__->export_pulsegraphs(std::to_string(0), "0-0");
}

void TpxDigitizer::finalize()
{
    if (__tpx_parameter__->output_parm.output_listmode.value)
    {
        LOG(STATUS) << "writing list-mode.";
        __lm_log__.close();
        __lm_log_sp__.close();
        LOG(STATUS) << "done.";
    }
    if (__tpx_parameter__->output_parm.output_histogram.value)
        __hist_export__.scale_hist();
    if (__tpx_parameter__->output_parm.output_single_pixel_histogram.value)
        __hist_export_single_pixel_hit__.scale_hist();



    if(__tpx_parameter__->output_parm.output_histogram_png.value)
    {
        LOG(STATUS) << "writing png.";
        if (__tpx_parameter__->output_parm.output_histogram.value)
            __hist_export__.write_png_histograms();
        if (__tpx_parameter__->output_parm.output_single_pixel_histogram.value)
            __hist_export_single_pixel_hit__.write_png_histograms();
    }

    if(__tpx_parameter__->output_parm.output_histogram.value)
    {
        LOG(STATUS) << "writing histograms.";
        __hist_export__.write_root_histograms();

        if(__tpx_parameter__->output_parm.output_histogram_csv.value)
        {
            LOG(STATUS) << "writing csv.";
            __hist_export__.write_csv_histograms();
        }

        if (__tpx_parameter__->output_parm.output_single_pixel_histogram.value)
            __hist_export_single_pixel_hit__.write_root_histograms();
    }


    if (__tpx_parameter__->output_parm.output_histogram.value)
        __hist_export__.finalize();
    if (__tpx_parameter__->output_parm.output_single_pixel_histogram.value)
        __hist_export_single_pixel_hit__.finalize();

}
