#include "TpxPixelHit.h"
#include <set>

#include "objects/DepositedCharge.hpp"
#include "objects/PropagatedCharge.hpp"
#include "objects/exceptions.h"

using namespace allpix;

TpxPixelHit::TpxPixelHit(Pixel pixel, double global_time, const DiscriminatorEvent *discEvent, const PixelCharge* pixel_charge)
    :PixelHit(pixel, discEvent->t_rise_async(), global_time+discEvent->t_rise_async(), discEvent->tot_async(), pixel_charge)
{

    _t_rise_async_ = discEvent->t_rise_async();
    _t_fall_async_ = discEvent->t_fall_async();
    _max_amplitude_ = discEvent->max_amplitude();
    _t_peak_ = discEvent->t_peak();

    _clk_rise_ = discEvent->clk_rise();
    _fclk_rise_ = discEvent->fclk_rise();
    _ufclk_rise_ = discEvent->ufclk_rise();

    _clk_fall_ = discEvent->clk_fall();
    _fclk_fall_ = discEvent->fclk_fall();
    _ufclk_fall_ = discEvent->ufclk_fall();
}


void TpxPixelHit::print(std::ostream& out) const {
    out << "TpxPixelHit " << getIndex().X() << ", " << getIndex().Y() << ", " << getGlobalTime()
        << ", " << _t_fall_async_
        << ", " << _t_rise_async_
        << ", " << _max_amplitude_
        << ", " << _t_peak_
        << ", " << _clk_rise_
        << ", " << _fclk_rise_
        << ", " << _ufclk_rise_
        << ", " << _clk_fall_
        << ", " << _fclk_fall_
        << ", " << _ufclk_fall_;
}

void TpxPixelHit::loadHistory()
{
    pixel_charge_.get();
    std::for_each(mc_particles_.begin(), mc_particles_.end(), [](auto& n) { n.get(); });
}
void TpxPixelHit::petrifyHistory()
{
    pixel_charge_.store();
    std::for_each(mc_particles_.begin(), mc_particles_.end(), [](auto& n) { n.store(); });
}
