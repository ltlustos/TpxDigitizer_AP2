#pragma once
#pragma link C++ class allpix::Object::BaseWrapper < allpix::TpxPixelHit> + ;
#include <Math/DisplacementVector2D.h>
#include <TRef.h>
#include "objects/MCParticle.hpp"
#include "objects/Object.hpp"
#include "objects/PixelCharge.hpp"
#include "objects/Pixel.hpp"
#include "Pixel/Disc/DiscriminatorEvent.h"
#include "objects/PixelHit.hpp"
namespace allpix {
/**
     * @ingroup Objects
     * @brief Pixel triggered in an event after digitization
     */
class TpxPixelHit : public PixelHit {
public:
    /**
         * @brief Construct a digitized pixel hit
         * @param pixel Object holding the information of the pixel
         * @param local_time Timing of the occurrence of the hit in local reference frame
         * @param global_time Timing of the occurrence of the hit in global reference frame
         * @param signal Signal data produced by the digitizer
         * @param pixel_charge Optional pointer to the related pixel charge
         */
    TpxPixelHit(
            Pixel pixel,
            double global_time,
            const DiscriminatorEvent *discEvent,
            const PixelCharge* pixel_charge = nullptr);


    /**
         * @brief Print an ASCII representation of TpxPixelHit to the given stream
         * @param out Stream to print to
         */
    void print(std::ostream& out) const override;

    /**
         * @brief ROOT class definition
         */
//    ClassDefOverride(TpxPixelHit, 10); // NOLINT
    /**
         * @brief Default constructor for ROOT I/O
         */
    TpxPixelHit() = default;

    void loadHistory() override;
    void petrifyHistory() override;

private:
    Pixel pixel_;
    double global_time_{};
    double signal_{};

    double _t_rise_async_{};
    double _t_fall_async_{};
    double _max_amplitude_{};
    double _t_peak_{};
    std::size_t _clk_rise_{};
    std::size_t _fclk_rise_{};
    std::size_t _ufclk_rise_{};
    std::size_t _clk_fall_{};
    std::size_t _fclk_fall_{};
    std::size_t _ufclk_fall_{};

    PointerWrapper<PixelCharge> pixel_charge_;
    std::vector<PointerWrapper<MCParticle>> mc_particles_;
};

/**
     * @brief Typedef for message carrying pixel hits
     */
using TpxPixelHitMessage = Message<TpxPixelHit>;
} // namespace allpix


