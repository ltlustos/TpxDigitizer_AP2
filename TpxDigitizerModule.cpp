/**
 * @file
 * @brief digitization module with Krummenacher-like preamplifier
 * @copyright Copyright (c) 2021 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include "TpxDigitizerModule.hpp"
#include "modules/TpxDigitizer/Common/const.h"
#include "core/utils/unit.h"
#include "objects/PixelCharge.hpp"
#include "core/messenger/Message.hpp"
#include "Util/u_string.h"
#include "Util/u_os.h"
#include "Util/u_root.h"
#include <string_view>
#include <string>
#include <iomanip>
#include <ctime>
#include <chrono>
#include "TCanvas.h"
#include "objects/PixelHit.hpp"
#include "physics/MaterialProperties.hpp"
#include "TpxParameter.h"
#include "Util/u_config.hpp"
using namespace allpix;

TpxDigitizerModule::TpxDigitizerModule(
    Configuration& config,
    Messenger* messenger,
    std::shared_ptr<Detector> detector)
    : Module(config, std::move(detector)), __messenger__(messenger), __pixel_message__(nullptr)
{
    LOG(WARNING) << "parallel event processing not supported";
    __config__ = &config;

           // Require PixelCharge message
    __messenger__->bindSingle<PixelChargeMessage>(this, MsgFlags::REQUIRED);

           // add missing local unist
    __add_units__();

}

void TpxDigitizerModule::__add_units__()
{
    // FIXIT - to be removed
    // add units if necessary
    std::map<std::string, allpix::Units::UnitType> units {
        {"Hz", 1e-9},
        {"A", 1/QE * 1e-9},
        {"nA", 1/QE * 1e-18},
        {"pA", 1/QE * 1e-21},
        {"fA", 1/QE * 1e-24},
        {"perc", 1e-2},
        {"e/ns", 1}
    };

    for (auto e : units)
    {
        try
        {
            Units::add(e.first, e.second);  // trying to register
        }
        catch (const std::invalid_argument&){}// already there, all ok, do nothing
    }
}


void TpxDigitizerModule::initialize()
{

    include_config(__config__, "tpx_def");
    auto model = getDetector()->getModel();
    __tpx_parameter__ = std::make_shared<TpxParameter>(model, *getConfigManager());
    __tpx_parameter__->parse_config(*__config__);

    __tpx_parameter__->check_adjust();
    __tpx_parameter__->output_parm.adjust_output_path(getConfigManager()->getGlobalConfiguration());
    __digitizer__ = std::make_unique<TpxDigitizer>(__tpx_parameter__, getROOTDirectory());
}

void TpxDigitizerModule::run(Event* event)
{
    __pixel_message__ = __messenger__->fetchMessage<PixelChargeMessage>(this, event);
    unsigned int event_num = event->number;

// Loop through all pixels with charges
    const auto *charges = &__pixel_message__->getData();
    __disc_events__.clear();
    __hits__.clear();
    DTYPE total_charge{0};
    for(const auto&  pixel_charge : *charges)
        total_charge += std::abs(static_cast<DTYPE>(pixel_charge.getCharge()));
    if (total_charge == 0)
        return;
    LOG(STATUS) << "running event #" << event_num << ", " << charges->size() << " pixel charges";

    for(const auto& pixel_charge : *charges)
    {
        __digitizer__->process_event(
            event->getRandomEngine(),
            pixel_charge,
            event_num,
            charges->size()==1);
        if (__digitizer__->hits.size())
            __hits__.insert(__hits__.end(), __digitizer__->hits.begin(), __digitizer__->hits.end());

        if (__digitizer__->tpx->disc->events.size())
            for (auto e : __digitizer__->tpx->disc->events)
                __disc_events__.emplace_back(*e);
    }
    if(!__hits__.empty())
    {
        // Create and dispatch hit message
        auto tpx_hits_message = std::make_shared<TpxPixelHitMessage>(std::move(__hits__), getDetector());
        __messenger__->dispatchMessage(this, tpx_hits_message, event);
        LOG(STATUS) << "TpxPixelHitMessage dispatched.";
    }
}

void TpxDigitizerModule::finalize()
{
    __digitizer__->finalize();
    cleanup_tcanvas();
}

